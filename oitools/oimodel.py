# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 08:47:42 2020

@author: Ame
"""



import dill
import numpy as np
#from astropy.io import fits
import astropy.units as u
from scipy.special import j1
from scipy.special import j0
from scipy.special import jv
from astropy.io import fits
from scipy.optimize import least_squares
from scipy.signal import convolve2d
#from scipy.interpolate import interp1d
import os
import json
import matplotlib.pyplot as plt
import copy
import pandas as pd

from . import fitter
from . import oifitstools as ut
from . import utils

I=np.complex(0,1)


"""

oim_interp1d        : class for chromatic value of model parameter 
                      (see example in test_oimodel_chromatic)
oim_param           : class for all model parameters
oim_component       : based class for components of model
    oim_pt          : component Point source
    oim_bckg        : component Background
    oim_ud          : component Uniform Disk
    oim_ellipse     : component Uniform Ellipse
    oim_gauss       : component Gaussian Disk
    oim_egauss      : component Gaussian Dllipse
    oim_ring        : component Ring 
    oim_ring2       : component Ring with width : din dout
    oim_ering       : component Elliptic ring 
    oim_ering2      : component Elliptic ring with width : din dout
    oim_eskewring   : Elliptic Skewed ring 
    oim_convolution : component created wiwth convolution of two components    
oim_model           : Class for model (i.e. combination of components)    
oim_simulator       : Class for simulator = data + model + fitter (not-implemented yet)



TODO: 
getImage with mutiple wavelengths is not optmized (i.e. loop on wavelength)
Merge circular  and elliptic classes (if "elong" is added to parameter list then it is elliptic )
Merge ring & ring2 (if dout is omitted then infintesimal ring)
Add limb darkenened linear and quadratic
Add simple temperature gradient disk
Add Model with flux function (i.e black-body, kurucz or other)
Add image-based model (maybe link/merge with cubetools simulator)
Implement emcee fit

"""


###############################################################################   
###############################################################################      

class oim_interp1d:
    def __init__(self,x = None,y = None, dct = None, template = None):
        if dct is None:
            self._x=[] # list of the reference pts (usually wl)
            self._y=[] # list of the values => type oim_param
            self.nparams=0
            x=np.array(x)
            y=np.array(y)
            self.isinterp1d = True
        
            if np.size(x)==np.size(y):
                for i in range(np.size(x)):
                    self.addRef(x[i],y[i])
             
            else:
                raise TypeError("'x' and 'y' should have the same length : len(x)={}, len(y)={}".format(len(x), len(y)))
        else:
            self._fromdct(dct)
           
    def setParamInfos(self,param=None):
        for i,parami in enumerate(self._y):
            parami.name="{0}_{1}".format(param.name,i+1)
            parami.min=param.min
            parami.max=param.max
            parami.template = param.template
            parami.description="{0} (interp)".format(param.description)
            parami.unit=param.unit
       
    def addRef(self,xi,yi):
        
        n=len(self._x)
        self._y.append(oim_param("interp_v{0}".format(n),yi))
        self._x.append(xi)
        self.nparams=len(self._y)     
        #TODO order x & y
        
    @property
    def yref(self):       
        return np.array([yi() for yi in self._y])
           
    def __call__(self,x):
        y0=self.yref
        return np.interp(x,self._x,y0,left=y0[0], right=y0[-1])
     
    def __str__(self):
        txt="interp1d(x=["
        for xi in self._x:
            txt+="{0},".format(xi)
        txt=txt[:-1]
        txt+="], y=["
        for yi in self._y:
            txt+="{0},".format(yi.value) 
        txt=txt[:-1]
        txt+="])"    
        return txt
    
    def __format__(self, spec):
        txt="interp1d(x=["
        for xi in self._x:
            txt+="{0},".format(xi)
        txt=txt[:-1]
        txt+="], y=["
        for yi in self._y:
            txt+="{0},".format(yi.value) 
        txt=txt[:-1]
        txt+="])"    
        return txt

    def _fromdct(self, dct):
        self._x = dct["x"]
        self._y = []
        for key in dct["y"]:
            self._y.append(oim_param(dct = dct["y"][key]))
        for key in dct:
            if key != "x" and key != "y":
                self.__dict__[key] = dct[key]
        

###############################################################################   
###############################################################################      
    
class oim_param:
    def __init__(self,name=None,value=None,mini=-1*np.inf,maxi=np.inf,description="",unit=1, dct = None, template = None):
        if dct is not None:
            self._loaddict(dct)
        else:
            self.name=name
            self.value=value  # can be either a number or an instance of oim_interp1d
            self.error=None
            self.min=mini
            self.template = template
            self.max=maxi
            self.free=True              
            self.description=description
            self.unit=unit
            
            self.type = "parameter"
            
            # Currently,the code never goes past this if, because the '.value' is set directly into 
            # an oim_interp1d object (in oim_component._eval), rather than calling oim_param(value = interp1d(...))
            if isinstance(self.value, oim_interp1d):
                self.type = "interp1d"
            else:
                self.type = "parameter"
        
        
        
    def __call__(self,wl=None):
        if isinstance(self.value,oim_interp1d):
            return self.value(wl)
        else:
            return self.value
    
    def _savedict(self):
        temp = {}
        for key,val in self.__dict__.items():
            if not isinstance(val, oim_interp1d):
                temp[key] = val
            else:
                if not key in temp:
                    temp[key] = {}
                
                temp[key]["x"] = val._x
                
                if not "y" in temp[key]:
                    temp[key]["y"] = {}
                for i in range(len(val._y)):
                    temp[key]["y"]["y{}".format(i)] = val._y[i]._savedict()
        return temp
    
    def _loaddict(self, dct):
        for key in dct:
            if key != "value":
                self.__dict__[key] = dct[key]
                
        if dct["type"] == "parameter":
            self.__dict__["value"] = dct["value"]
            
        elif dct["type"] == "interp1d":
            val = dct["value"]
            self.value = oim_interp1d(dct = val)
            
        else:
            raise TypeError("Unknown parameter type")
        
        return self
    
    
###############################################################################   
###############################################################################      

class oim_component:
    def __init__(self, dct = None, key = None, **kwargs):
        
        if dct is not None and key is not None:
            self._loaddict(dct, key)
        
        else:
            self.name = "Generic component"
            self.shortname = "Gen comp"
            self.elliptic=False
            self.type="component"
                
            x=oim_param("x",0,description="x position",unit=u.mas, template = 'pos')
            y=oim_param("y",0,description="y position",unit=u.mas, template = 'pos')
            f=oim_param("f",1,description="flux", template = 'flux')     
            self.params={"x":x,"y":y,"f":f}
            self.name="Generic component"
            
            self._eval(**kwargs)
            
        
        
        
    def _eval(self,**kwargs):
        for key, value in kwargs.items():
            if key in self.params.keys(): 
                self.params[key].value=value
     
     
                if isinstance(value,oim_interp1d):
                    self.params[key].type = "interp1d"
                    value.setParamInfos(self.params[key])
                
                # elif hasattr(value, "__iter__"):
                    
                    
    def getComplexVisibility(self,spafreqs,wl=None):
        
        if self.elliptic==True:
            
            pa_rad=(self.params["pa"](wl)+90)*self.params["pa"].unit.to(u.rad)      
            co=np.cos(pa_rad)
            si=np.sin(pa_rad)
            fxp=spafreqs[0]*co-spafreqs[1]*si
            fyp=spafreqs[0]*si+spafreqs[1]*co
            rho=np.sqrt(fxp**2+fyp**2/self.params["elong"](wl)**2) 
        else:
            fxp=spafreqs[0]
            fyp=spafreqs[1]
            rho=np.sqrt(fxp**2+fyp**2)            
               
        vc=self._visFunction(fxp,fyp,rho,wl)
               
        return vc*self._ftTranslateFactor(spafreqs,wl)
    
    

        
    def _ftTranslateFactor(self,spafreqs,wl): 
        x=self.params["x"](wl)*self.params["x"].unit.to(u.rad)
        y=self.params["y"](wl)*self.params["y"].unit.to(u.rad)
        return np.exp(-2*I*np.pi*(spafreqs[0]*x+spafreqs[1]*y))
    
          
    def _visFunction(self,xp,yp,rho,wl):
        return xp*0
    
    def _directTranslate(self,x,y,wl):
        x=x-self.params["x"](wl)
        y=y-self.params["y"](wl)
        return x,y
    
    def getImage(self,dim,pixsize,wl=None):  
        x=np.tile((np.arange(dim)-dim/2)*pixsize,(dim,1))
        y=np.transpose(x)
        x,y=self._directTranslate(x,y,wl)
        
        if self.elliptic:
            pa_rad=(self.params["pa"](wl)+90)*self.params["pa"].unit.to(u.rad)
            xp=x*np.cos(pa_rad)-y*np.sin(pa_rad)
            yp=x*np.sin(pa_rad)+y*np.cos(pa_rad)
            x=xp
            y=yp
        image = self._imageFunction(x,y,wl)
        tot=np.sum(image)
        if tot!=0:  
            image = image / np.sum(image,axis=(0,1))*self.params["f"](wl)    

        return image
    
    def getParameters(self, only_free = False):
        params = {}
        for name,param in self.params.items():
            if hasattr(param,'components'):
                for key in param.params:
                    params["c0_{0}_{1}".format(param.shortname, key)] = param.params[key]
                
                i=1
                for comp in param.components:
                    comp_params = comp.getParameters(only_free = only_free)
                    for key in comp_params:
                        params["c{0}_{1}_{2}".format(i, comp.shortname, key)] = comp_params[key]
                    i+=1
            elif isinstance(param.value,oim_interp1d):
                for iy,yi in enumerate(param.value._y):
                    if only_free and not yi.free:
                        continue
                    params["{0}_interp{1}".format(name, iy+1)]=yi
            else:
                if only_free and not param.free:
                    continue
                params[name]=param
        return params
                    
    
    
    def _toPandas(self, to_get = "value", only_free = True, only_params = None, ignore = None, mode = "errors"):
        if type(to_get) == type(""):
            to_get = [to_get]
        
        if type(ignore) == type(""):
            ignore = [ignore]
        
        if type(only_params) == type(""):
            only_params = [only_params]

            
        if hasattr(self,"components"):
            comp_dfs = []
            for comp in self.components:
                comp_dfs.append(comp._toPandas(to_get = to_get, only_free = only_free, mode = mode))
            
            df = pd.concat(comp_dfs, axis = 1)
            df = pd.concat([df], axis=1, keys=[self.shortname])
            return df
            
            
        vals = []
        keys = []
        for key in self.params:
            param = self.params[key]
            
            if ignore is not None:
                if key in ignore:
                    continue
            if only_params is not None:
                if not key in only_params:
                    continue

            
            if not isinstance(param.value, oim_interp1d):
                
                if only_free and not param.free:
                    continue
                
                sub_vals = []
                for attr in to_get:
                    thedct = param.__dict__
                    if mode == "value":
                        sub_vals.append(thedct[attr])
                    if mode == "errors" and attr == "value":
                        errs = thedct["error"]
                        if errs is None:
                            sub_vals.append(thedct[attr])
                            continue
                        err_up = errs[1]
                        err_low = errs[0]
                        if err_low is None:
                            err_low = ""
                        else:
                            err_low = "_{{" + str(np.round(err_low,2)) + "}}"
                        if err_up is None:
                            err_up = ""
                        else:
                            err_up = "^{{+" + str(np.round(err_up,2)) + "}}"
                            
                        theval = np.round(thedct[attr],2)
                        string = "${}{}{}$".format(theval, err_up, err_low)
                        sub_vals.append(string)
                    # print(attr,param.__dict__[attr] )
                vals.append(sub_vals)
                keys.append(key)
                
            else:
                for i in range(len(param.value._y)):
                    if only_free and not param.value._y[i].free:
                        continue
                    
                    sub_vals = []
                    for attr in to_get:
                        sub_vals.append(param.value._y[i].free)
                    vals.append(sub_vals)
                    keys.append("{}_y{}".format(key,i+1))
        
        vals = np.array(vals).transpose()
        header = pd.MultiIndex.from_product([[self.shortname],keys])
        df = pd.DataFrame(vals, index = to_get, columns = header)
        return df
    
    def _imageFunction(self,xx,yy,wl=None):
        image=xx*0+1
        return image
  
    def __str__(self):
        txt=self.name
        for name,param in self.params.items():  
            txt+=" {0}={1:.2f}".format(param.name,param.value)
        return txt
    
    def __add__(self,componentOrModel):
        c=[self]
        if componentOrModel.type=="model":
            c.extend(componentOrModel.components)
        else:
            c.append(componentOrModel)
        return oim_model(c)
    
    def _savedict(self):
        temp = {}
        temp["params"] = {}
        for key,val in self.__dict__.items():
            if key == "params":
                for name, val2 in val.items():
                    temp["params"][name] = val2._savedict()
                temp["params"]["origin_order"] = [key for key in val]
            else:
                # if utils.isjsonable(val):
                temp[key] = val
                # else:
                    # temp[key] = val.__str__()
        return temp

    def _loaddict(self, dct, classname):
        params_dct = {}
        
        origin_order = dct["params"]["origin_order"]
        del dct["params"]["origin_order"]
        
        for key in dct["params"]:
            params_dct[key] = oim_param(dct = dct["params"][key])
            
        params_dct = {key:params_dct[key] for key in origin_order}
        
        comp = eval(classname)()
        for key in dct:
            if not key == "params":
                comp.__dict__[key] = dct[key]
            else:
                comp.__dict__[key] = params_dct
        
        return comp
    
        
###############################################################################   
###############################################################################
class oim_pt(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Point source"
         self.shortname = "Pt"
         self._eval(**kwargs)      

    def _visFunction(self,xp,yp,rho,wl):
        return 1
    
    def _imageFunction(self,xx,yy,wl):
        image=xx*0
        val=np.abs(xx)+np.abs(yy)
        idx=np.unravel_index(np.argmin(val),np.shape(val))
        image[idx]=1
        return image
    
###############################################################################
class oim_bckg(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Background"
         self.shortname = "Bckg"
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        vc=rho*0
        idx=np.where(rho==0)[0]
        if np.size(idx)!=0:
            vc[idx]=1
        return vc

    def _imageFunction(self,xx,yy,wl):
        return xx*0+1
    
###############################################################################
class oim_ud(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Uniform Disk"
         self.shortname = "UD"
         self.params["d"]=oim_param("d",0,description="Diameter",unit=u.mas, template = 'diameter')
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        xx=np.pi*self.params["d"](wl)*self.params["d"].unit.to(u.rad)*rho
        return 2*j1(xx)/xx
    
    def _imageFunction(self,xx,yy,wl):
        return ((xx**2+yy**2)<=(self.params["d"](wl)/2)**2).astype(float)
    
###############################################################################
class oim_ellipse(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Uniform Ellipse"
         self.shortname = "eUD"
         self.elliptic=True
         self.params["d"]=oim_param("d",0,description="Major-axis diameter",unit=u.mas, template = 'diameter')
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self._eval(**kwargs)
         
    def _visFunction(self,xp,yp,rho,wl):
        xx=np.pi*self.params["d"].value*self.params["d"].unit.to(u.rad)*rho
        return 2*j1(xx)/xx
          
    def _imageFunction(self,xx,yy,wl):
        return ((xx**2+yy**2*self.params["elong"].value**2)<=(self.params["d"].value/2)**2).astype(float)
###############################################################################   

class oim_gauss(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Gaussian Disk"
         self.shortname = "Gauss"
         self.params["fwhm"]=oim_param("fwhm",0,description="FWHM",unit=u.mas, template = 'fwhm')
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        return np.exp(-1*(np.pi*self.params["fwhm"](wl)*self.params["fwhm"].unit.to(u.rad)*rho)**2/(4*np.log(2)))
    
    def _imageFunction(self,xx,yy,wl):
        
        r2=(xx**2+yy**2)
        return np.sqrt(4*np.log(2*self.params["fwhm"](wl))/np.pi)*np.exp(-4*np.log(2)*r2/self.params["fwhm"](wl)**2)


class oim_gauss2(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Gaussian Disk"
         self.shortname = "Gauss"
         self.params["fwhm"]=oim_param("fwhm",0,description="FWHM",unit=u.mas, template = 'fwhm')        
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        
        d = 10 * 0.5
        d = d*(u.mas).to(u.rad)
        return np.exp(-1*(np.pi*self.params["fwhm"](wl)*self.params["fwhm"].unit.to(u.rad)*rho)**2/(4*np.log(2)))
    
    def _imageFunction(self,xx,yy,wl):       
        r2=(xx**2+yy**2)
        return 4 * np.log(2) /( np.pi * self.params["fwhm"](wl)**2) * np.exp(-4*np.log(2)*r2/self.params["fwhm"](wl)**2)
   


###############################################################################   

class oim_egauss(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Gaussian Ellipse"
         self.shortname = "eGauss"
         self.elliptic=True
         self.params["fwhm"]=oim_param("fwhm",0,description="FWHM",unit=u.mas, template = 'fwhm')        
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        return np.exp(-1*(np.pi*self.params["fwhm"](wl)*self.params["fwhm"].unit.to(u.rad)*rho)**2/(4*np.log(2)))
    
    def _imageFunction(self,xx,yy,wl):       
        r2=(xx**2+yy**2*self.params["elong"].value**2)
        return np.sqrt(4*np.log(2*self.params["fwhm"](wl))/np.pi)*np.exp(-4*np.log(2)*r2/self.params["fwhm"](wl)**2)
   
    
class oim_egauss2(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Gaussian Ellipse"
         self.shortname = "eGauss"
         self.elliptic=True
         self.params["fwhm"]=oim_param("fwhm",0,description="FWHM",unit=u.mas, template = 'fwhm')        
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
        return np.exp(-1*(np.pi*self.params["fwhm"](wl)*self.params["fwhm"].unit.to(u.rad)*rho)**2/(4*np.log(2)))
    
    def _imageFunction(self,xx,yy,wl):       
        r2=(xx**2+yy**2*self.params["elong"].value**2)
        return 4 * np.log(2) /( np.pi * self.params["fwhm"](wl)**2) * np.exp(-4*np.log(2)*r2/self.params["fwhm"](wl)**2)
   


###############################################################################
class oim_ring(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Ring"
         self.shortname = "Ring"
         self.params["d"]=oim_param("d",0,description="Diameter",unit=u.mas, template = 'diameter')            
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):     
        xx=np.pi*self.params["d"](wl)*self.params["d"].unit.to(u.rad)*rho
        return j0(xx)
    
    def _imageFunction(self,xx,yy,wl):
        r2=(xx**2+yy**2)   
        dx=1.*(xx[0,1]-xx[0,0])        
        return ((r2<=(self.params["d"](wl)/2+dx)**2) & (r2>=(self.params["d"](wl)/2)**2)).astype(float)


###############################################################################
class oim_ring2(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Ring"
         self.shortname = "Ring"
         self.params["din"]=oim_param("din",0,description="Inner Diameter",unit=u.mas, template = 'diameter')
         self.params["dout"]=oim_param("dout",0,description="Outer Diameter",unit=u.mas, template = 'diameter')                 
         self._eval(**kwargs)

    def _visFunction(self,xp,yp,rho,wl):
      
        xxin=np.pi*self.params["din"](wl)*self.params["din"].unit.to(u.rad)*rho
        xxout=np.pi*self.params["dout"](wl)*self.params["dout"].unit.to(u.rad)*rho
    
        fin=(self.params["din"](wl))**2
        fout=(self.params["dout"](wl))**2
           
        return 2*(j1(xxout)/xxout*fout-j1(xxin)/xxin*fin)/(fout-fin)
    
    def _imageFunction(self,xx,yy,wl):
        r2=(xx**2+yy**2)      
        return ((r2<=(self.params["dout"](wl)/2)**2) & (r2>=(self.params["din"](wl)/2)**2)).astype(float)
    
###############################################################################
class oim_ering(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Elliptical Ring"
         self.shortname = "eRing"
         self.elliptic=True
         self.params["d"]=oim_param("d",0,description="Diameter",unit=u.mas, template = 'diameter')  
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self._eval(**kwargs)
         
    def _visFunction(self,xp,yp,rho,wl):
        xx=np.pi*self.params["d"](wl)*self.params["d"].unit.to(u.rad)*rho
        return j0(xx)
          
    def _imageFunction(self,xx,yy,wl):
        r2=(xx**2+yy**2*self.params["elong"](wl)**2)       
        dx=np.abs(1*(xx[0,1]-xx[0,0]+xx[1,0]-xx[0,0])*self.params["elong"](wl))
        
        print(dx)
        return ((r2<=(self.params["d"](wl)/2+dx)**2) & (r2>=(self.params["d"](wl)/2)**2)).astype(float)
###############################################################################   

class oim_ering2(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Elliptical Ring"
         self.shortname = "eRing"
         self.elliptic=True
         self.params["din"]=oim_param("din",0,description="Inner Diameter",unit=u.mas, template = 'diameter')
         self.params["dout"]=oim_param("dout",0,description="Outer Diameter",unit=u.mas, template = 'diameter')       
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self._eval(**kwargs)
         
    def _visFunction(self,xp,yp,rho,wl):
        
        xxin=np.pi*self.params["din"](wl)*self.params["din"].unit.to(u.rad)*rho
        xxout=np.pi*self.params["dout"](wl)*self.params["dout"].unit.to(u.rad)*rho
    
        fin=(self.params["din"](wl))**2
        fout=(self.params["dout"](wl))**2
           
        return 2*(j1(xxout)/xxout*fout-j1(xxin)/xxin*fin)/(fout-fin)
          
    def _imageFunction(self,xx,yy,wl):
        r2=(xx**2+yy**2*self.params["elong"](wl)**2)               
        return ((r2<=(self.params["dout"](wl)/2)**2) & (r2>=(self.params["din"](wl)/2)**2)).astype(float)

###############################################################################
class oim_eskewring(oim_component):
    def __init__(self,**kwargs):        
         super().__init__(**kwargs)
         self.name="Elliptical Skewed Ring"
         self.shortname = "eSKW"
         self.elliptic=True
         self.params["d"]=oim_param("d",0,description="Diameter",unit=u.mas, template = 'diameter')  
         self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
         self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
         self.params["skw"]=oim_param("skw",0,description="Skewness",unit=1, template = 'skw')
         self._eval(**kwargs)
         
    def _visFunction(self,xp,yp,rho,wl):
        # note on xx : prefactor is np.pi instead of 2*np.pi
        # That's because we use the diameter 'd' instead of the half-light radius 
        xx=np.pi*self.params["d"](wl)*self.params["d"].unit.to(u.rad)*rho    
        phi=np.arctan2(yp, xp) +  self.params["pa"](wl)* self.params["pa"].unit.to(u.rad)
        return j0(xx)-I*np.cos(phi)*j1(xx)*self.params["skw"].value
          
    def _imageFunction(self,xx,yy,wl):
        r2=(xx**2+yy**2*self.params["elong"](wl)**2)  
        # dr=np.sqrt(np.abs(np.roll(r2,(-1,-1),(0,1))-np.roll(r2,(1,1),(0,1))))
        phi=np.arctan2(yy,xx)  +  self.params["pa"](wl)* self.params["pa"].unit.to(u.rad)
        dx=np.abs(1*(xx[0,1]-xx[0,0]+xx[1,0]-xx[0,0])*self.params["elong"](wl))
        #dx=np.abs(1*(xx[0,1]-xx[0,0]+xx[1,0]-xx[0,0]))*3
        F=1+self.params["skw"](wl)*np.cos(phi)           
        return  ((r2<=(self.params["d"](wl)/2+dx/2)**2) & (r2>=(self.params["d"](wl)/2-dx/2)**2)).astype(float)*F
        
          

        

class oim_lorentz(oim_component):
    """
    Lorentzian
    F(r) = a/(2pi*sqrt(3)) * (a**2 /3 + r**2)**(-3/2)
    V(q) = exp(- 2pi * a/sqrt(3) * q)
    
    with 'a' the half-light radius
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name="Lorentzian"
        self.shortname = "Lrtz"
        self.elliptic=False
        self.params["fwhm"]=oim_param("fwhm",0,description="FWHM",unit=u.mas, template = 'fwhm')        
        self._eval(**kwargs)
        
    def _visFunction(self,xp,yp,rho,wl):
        return np.exp(-np.pi*self.params["fwhm"](wl) *self.params["fwhm"].unit.to(u.rad)*rho / np.sqrt(3))
          
    def _imageFunction(self,xx,yy,wl):
        prefact = self.params["fwhm"](wl) / (4*np.pi * np.sqrt(3))
        r2 = xx**2 + yy**2
        postfact = (self.params["fwhm"](wl)**2 /12 + r2)**(-3/2)
        return prefact*postfact


class oim_new_skw_ring(oim_component):
    """
    WARNING : in Lazareff2017, the pa_skw is defined with the origin along the major axis.
    That means that in order to have the bright side as the inner edge of the ring,
    in the small scale axis, we must have pa_skw = 90°. Thus we add pi to the angles in the code,
    so that we can have pa_skw as the favorable case. That's the attribute 'pa_skw_offset'
    """
    def __init__(self, **kwargs):
        
        super().__init__(**kwargs)
        self.name="Elongated Skewed Ring"
        self.shortname = "eSKW"
        self.elliptic = True
        
        self.params["d"]=oim_param("d",1,description="Diameter",unit=u.mas, template = 'diameter')  
        self.params["elong"]=oim_param("elong",1,description="Elongation Ratio",unit=1, template = 'elong')
        self.params["pa"]=oim_param("pa",0,description="Position Angle of Major-axis",unit=u.deg, template = 'pa')
        self.params["pa_skw"]=oim_param("pa",0,description="Position Angle Skewness",unit=u.deg, template = 'pa')
        self.params["skw"]=oim_param("skw",0,description="Skewness",unit=1, template = 'skw')
        
        self.pa_skw_offset = np.pi/2
        
        keys = ["pa_skw", "skw"]
        self.m = []
        for key in keys:
            if not key in kwargs:
                continue
            
            if not hasattr(kwargs[key], "__iter__"):
                kwargs[key] = [kwargs[key]]
            
            for i in range(len(kwargs[key])):
                kwargs[key+str(i+1)] = kwargs[key][i]
                self.params[key+str(i+1)] = copy.deepcopy(self.params[key])
            
            self.m.append(i+1)
            
            del self.params[key]
            del kwargs[key]
        
        if self.m == []:
            self.m = 1
            # Currently, the reloading only works for m=1
            print("WARNING TODODTODODO")
        
        if np.min(self.m) != np.max(self.m):
            raise ValueError("'pa' and 'skw' should have the same length")
        
        self.m = np.max(self.m)
        self._eval(**kwargs)
        
    def _visFunction(self,xp,yp,rho,wl):
        # note on xx : prefactor is in np.pi instead of 2*np.pi
        # That's because we use the diameter 'd' instead of the half-light radius
        xx=np.pi*self.params["d"](wl)*self.params["d"].unit.to(u.rad)*rho
        prefact = j0(xx)
        
        thesum = 0
        
        phi=  np.arctan2(yp, xp) -  (self.pa_skw_offset + self.params["pa_skw1"](wl)* self.params["pa_skw1"].unit.to(u.rad))
        thesum += -I * self.params["skw1"].value * np.cos(phi) * j1(xx)
        for i in range(2, self.m +1):
            phi = np.arctan2(yp, xp) - (self.pa_skw_offset + self.params["pa_skw"+str(i)](wl)* self.params["pa_skw"+str(i)].unit.to(u.rad))
            thesum += (-I)**(i) * self.params["skw"+str(i)].value * np.cos(i * phi) * jv(i, xx)
        return prefact + thesum

    def _imageFunction(self,xx,yy,wl):
        phi=np.arctan2(yy,xx)
        thesum = 0
        for i in range(1, self.m +1):
            cj = self.params["skw"+str(i)].value * np.cos(self.pa_skw_offset + self.params["pa_skw"+str(i)](wl)* self.params["pa_skw"+str(i)].unit.to(u.rad))
            sj = self.params["skw"+str(i)].value * np.sin(self.pa_skw_offset + self.params["pa_skw"+str(i)](wl)* self.params["pa_skw"+str(i)].unit.to(u.rad))
            thesum += cj*np.cos(i*phi) + sj*np.sin(i*phi)
        
        dx=np.abs(1*(xx[0,1]-xx[0,0]+xx[1,0]-xx[0,0])*self.params["elong"](wl))
        F = 1+thesum
        r2=(xx**2+yy**2*self.params["elong"](wl)**2)  
        return  ((r2<=(self.params["d"](wl)/2+dx/2)**2) & (r2>=(self.params["d"](wl)/2-dx/2)**2)).astype(float)*F





###############################################################################   

class oim_convolution(oim_component):
    def __init__(self,component1,component2 =None):
        if type(component1) == type([]):
            component2 = component1[1]
            component1 = component1[0]
        super().__init__()
        self.name="Convolution of {0} and {1}".format(component1.name,component2.name)
        self.shortname = "{0}*{1}".format(component1.shortname, component2.shortname)
        self.elliptic=component1.elliptic
        self.components=[component1,component2]
        self.type = "composite"
        
        self.params = self.getParameters()
        
    
    def getParameters(self, only_free = False):
        component1 = self.components[0]
        component2 = self.components[1]
        
        params = component1.getParameters(only_free = only_free)
        
        if only_free:
            for key in params:
                if not params[key].free:
                    del params[key]
        
        i=0
        for key,value in component2.params.items():
            if only_free and not value.free:
                    continue
            if key in component1.params.keys():
                if not(key in ["f","x","y"]):    
                    params["{0}{1}".format(key,i)]=value
                    i+=1
            else:
                params[key]=value
        
        return params
        
    
    def  _visFunction(self,xp,yp,rho,wl):
         return self.components[0]._visFunction(xp,yp,rho,wl)* self.components[1]._visFunction(xp,yp,rho,wl)
   
       
    def _imageFunction(self,xx,yy,wl):
       #I'm not sure why but the convolution is shifting the image by 1 pix in each direction
       return np.roll(convolve2d(self.components[0]._imageFunction(xx,yy,wl),
                                 self.components[1]._imageFunction(xx,yy,wl),
                                 mode='same'),shift=(-1,-1),axis=(0,1))


###############################################################################


class oim_product(oim_component):
    """
    Do not use - Incomplete component
    Idea : TF of product == Convolution of TFs
    """
    def __init__(self,component1,component2 =None):
        if type(component1) == type([]):
            component2 = component1[1]
            component1 = component1[0]
        super().__init__()
        self.name="Product of {0} and {1}".format(component1.name,component2.name)
        self.shortname = "{0}x{1}".format(component1.shortname, component2.shortname)
        self.elliptic=component1.elliptic
        self.components=[component1,component2]
        self.type = "composite"
        
        self.params = self.getParameters()
    
    def getParameters(self, only_free = False):
        component1 = self.components[0]
        component2 = self.components[1]
        
        params = component1.getParameters(only_free = only_free)
        
        if only_free:
            for key in params:
                # print(key)
                if not params[key].free:
                    del params[key]
        
        i=0
        for key,value in component2.params.items():
            # print(key)
            if only_free and not value.free:
                    continue
            if key in component1.params.keys():
                if not(key in ["f","x","y"]):    
                    params["{0}{1}".format(key,i)]=value
                    i+=1
            else:
                params[key]=value
        
        return params
    
    def _visFunction(self,xp,yp,rho,wl):
        comp1 = self.components[0]
        comp2 = self.components[1]
        return np.convolve(comp1._visFunction(xp,yp,rho,wl), comp2._visFunction(xp,yp,rho,wl),mode= 'same')
    
    def _imageFunction(self, xx, yy, wl):
        comp1 = self.components[0]
        comp2 = self.components[1]
        return comp1._imageFunction(xx,yy,wl) * comp2._imageFunction(xx,yy,wl)


class inner_outer(oim_component):
    """
    Do not use - Incomplete component
    """
    def __init__(self, components, rlim):
        super().__init__()
        self.rlim = rlim
        self.components = components


    def getComplexVisibility(self,spafreqs,wl=None):
        r = None #TODO
        
        # Voir plutot:
        # Return la somme des deux visibilités de chacune des composantes
        # Sauf que les composantes doivent être coupées à rlim
        
        
        if r < self.rlim:
            return self.components[0].getComplexVisibility(spafreqs, wl =wl)
        else:
            return self.components[1].getComplexVisibility(spafreqs, wl =wl)


class kernel_lor_gauss(oim_component):
    """
    Do not use - Incomplete component
    """
    def __init__(self, components, rlim, rlim_mode = "diameter"):
        """
        rlim_mode should be either 'diameter' or 'radius'. If 'diameter', will multiply its value by 0.5
        """
        super().__init__()
        # rlim should point towards another component's parameter.
        # For example : an skw_ring +diameter
        self.rlim = rlim
        if rlim_mode ==  'diameter':
            self.rlim_fact = 0.5
        else:
            self.rlim_fact = 1
        self.components = components
        self.name="Convolution Kernel, Lorentzian r<0, Gaussian r>0"
        self.shortname = "Ker_-Lor_+Gauss"
        self.params = self.getParameters()

    def getParameters(self, only_free = False):
        component1 = self.components[0]
        component2 = self.components[1]
        
        params = component1.getParameters(only_free = only_free)
        
        if only_free:
            for key in params:
                # print(key)
                if not params[key].free:
                    del params[key]
        
        i=0
        for key,value in component2.params.items():
            # print(key)
            if only_free and not value.free:
                    continue
            if key in component1.params.keys():
                if not(key in ["f","x","y"]):    
                    params["{0}{1}".format(key,i)]=value
                    i+=1
            else:
                params[key]=value
        
        return params

    def _visFunction(self,xp,yp,rho,wl):        
        rho = rho
        psi = np.arctan2(yp,xp)
        
        rhos = rho.flatten()
        psis = psi.flatten()
        print(len(psis))
        final1 = []
        final2 = []
        for i in range(len(psis)):
            taus = np.linspace(psis[i]-np.pi, psis[i]+np.pi, 1000)
            rs = np.linspace(0.00001,100,100)
            mult1 = np.multiply(rhos[i], np.cos(taus))
            mult2 = np.multiply.outer(rs, mult1)
            
            diff = np.subtract(psis[i], taus)
            mult_diff = np.multiply.outer(1/rs, diff)
            # print(np.shape(mult_diff))
            mult_tot = np.multiply(mult_diff, rs[:,np.newaxis])
            
            add1 = np.add(mult2, mult_tot)
            add2 = np.subtract(mult2, mult_tot)
            vals1 = np.exp(-I*add1)
            vals2 = np.exp(-I*add2)
            vals = vals1+vals2
            print(np.shape(vals), i)
        

        return 1/(rho**2) * 0.5*2*np.pi / (2) * (2) * 0.5#2.6133200941251045
        
        
    
    def _imageFunction(self,xx,yy,wl):
        comp1 = self.components[0]
        comp2 = self.components[1]
        K = 2/3 * np.log(2)* (comp1.params['fwhm'](wl)/comp2.params['fwhm'](wl))**2
        # K=1
        # K=1
        r = np.sqrt(xx**2 + yy**2)
        rlim = self.rlim(wl) * self.rlim_fact
        
        # print("rlim", rlim)
        theta = np.arctan2(yy,xx)
        xlim = rlim * np.cos(theta)
        ylim = rlim * np.sin(theta)
        
        new_xx = xx - xlim
        new_yy = yy - ylim
        
        idx_low = np.where(r<rlim)
        idx_up = np.where(r>=rlim)
        
        results = np.zeros(np.shape(r))
        if not np.shape(idx_low)[1] == 0:
            # print(1, np.shape(idx_low))
            # rescale = comp1._imageFunction(0,0,wl)
            rescale=1
            idx = idx_low
            results[idx] = K * 1/rescale * comp1._imageFunction(new_xx[idx], new_yy[idx], wl)
            
        if not np.shape(idx_up)[1] == 0:
            rescale=1
            idx = idx_up
            results[idx] = 1/rescale * comp2._imageFunction(new_xx[idx], new_yy[idx] , wl)
            
        return results


###############################################################################   
###############################################################################   
          
class oim_model:

    def __init__(self,components=[]):
        self.components=components
        self.params=self.getParameters()
        self.type="model"
        
    def getComplexVisibility(self,spafreqs,wl=None):
        ftot=0
        res=complex(0,0)
        for c in self.components:
            fi=c.params["f"](wl)
            res+=c.getComplexVisibility(spafreqs,wl)*fi
            ftot+=fi
            
        return res/ftot
    
    def getImage(self,dim,pixsize,wl=None, fromTF = False):
        
        if fromTF:
            raise ValueError("TODO")
            #TODO
            return 0
        #Used if wl is a scalar
        wl=np.array(wl)
        nwl=np.size(wl)
        #wl=np.reshape(wl,nwl)
     
        if np.shape(wl)==():
            image=np.zeros([dim,dim])
            for c in self.components:
                image+=c.getImage(dim,pixsize,wl)
        else:
            #TODO : this is very slow!!!
            image=np.zeros([nwl,dim,dim])
            for iwl,wli in enumerate(wl):
                for c in self.components:
                    image[iwl,:,:]+=c.getImage(dim,pixsize,wli)
        
        
        
        return image
    
    def getParameters(self, only_free = False):
        
        
        """
        for comp in self.components:
            params = com.getParameters()
            self.params
        """
        # self.params={}
        params_mod = {}
        for i,c in enumerate(self.components):
            params = c.getParameters(only_free = only_free)
            for key in params:
                param_obj = params[key]
                if param_obj in params_mod.values():
                    continue
                params_mod["c{0}_{1}_{2}".format(i+1, c.shortname.replace(" ", "_"), key)] = param_obj
        if not only_free:
            self.params = params_mod
        return params_mod
        
        # for i,c in enumerate(self.components):
        #     for name,param in c.params.items():
        #         if param in self.params.values():
        #             pass
        #         else:
        #             if isinstance(param.value,oim_interp1d):
        #                 for iy,yi in enumerate(param.value._y):
        #                      # self.params["{0}_{1}_interp{2}".format(name,i+1,iy+1)]=yi
        #                      self.params["c{0}_{1}_{2}_interp{3}".format(i+1, c.shortname.replace(" ", "_"), name, iy+1)]=yi
        #             # elif hasattr(param.value, "__iter__"):
        #             #     for pi in param.value:
        #             #         self.params["c{0}_{1}_{2}".format(i+1, c.shortname.replace(" ", "_"), name+str(pi))]=param
        #             else:
        #                 # self.params["{0}_{1}".format(name,i+1)]=param
        #                 self.params["c{0}_{1}_{2}".format(i+1, c.shortname.replace(" ", "_"), name)]=param
                    
                             
        return self.params
    
    def getFreeParameters(self):
        
        return self.getParameters(only_free = True)
        # freeParam={}
        # for i,c in enumerate(self.components):
        #     for name,param in c.params.items():
        #         if not(param in freeParam.values()):
        #             if  isinstance(param.value,oim_interp1d):
        #                 for iy,yi in enumerate(param.value._y):
        #                     if yi.free==True:
        #                         # freeParam["{0}_{1}_interp{2}".format(name,i+1,iy+1)]=yi
        #                         freeParam["c{0}_{1}_{2}_interp{3}".format(i+1, c.shortname.replace(" ", "_"), name, iy+1)]=yi
        #             elif param.free==True:
        #                 # freeParam["{0}_{1}".format(name,i+1)]=param
        #                 freeParam["c{0}_{1}_{2}".format(i+1, c.shortname.replace(" ", "_"), name)]=param
        # return freeParam

    def setDefaults(self):
        for key in self.getFreeParameters():
            param = self.getFreeParameters()[key]
            if not hasattr(param,'template'):
                continue
            tplt = param.template
            if tplt == "fwhm":
                param.min = 0
                param.max = 100
            if tplt == "diameter":
                param.min = 0
                param.max = 50
            if tplt == "flux":
                param.min = 0
                param.max = 50
            if tplt == "pa":
                param.min = -90
                param.max = 90
            if tplt == "elong":
                param.min = 1
                param.max = 10
            if tplt == "skw":
                param.min = -1
                param.max = 1
            
    
    def _todict(self):
        def rec(obj):
            if hasattr(obj,"components"):
                temp = {}
                for comp in obj.components:
                    temp[type(comp).__name__] = rec(comp)
                    temp["type"] = "composite"
                    
            else:
                temp = obj._savedict()
                    
            return temp
        
        t = {}
        for comp in self.components:
            t[type(comp).__name__] = rec(comp)
        
        return t
        
    def save(self, savename = "model.json"):
        t = self._todict()
        
        utils.dict_to_json(t, savename, errors_to_str = True)
        # return t
      
    def _loaddict(self, dct, orikey):
        if dct["type"] == "composite":# and dct["type"] == True:
            to_conv = []
            for key in dct:
                print(key)
                if key != "type":
                    to_conv.append(self._loaddict(dct[key], key))
            print(to_conv)
            model_components = eval(orikey)(to_conv)
         
        else:
            comp = oim_component()._loaddict(dct, orikey)
            model_components = comp
        
        return model_components
        
        
        
    def load(self, filename):
        with open(filename, "r") as f:
            dct = json.load(f, object_hook = utils.custom_json_hook)
        model_components = []
        for key in dct:
            model_components.append(self._loaddict(dct[key], key))
        self.__init__(model_components)
        return self
    
    
    
    def toPandas(self, to_get = "value", params = None, ignore = None, only_free = True, mode = "errors"):
        """
        Makes a Pandas DataFrame from the model
        Originaly intended to then be exported to a Latex Table
        """

        
        comp_dfs = []
        for comp in self.components:
            comp_dfs.append(comp._toPandas(to_get = to_get, only_free = only_free, only_params = params, ignore = ignore, mode = mode))
        
        df = utils.concat(comp_dfs, axis = 1)
        
        
        return df
    
    
    
    def __str__(self):
        txt="OI model containing\n"
        for c in self.components:
            txt+=c.__str__()+"\n"
        return txt
    
      
    def __add__(self,componentOrModel):
        c=[]
        c.extend(self.components)

        if componentOrModel.type=="model":  
            print("model")
            c.extend(componentOrModel.components)
        else:
            c.append(componentOrModel)
        return oim_model(c)


    def __mul__(self,NumberComponentOrModel):
        c=[]
        c.extend(self.components)
        
        if isinstance(NumberComponentOrModel, (int,float)):
            for c in self.components:
                c.params["f"].value*=NumberComponentOrModel
        else:
            #TODO
            pass
        return self
    __rmul__ = __mul__
      
    
###############################################################################   
###############################################################################       
###############################################################################   
###############################################################################  

_fit_quantities=["VIS2DATA","VISAMP","VISPHI","T3AMP","T3PHI","FLUXDATA"]
_fit_errors=["VIS2ERR","VISAMPERR","VISPHIERR","T3AMPERR","T3PHIERR","FLUXERR"]
_fit_default=[1,0,1,0,1,0]
_fit_array=["OI_VIS2","OI_VIS","OI_VIS","OI_T3","OI_T3","OI_FLUX"]              
_fit_type_name=["Non","Absolute","Differential"]       
_fit_type_short=[0,1,2]

class _fitting:
    def __init__(self,data):
        extnames=[di.name for di in data]
        for i in range(len(_fit_quantities)):
           self.__dict__[_fit_quantities[i]]=0
           if _fit_array[i] in extnames:
               self.__dict__[_fit_quantities[i]]=_fit_default[i]
  
    def __str__(self):
        str=""
        for i in range(len(_fit_quantities)):
            j=self.__dict__[_fit_quantities[i]]
            str+="{0}={1} ".format(_fit_quantities[i],_fit_type_name[j])
        return str


   
class oim_simulator:

    def __init__(self,oifits=None,model=None,fitter=None):
        self.data=[]
        self.simulatedData=[]        
        self.fit=[]

        self.setModel(model)
        if oifits!=None:
            self.addData(oifits)
            
        self.isComputed=False
        self.isReadyToCompute=False
        
        self.chi2List=[]

    def setModel(self,model=None):
        self.model=model
        self.isComputed=False
    
        
    def setFitter(self, f = "default", fitted_data = None, no_update = False, **kwargs):
        if f == "default":
            self.fitter = fitter.fitEMCEE()
        else:
            self.fitter = f
        
        if fitted_data is not None:
            self.fitted_data = fitted_data
        else:
            print("Fitted data is currently : {}".format(fitted_data))
        
        self.updateFitter(no_update = no_update, **kwargs)
        # self.fitter.Simulator = self
        
        
    
    
    
    def updateFitter(self, no_update = False, **kwargs):
        self.fitter.setData(self.data)
        self.fitter.setModel(self.model)
        self.fitter.fitted_data = self.fitted_data
        if not no_update:
            self.fitter.update(**kwargs)

    def addData(self,oifits=None):
        if type(oifits)==type(""):
            self.data.append(fits.open(oifits))
            self.simulatedData.append(fits.open(oifits))
            self.fit.append(_fitting(self.data[-1]))
        elif type(oifits)==type([]):
            for item in oifits:
                if type(item)==type(""):
                    self.data.append(fits.open(item))
                    self.fit.append(_fitting(self.data[-1]))
                    self.simulatedData.append(fits.open(item))
                else:
                    self.data.append(item)
                    self.fit.append(_fitting(item))
                    c=[]
                    for itemi in item:    
                        c.append(itemi.copy())
                    item_copy=fits.HDUList(c)
                    self.simulatedData.append(item_copy)
                    
        else:
            self.data.append(oifits)
            self.fit.append(_fitting(oifits))
            c=[]
            for itemi in oifits:               
                c.append(itemi.copy())

            item_copy=fits.HDUList(c)
            self.simulatedData.append(item_copy)

        self.isComputed=False            
        self.isReadyToCompute=False
        
    #def setFitter(self):
    #    self.fitter = fitter.fitEMCEE()



    def _prepareData(self):
        self.nuv=[]
        self.nwl=[]
        self.wlf=[]
        self.uf=[]
        self.vf=[]
        self.oit3uvidx=[]
        
        for idata in range(len(self.data)):
            extnames=[ext.name for ext in self.data[idata]]
            #TODO check all table 
            um=self.data[idata]['OI_VIS2'].data['UCOORD']
            vm=self.data[idata]['OI_VIS2'].data['VCOORD']
            wl=self.data[idata]['OI_WAVELENGTH'].data['EFF_WAVE']            
            self.nuv.append(np.size(um))
            self.nwl.append(np.size(wl))
            self.uf.append(np.outer(um,1./wl))
            self.vf.append(np.outer(vm,1./wl))
            self.wlf.append(np.outer(np.ones(np.size(um)),wl))
            
            
            
            #Really not nice
            if "OI_T3" in extnames:
                
                ncp=np.shape(self.data[idata]['OI_T3'].data['T3PHI'])[0]
                idxs=[]
                for icp in range(ncp):
                    #Tambouille pas très propre à reprendre
                    idx1=np.where(self.data[idata]['OI_VIS2'].data['UCOORD'] == self.data[idata]["OI_T3"].data['U1COORD'][icp])[0]
                    idx2=np.where(self.data[idata]['OI_VIS2'].data['UCOORD'] == self.data[idata]["OI_T3"].data['U2COORD'][icp])[0]
                    sta_idx1=self.data[idata]['OI_VIS2'].data['STA_INDEX'][idx1,0]
                    sta_idx2=self.data[idata]['OI_VIS2'].data['STA_INDEX'][idx2,1]
                    idx3= np.where((self.data[idata]['OI_VIS2'].data['STA_INDEX'][:,0]==sta_idx1) &
                                   (self.data[idata]['OI_VIS2'].data['STA_INDEX'][:,1]==sta_idx2) &
                                   (self.data[idata]['OI_VIS2'].data['MJD']==self.data[idata]["OI_T3"].data['MJD'][icp]))[0]
                    idxs.append([idx1,idx2,idx3])
                self.oit3uvidx.append(idxs)    
        
            
        self.isReadyToCompute=True    
                   
    def compute(self,precision=1,normalizeV2=False):
        
        if not(self.isReadyToCompute):
            self._prepareData()
        self.vcompl=[]
        for idata in range(len(self.data)):
            vcompl=self.model.getComplexVisibility([self.uf[idata],self.vf[idata]],self.wlf[idata])   
            if self.nwl[idata]!=1:
                vcompl=np.reshape(vcompl,[self.nuv[idata], self.nwl[idata]])
            else:
                vcompl=np.reshape(vcompl,[ self.nuv[idata]])
            self.vcompl.append(vcompl)
                      
        self.computeObservables(normalizeV2)
        self.isComputed=True

    def computeObservables(self,normalizeV2=False,normalizeV=False):
         for idata in range(len(self.data)):
            nextension=len(self.data[idata])

            for iext in range(1,nextension):
                extname=self.data[idata][iext].header['EXTNAME']

                if extname=="OI_VIS2":
                    self.simulatedData[idata][iext].data['VIS2DATA']=np.abs(self.vcompl[idata])**2
                    if normalizeV2:
                        for iB in range(self.nuv[idata]):
                            self.simulatedData[idata][iext].data['VIS2DATA'][iB,:]/= self.simulatedData[idata][iext].data['VIS2DATA'][iB,0]

                    self.simulatedData[idata][iext].data['VIS2ERR']*=0

                elif extname=="OI_VIS":
                    self.simulatedData[idata][iext].data['VISAMP']=np.abs(self.vcompl[idata])
                    self.simulatedData[idata][iext].data['VISAMPERR']*=0
                    
                    if normalizeV:
                        for iB in range(self.nuv[idata]):
                            self.simulatedData[idata][iext].data['VISAMP'][iB,:]/= self.simulatedData[idata][iext].data['VISAMP'][iB,0]

                    phi=np.rad2deg(np.angle(self.vcompl[idata]))
                    for i in range(self.nuv[idata]):
                        phi[i,:]=phi[i,:]-np.mean(phi[i,:])
                    self.simulatedData[idata][iext].data['VISPHI']=phi
                    self.simulatedData[idata][iext].data['VISAMPERR']*=0
                                       
                elif extname=="OI_T3":
    
                    ncp=np.shape(self.simulatedData[idata]['OI_T3'].data['T3PHI'])[0]
                    for icp in range(ncp):
                        idx1=self.oit3uvidx[idata][icp][0]
                        idx2=self.oit3uvidx[idata][icp][1]
                        idx3=self.oit3uvidx[idata][icp][2]

                        BS=self.vcompl[idata][idx1,:]*self.vcompl[idata][idx2,:]*np.conjugate(self.vcompl[idata][idx3,:])
                        self.simulatedData[idata][iext].data['T3PHI'][icp,:]=np.rad2deg(np.angle(BS))
                   
                elif extname=="OI_FLUX":
                    pass
                    # nflx=np.shape(self.simulatedData[idata]['OI_FLUX'].data['FLUXDATA'])[0]
                    # flx=interpolate.interp1d(self.wl,self.flux,kind='linear',bounds_error=False,fill_value="extrapolate")(self.wlf[idata])
                    # for iflx in range(nflx):
                    #     self.simulatedData[idata][iext].data["FLUXDATA"][iflx,:]=flx
                    #     self.simulatedData[idata][iext].data['FLUXERR']*=0


    def computeChi2(self,wlrange=[0,1e99],nfree=None):
        self.sim_mean=[]
        self.data_mean=[]
        if self.isComputed:
            self.chi2List=[]
            for idata in range(len(self.data)):
                self.sim_mean.append({})
                self.data_mean.append({})
                lam=self.data[idata]['OI_WAVELENGTH'].data['EFF_WAVE']
                idx=np.where((lam > wlrange[0]) & (lam < wlrange[1]))[0]
                
                if np.size(idx)>0:
                    if np.size(lam)>1:
                        for iquantity,quantity in enumerate(_fit_quantities):
                            fit=self.fit[idata].__dict__[quantity]
                            arr=_fit_array[iquantity]
                            err=_fit_errors[iquantity]
                            dim=np.size(np.shape(fit))
                            if dim==0:
                                if fit==1:
                                     self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:,idx]-
                                                           self.data[idata][arr].data[quantity][:,idx])**2/
                                                          self.data[idata][arr].data[err][:,idx]**2)
                                if fit==2:
                                     sim_mean=np.transpose(np.tile(np.mean(self.simulatedData[idata][arr].data[quantity][:,idx],axis=1),(np.size(idx),1)))
                                     data_mean=np.transpose(np.tile(np.mean(self.data[idata][arr].data[quantity][:,idx],axis=1),(np.size(idx),1)))
                                     self.sim_mean[idata][quantity]=sim_mean
                                     self.data_mean[idata][quantity]=data_mean
                                     self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:,idx]/sim_mean-
                                                           self.data[idata][arr].data[quantity][:,idx]/data_mean)**2/
                                                          (self.data[idata][arr].data[err][:,idx]/data_mean)**2)
                    else:
                        for iquantity,quantity in enumerate(_fit_quantities):
                           fit=self.fit[idata].__dict__[quantity]
                           arr=_fit_array[iquantity]
                           err=_fit_errors[iquantity]
                           dim=np.size(np.shape(fit))
                           if dim==0:
                               if fit==1:
                                    self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:]-
                                                          self.data[idata][arr].data[quantity][:])**2/
                                                         self.data[idata][arr].data[err][:]**2)
                               if fit==2:
                                    sim_mean=np.transpose(np.tile(np.mean(self.simulatedData[idata][arr].data[quantity][:],axis=1),(np.size(idx),1)))
                                    data_mean=np.transpose(np.tile(np.mean(self.data[idata][arr].data[quantity][:],axis=1),(np.size(idx),1)))
                                    self.sim_mean[idata][quantity]=sim_mean
                                    self.data_mean[idata][quantity]=data_mean
                                    self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:]/sim_mean-
                                                          self.data[idata][arr].data[quantity][:]/data_mean)**2/
                                                         (self.data[idata][arr].data[err][:]/data_mean)**2)
      
        chi2=0
        nel=0
        for c in self.chi2List:
            nel+=np.size(c)
            chi2+=np.nansum(c)
        self.chi2r=chi2/nel
        return self.chi2r
                                    
        
    def runFit(self):
        if self.fitter.is_init == False:
            raise ValueError("Fitter has not been initialized yet and therefore is not ready for a run.")
        
        self.fitter.run()
            

    def getBestFit(self, **kwargs):
        self.best_fit = self.fitter.getBestFit(**kwargs)
        # Update model with best fit parameters
        for key in self.best_fit:
            self.model.params[key].value = self.best_fit[key][0]
            self.model.params[key].error = (self.best_fit[key][1], self.best_fit[key][2])
            
        return self.best_fit

    def fit(self,tol=1e-8):
        params=self.model.getParameters()
        pValues=[]
        pMax=[]
        pMin=[]
        for key,p in params.items():
            if p.free==True:
                pValues.append(p.value)
                pMax.append(p.max)
                pMin.append(p.min)     

    def save(self, directory,
             overwrite = False,
             simObjectName = "simulator.dill",
             saveSimulatedData = False,
             ignoreObject = True):
        """Saves all interesting stuff to a directory
            To SAVE :
                - SimulatedData
                - Best Fit
                - Call sim.fitter.save(directory) to save fitter-dependant data
                - plot of data and best fit model
                """
        cwd = os.getcwd()
        os.chdir(directory)
        
        # Save SimulatedData
        if not self.isComputed:
            while True:
                c = input("Simulated Data has not been computed yet. Do you wish to compute it now ? (y/n)\n")
                if c == "y":
                    self.compute()
                    break
                elif c == "n":
                    break
        
        # Using 'if' and not 'elif' on purpose
        if self.isComputed and saveSimulatedData:
            for dat, simDat in zip(self.data, self.simulatedData):
                savename = dat.filename()
                savename = "simDat_" + savename
                simDat.writeto(savename, overwrite = overwrite)
        
        
        # Save best fit 
        try:
            best_fit_savename = "best_fit_model.json"
            self.fitter.model = self.model
            chi2 = self.fitter.computechi2(reduce = True)
            to_save = self.best_fit
            to_save["chi2r"] = chi2
            json.dump(to_save, open(best_fit_savename, 'w'), sort_keys = False, indent = 4)
        except:
            print("Couldn't save 'best_fit'")
        
        # Save model
        self.model.save()
        
        # Save fitter dependant data
        self.fitter.saveImages(overwrite = overwrite)
        self.fitter.save(ignoreObject = True)
        
        
        # Save the plot of data and best fitted model
        
        
        # Save the simulator object itself to dill
        if overwrite == False and simObjectName in os.listdir():
            print("Not overwritting '{}'".format(simObjectName))
        else:
            if not ignoreObject:
                with open(simObjectName, "wb") as dill_file:
                    dill.dump(copy.deepcopy(self), dill_file)
        
        
        
        # Saving used files names
        with open("simulator.dat", "w") as f:
            for dat in self.data:
                f.write(dat.filename() + "\n")
        
        
        os.chdir(cwd)
        return 0
                
    
    
    def plotData(self, pdat = None, use_flags = True, **kwargs):
        if pdat is None:
            pdat = self.fitted_data
            if type(pdat) == type(""):
                pdat = [pdat]
        elif type(pdat) == type(""):
            pdat = [pdat]
        for fdat in pdat:
            if 'ylim' not in kwargs:
                if "PHI" in fdat:
                    ylim = (-190,190)
                elif fdat == "VISAMP" or fdat == "VIS2DATA":        
                    ylim = (-0.1, 1.1)
            else:
                ylim = kwargs['ylim']
                del kwargs['ylim']
            
            ut.newPlot(self.data, "SPA_FREQ", fdat,  ylim = ylim, use_flags = use_flags, **kwargs)
            plt.show()
    
    
    def plotDataModel(self, pdat = None, use_flags = True, save = False, lw = 3, alpha = 0.6, **kwargs):
        if pdat is None:
            pdat = self.fitted_data
            if type(pdat) == type(""):
                pdat = [pdat]
        elif type(pdat) == type(""):
            pdat = [pdat]
        for fdat in pdat:
            # print(fdat, pdat)
            if 'ylim' not in kwargs:
                if "PHI" in fdat:
                    ylim = (-190,190)
                elif fdat == "VISAMP" or fdat == "VIS2DATA":        
                    ylim = (-0.1, 1.1)
            else:
                ylim = kwargs['ylim']
                del kwargs['ylim']
            ut.newPlot(self.data, "SPA_FREQ", fdat,  ylim = ylim, use_flags = use_flags, **kwargs)
            ut.newPlot(self.simulatedData, "SPA_FREQ", fdat,  ylim = ylim, color = "black", errorbars = False, overlap = True, alpha = alpha, lw = lw, **kwargs)
            # plt.show()
            
            if save == False:
                plt.show()
            else:
                plt.savefig(fdat + ".png")

            
    def plotOrientationDensity(self, arr = "OI_VIS", **kwargs):
        ut.getOrientationDensity(self.data, arr, **kwargs)






def rebuild_simulator(data_dir = "", band = "L", fitted_data = "VISAMP", verbose = True):
    mod = oim_model().load("model.json")
    # return mod
    
    # Data
    files = []
    with open("simulator.dat", "r") as f:
        for line in f:
            # print(line)
            if line.endswith(".fits\n"):
                files.append(line.replace("\n", ""))
    files = [data_dir + f for f in files]
    # print("---")
    
    if verbose:
        print(str(len(files)) + " files")
    
    
    lams = [ut.getBandsLimits()[band][0], ut.getBandsLimits()[band][1]]
    ois = []
    for file in files:
        oi = ut.cutWavelengthRange(fits.open(file), lams)
        ois.append(oi)
    
    
    sim = oim_simulator(ois)
    # sim.model = mod
    sim.setModel(mod)
    sim.setFitter(fitted_data = fitted_data, no_update = True)
    sim.fitter.load(oifits = "skip", dirty_load = True, verbose = verbose)
    
    sim.compute()
    return sim
