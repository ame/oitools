# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 09:36:23 2021

@author: Ame
"""


from . import cubetools
from . import oimodel
from . import oifitstools
from . import utils


from os.path import join, dirname, split
import inspect

__pkg_dir__ = dirname(inspect.getfile(inspect.currentframe()))


if split(__pkg_dir__)[-1] == "":
    __git_dir__ = dirname(split(__pkg_dir__)[0])
else:
    __git_dir__ = split(__pkg_dir__)[0]

