# -*- coding: utf-8 -*-
"""
Created on Thu May  6 14:13:54 2021

@author: Dorian
"""



import numpy as np
import emcee
import matplotlib.pyplot as plt
import corner
import dill
import copy
import json
from scipy.optimize import curve_fit
from astropy import units as u
from copy import deepcopy

from . import utils as ut
from . import oifitstools as oiut


"""
Fitter class,
should contain stuff
"""

I = np.complex(0,1)



class Fitter:
    
    """
    Base Fitter class.
    Meant to be subclassed for each specific fitter. See fitEMCEE for a subclass example.
    Contains basic methods to format/flatten the oifits data.
    """
    
    def __init__(self, **kwargs):
        
        self.fitted_data = "VISAMP"
        self.calcMethod = {"VISAMP": self.computeVISAMP,
                           "VISPHI": self.computeVISPHI,
                           "VIS2DATA": self.computeVIS2DATA,
                           "T3PHI": self.computeVISPHI}
        
        self.flaggedOut = False
    
    
    def computeVISAMP(self, viscomp):
        """
        Return the absolute value of the complex visibility
        """
        return np.abs(viscomp)
    
    def computeVISPHI(self, viscomp):
        """
        Returns the angle of the complex visibility
        """
        return np.angle(viscomp, deg = True)
    
    def computeVIS2DATA(self, viscomp):
        """
        Returns the squared absolute value of the complex visibility
        """
        return np.abs(viscomp)**2

    def setModel(self, model):
        """
        Sets the model
        """
        self.model = model

    def setData(self, data):
        """
        Sets the data
        """
        self.data = data

    def debugSkipNanZeros(self):
        """
        Used for debugging.
        Replaces 'NaN' values in the data with default values
        
        """
        self.test = {}
        for key in self.flattenData:
            if key == 'VISAMPERR' or key == 'VIS2DATAERR':
                val = 0.05
            elif key == "VISPHIERR" or key == "T3PHIERR":
                val = 2
            else:
                val = 0
            if key not in ('VISAMPERR', 'VIS2DATAERR', 'VISPHIERR', 'T3PHIERR'):
                continue
            idx = np.where(np.isnan(self.flattenData[key]))[0]
            self.flattenData[key][idx] = np.add(np.dot(self.flattenData[key.replace("ERR", "")][idx], 0), val)
            
            if "ERR" in key:
                idx = np.where(self.flattenData[key] == 0)[0]
                self.flattenData[key][idx] = self.flattenData[key.replace("ERR", "")][idx]

    def computeSpaFreqs(self):
        """
        Compute a list of spfs to pass to the model
        """
        #TODO Voir pour éliminer les doublons entre les tables de OI_VIS, OI_VIS2 et OI_T3
        #TODO est-ce que c'est vraioment mieux ?
        
        #Get wavelengths from data
        wls=  []
        for i in range(len(self.data)): # iterate over the number of files that there are in the data
            wl = oiut.get_data(self.data[i], "EFF_WAVE")
            wls.append(np.array(wl))
        
        self.wl = wls
        
        
        self.spfs = {}
        
        # Get SpaFreqs for OI_VIS
        Us = {}
        Vs = {}
        visamp_spf = []
        for i in range(len(self.data)): # iterate over the number of files that there are in the data
            Us = self.data[i]["OI_VIS"].data["UCOORD"]
            Vs = self.data[i]["OI_VIS"].data["VCOORD"]
            
            spaU = [np.divide(u, self.wl[i]) for u in Us]   # Compute U_spf for each baseline and each wavelength
            spaV = [np.divide(v, self.wl[i]) for v in Vs]   # Compute V_spf for each baseline and each wavelength
            visamp_spf.append(np.array([spaU, spaV]))
        
        
        self.spfs["OI_VIS"] = visamp_spf
        
        
        # Get SpaFreqs for the OI_VIS2 ext
        Us = {}
        Vs = {}
        vis2data_spf = []
        for i in range(len(self.data)): # iterate over the number of files that there are in the data
            Us = self.data[i]["OI_VIS2"].data["UCOORD"]
            Vs = self.data[i]["OI_VIS2"].data["VCOORD"]
            
            spaU = [np.divide(u, self.wl[i]) for u in Us]   # Compute U_spf for each baseline and each wavelength
            spaV = [np.divide(v, self.wl[i]) for v in Vs]   # Compute V_spf for each baseline and each wavelength
            vis2data_spf.append(np.array([spaU, spaV]))
        
        
        self.spfs["OI_VIS2"] = vis2data_spf
        
        
        # Get SpaFreqs for "OI_T3"
        """
            For 'OI_T3', the spfs will have the shape :
                 [ [ [U1s], [V1s] ],
                   [ [U2s], [V2s] ],
                   [ [U3s], [V3s] ] ]
        """
        t3amp_spf = []
        for i in range(len(self.data)):
            U1s = self.data[i]["OI_T3"].data["U1COORD"]
            U2s = self.data[i]["OI_T3"].data["U2COORD"]
            U3s = [-(u1 + u2) for u1,u2 in zip(U1s, U2s)]
            V1s = self.data[i]["OI_T3"].data["V1COORD"]
            V2s = self.data[i]["OI_T3"].data["V2COORD"]
            V3s = [-(v1 + v2) for v1,v2 in zip(V1s, V2s)]
            
            spaU1 = [np.divide(u1, self.wl[i]) for u1 in U1s]
            spaU2 = [np.divide(u2, self.wl[i]) for u2 in U2s]
            spaU3 = [np.divide(u3, self.wl[i]) for u3 in U3s]
            spaV1 = [np.divide(v1, self.wl[i]) for v1 in V1s]
            spaV2 = [np.divide(v2, self.wl[i]) for v2 in V2s]
            spaV3 = [np.divide(v3, self.wl[i]) for v3 in V3s]
            
            t3amp_spf.append([[spaU1, spaV1], [spaU2, spaV2], [spaU3, spaV3]])
        self.spfs["OI_T3"] = t3amp_spf
        
    
    def makeSimpleData(self):
        """
        Simplifies the data structure.
        Get each type of data from the self.data so it lowers the computing during the fit
        """
        
        keys= ["VISAMP", "VIS2DATA", "VISPHI", "VISAMPERR", "VIS2ERR", "VISPHIERR", "T3PHI", "T3PHIERR"]
        flags_keys = ["OI_VIS", "OI_VIS2", "OI_T3"]
        self.simpleData = {}
        self.flags = {}
        
        for key in keys:
            self.simpleData[key] = []
        for key in flags_keys:
            self.flags[key] = []
        
        # make data
        for i in range(len(self.data)):
            for key in self.simpleData:
                self.simpleData[key].append(oiut.get_data(self.data[i], key))
        
        # make flags
        for i in range(len(self.data)):
            for key in self.flags:
                self.flags[key].append(self.data[i][key].data["FLAG"])
    
    
    def makeFlattenData(self):
        """
        Flattens the spfs and data to be both easier and faster to use in the 'logLikelihood' method
        """
        
        # Flattening Spfs
        self.flatten_spfs = {}
        for key in self.spfs:
            
            # Special case for OI_T3
            if key == "OI_T3":
                """
                For 'OI_T3', the spfs will have the shape :
                     [ [ [U1s], [V1s] ],
                       [ [U2s], [V2s] ],
                       [ [U3s], [V3s] ] ]
                """
                self.flatten_spfs[key] = [ [ [], [] ],
                                           [ [], [] ],
                                           [ [], [] ] ]
                
                for i in range(len(self.spfs[key])):
                    temp1 = np.array(self.spfs[key][i][0]).reshape(2, -1)
                    temp2 = np.array(self.spfs[key][i][1]).reshape(2, -1)
                    temp3 = np.array(self.spfs[key][i][2]).reshape(2, -1)
                    self.flatten_spfs[key][0][0] = np.append(self.flatten_spfs[key][0][0], temp1[0])
                    self.flatten_spfs[key][0][1] = np.append(self.flatten_spfs[key][0][1], temp1[1])
                    self.flatten_spfs[key][1][0] = np.append(self.flatten_spfs[key][1][0], temp2[0])
                    self.flatten_spfs[key][1][1] = np.append(self.flatten_spfs[key][1][1], temp2[1])
                    self.flatten_spfs[key][2][0] = np.append(self.flatten_spfs[key][2][0], temp3[0])
                    self.flatten_spfs[key][2][1] = np.append(self.flatten_spfs[key][2][1], temp3[1])
                self.flatten_spfs[key] = np.array(self.flatten_spfs[key])
                continue
            
            self.flatten_spfs[key] = [[],[]]
            for i in range(len(self.spfs[key])):
                temp = np.array(self.spfs[key][i]).reshape(2, -1)
                self.flatten_spfs[key][0] = np.append(self.flatten_spfs[key][0], temp[0])
                self.flatten_spfs[key][1] = np.append(self.flatten_spfs[key][1], temp[1])
            self.flatten_spfs[key] = np.array(self.flatten_spfs[key])
        
        
        
        # Flattening data
        self.flattenData = {}
        for key in self.simpleData:
            self.flattenData[key] = np.array([])
            for i in range(len(self.simpleData[key])):
                temp = np.array(self.simpleData[key][i]).reshape(-1)
                self.flattenData[key] = np.append(self.flattenData[key], temp)
    
    
    
        # Flattens Flags
        self.flattenFlags = {}
        for key in self.flags:
            self.flattenFlags[key] = np.array([])
            for i in range(len(self.flags[key])):
                temp = np.array(self.flags[key][i]).reshape(-1)
                self.flattenFlags[key] = np.append(self.flattenFlags[key], temp)
    
    
        # Flattens Wavelengths
        self.flatten_wl = {}
        for key in self.spfs:
            if key == "OI_T3":
                l = []
                for i in range(len(self.wl)):
                    l.append(np.outer(np.zeros(len(self.simpleData["T3PHI"][i]))+1,self.wl[i]).flatten())
            else:
                l = []
                for i in range(len(self.wl)):
                    l.append(np.outer(np.zeros(len(self.simpleData["VISAMP"][i]))+1,self.wl[i]).flatten())
                    
            fwl = []
            for i in range(len(l)):
                fwl = np.append(fwl, l[i])
            self.flatten_wl[key] = fwl
    
    def flagOutFlattenData(self, update = False):
        """
        Removes flagged data from the flatten data.
        Returns an error if the data was already flagged out once.
        """
        if self.flaggedOut == True and update == False:
            raise AttributeError("Flagged data was already excluded")
        
        for key in self.flatten_spfs:
            idx = np.where(self.flattenFlags[key] == False)[0]
            if key == "OI_T3":
                self.flatten_spfs[key] = self.flatten_spfs[key][:,:,idx]
            else:
                self.flatten_spfs[key] = self.flatten_spfs[key][:,idx]
            
        for key in self.flattenData:
            idx = np.where(self.flattenFlags[oiut.keys_links[key]] == False)[0]
            self.flattenData[key] = self.flattenData[key][idx]
        
        for key in self.flatten_wl:
            idx = np.where(self.flattenFlags[key] == False)[0]
            self.flatten_wl[key] = self.flatten_wl[key][idx]
        
        self.flaggedOut = True
    
    
    def makeDataPerWavelength(self):
        """
        Unused now
        """
        # Makes data per wavelength for each file
        dic = {}
        for key in self.simpleData:
            full = []
            for i in range(len(self.simpleData[key])):
                arr = []
                for j in range(len(self.wl[i])):
                    arr.append(self.simpleData[key][i][:,j])
                full.append(np.array(arr))
            dic[key] = full
        self.simplePerWl = dic
    
        dic = {}
        for key in self.spfs:
            full = []
            for i in range(len(self.spfs[key])):
                arr = []
                for j in range(len(self.wl[i])):
                    arr.append(self.spfs[key][i][:,:,j])
                full.append(np.array(arr))
            dic[key] = full
            
        self.spfsPerWl = dic


    def save(self, filename = "fitter.dill"):
        """
        Default saving method, will just save the object to a dill object.
        Note that dill objects can have a size up to several Mo's.
        It is much more efficient to define a fitter-specific saving method. See 'fitEMCEE.save' for an example.
        Note that using dill objects will most likely lead to compatibility issues between different versions of the code.
        
        Parameters
        ----------
        filename : 'str', optional
            File name to save the dill object to. The default is "fitter.dill".
        """

        with open(filename, "wb") as dill_file:
            dill.dump(copy.deepcopy(self), dill_file)
    
    def load(self, filename = "fitter.fill"):
        """
        Default loading method
        
        Parameters
        ----------
        filename : 'str', optional
            Name of the dill file to be loaded. The default is "fitter.fill".

        Returns
        -------
        Unpickled dill object.

        """
        with open(filename, 'rb') as pickle_file:
            self = dill.load(pickle_file)
        return self


class fitEMCEE(Fitter):
    
    """
    Subclassed 'Fitter' class.
    Uses the 'emcee' package to fit the data
    
    Parameters
    ----------
    nwalkers : 'int', optional
        Number of walkers. The default is 32.
    nsteps : 'int', optional
        Number of steps. The default is 10000.
    """
    
    def __init__(self, nwalkers = 32, nsteps = 10000, **kwargs):
        self.nwalkers = nwalkers
        self.nsteps = nsteps
        self.discard = None
        self.last_params = {}
        self.backend = None
        
        self.dirty_load = False
        
        self.is_init = False
        super().__init__()
    
    def run(self, backend = None):
        """
        Runs the EMCEE fit.

        Parameters
        ----------
        backend : None or 'string', optional
            If not None, will use a EMCEE backend to save the data to.
            Note that using a backend really slows down the fit.
            The default is None.

        Raises
        ------
        ValueError
            Raises ValueError if the fitter was not initialized with the update() method.
        """
        
        if self.is_init == False:
            raise ValueError("Please run the self.update() method before running the fit")
        
        self.backend = backend
        
        if backend is not None:
            backend = emcee.backends.HDFBackend(backend)
            backend.reset(self.nwalkers, self.ndim)
        self.sampler = emcee.EnsembleSampler(self.nwalkers, self.ndim, self.logProbability, args = self.LogProbArgs, backend = backend)
        self.sampler.run_mcmc(self.init, self.nsteps, progress=True)
    
    
    def resume(self, nsteps):
        """
        Restarts the MCMC where it stopped.

        Parameters
        ----------
        nsteps : int
            Number of iterations during which to pursue the MCMC.
        """

        self.sampler.run_mcmc(None, nsteps, progress = True)
    
    
    def update(self,
               flagOut = True,
               nsteps = None,
               nwalkers = None,
               random = True):
        """
        Update the fitter with all required data/parameters.
        In order :
                - Makes priors
                - Makes flatten data using the Fitter base class
                - Flags out flagged data (by default)
                - Makes init parameters. The user can override it by overriding the 'init' attribute
                - Some fitter-specific things that are probably not worth listing

        Parameters
        ----------
        flagOut : bool, optional
            True/False, Flags out flagged data. The default is True.
        nsteps : int, optional
            If given, overrides the number of steps given at object creation. The default is None.
        nwalkers : int, optional
            DESCRIPTION. The default is None.
        random : TYPE, optional
            If given, overrides the number of steps given at object creation. . The default is True.

        """
        
        
        
        self.last_update_params = {"flagOut": flagOut,
                                    "nsteps": nsteps,
                                    "nwalkers": nwalkers,
                                    "random": random}
        if nsteps is not None:
            self.nsteps = nsteps
        if nwalkers is not None:
            self.nwalkers = nwalkers
            
        self._get_ndim()
        self._getLogProbArgs()
        self.makePriors()
        self.computeSpaFreqs()
        self.makeSimpleData()
        self.makeFlattenData()
        if flagOut == True:
            self.flagOutFlattenData(update = True)
        # self.makeFlattenWl()
        self.makeInit(random = random)
        # self.makeDataPerWavelength()
        self.makeLabels()
        if type(self.fitted_data) == type(""):
            self.fitted_data = [self.fitted_data]
        
        
        self.freeKeys = [key for key in self.model.getFreeParameters()]
        
        self.is_init = True
        
        

    def makeInit(self, random = True):
        """
        Makes the initial MCMC state.
        By default, randomly places walker in every parameter space (if the space does not have an infinite lower/upper bound).

        Parameters
        ----------
        random : bool, optional
            True/False Randomly places walker in the space. The default is True.

        """

        #TODO Laisser le choix à l'utilisateur de la valeur initiale
        #TODO voir l'initialisation
        init = []
        p = self.priors
        # count = 0
        for i in range(self.nwalkers):
            param = []
            for key in p:
                # Initiating at the middle between upper and lower limits
                low, up = p[key]
                
                if not np.isfinite(low) and not np.isfinite(up):
                    param.append(0)
                elif random == True:
                    param.append(np.random.random() * (up-low) + low)
                elif not np.isfinite(low):
                    param.append(up*2 -5)
                elif not np.isfinite(up):
                    param.append(low*2 +5)
                else:
                    param.append((up + low)/2)
            init.append(param)
            
        self.init = np.array(init)
    
    
    def _get_ndim(self):
        """
        Gets the number of free parameters
        """
        ndim = len(self.model.getFreeParameters())
        self.ndim = ndim
        
        
    
    
    def _getLogProbArgs(self):
        """
        Creates the LogProbArgs attribute.
        I know, that is quite an useless method.
        """
        self.LogProbArgs = None
    
    
    def makeLabels(self):
        """
        Makes a list of free parameters to be used as labels for the graphs.
        """
        labels = [key for key in self.model.getFreeParameters()]
        self.labels = labels
        
        
    def makePriors(self):
        """
        Makes a list of priors for each free parameter, where the prior are the min and max limits for that parameter. They can be infinite.
        """
        priors = {}
        free_params = self.model.getFreeParameters()
        for key in free_params:
            priors[key] = (free_params[key].min, free_params[key].max)
        
        self.priors = priors
    
    
    
    def logProbability(self, theta):
        """
        logProbability, used in EMCEE.
        """
        fit_param = {}
        for key, val in zip(self.freeKeys, theta):
            fit_param[key] = val
        
        lp = self.logPrior(fit_param)
        if not np.isfinite(lp):
            return -np.inf
        
        return lp + self.logLikelihood(fit_param)
    
    def logPrior(self, fit_param):
        """
        logPrior, used in EMCEE.
        """
        for key in self.freeKeys:
            val = fit_param[key]
            low,up = self.priors[key]
            if not (low < val < up):
                return -np.inf
        
        return 0.0

    
    def logLikelihood(self, fit_param):
        """
        LogLikelihood function passed to EMCEE
        
        PARAMETERS :
            - fit_param : (list) list of str, where each content are the fitted parameters, for example ['VISAMP', 'VISPHI', 'OIT3', ...].
        """
        for key in fit_param:
            self.model.params[key].value = fit_param[key]
        
        chi2 = self.computechi2(reduce = True)

        return -0.5 * chi2
        
    
    def computechi2(self, reduce = False):
        """
        Computes the chi2 of the current model
        
        PARAMETERS :
            - reduce : (True/False) If False, computes the reduced chi2 (chi2 divided by number of points)
        """
        
        chi2 = 0
        n = 0
        for fdat in self.fitted_data:
            x = self.flatten_spfs[oiut.keys_links[fdat]]
            y = self.flattenData[fdat]
            if not fdat == "VIS2DATA":
                yerr = self.flattenData[fdat + "ERR"]
            else:
                yerr = self.flattenData["VIS2ERR"]
            lam = self.flatten_wl[oiut.keys_links[fdat]]
            
            if fdat == "T3PHI":
                viscomp = 1
                for u,v in x:
                    viscomp = np.multiply(viscomp, self.model.getComplexVisibility((u,v), lam))
                    diff =  np.angle(np.exp(I*np.deg2rad(y)) * np.conjugate(viscomp), deg = True)
            else:
                viscomp = self.model.getComplexVisibility(x, lam)
                mod= self.calcMethod[fdat](viscomp)
                diff = y-mod
            
            ch = np.nansum((diff)**2 / yerr**2)
            n += len(y[~np.isnan(yerr)])
            chi2 += ch
        
        if reduce == True:
            chi2 = np.divide(chi2,n)
        
        return chi2
    
    def plotChain(self, save = None, use_last_params = False, chain = None):
        """
        Plots the EMCEE chain, with y labels being the name of the parameters, and x being the iteration number.

        Parameters
        ----------
        save : string, optional
            Name of the file to save the plot to. The default is None.
        use_last_params : bool, optional
            True/False. If True, will plot the graph with the parameters used in the last function call.
        chain : numpy.ndarray, optional
            If given, will plot the chain using that chain instead of fitter one. The default is None.

        """
        
        labels = self.labels
        ndim = self.ndim
        fig, axes = plt.subplots(ndim, figsize=(15, 10), sharex=True)
        if chain is None:
            if not self.dirty_load:
                chain = self.sampler.get_chain()
            else:
                chain = self.dirty_chain
        
        if use_last_params == True:
            try:
                ndim = self.last_params['chain'][0]
                labels = self.last_params['chain'][1]
                chain = self.last_params['chain'][2]
            except:
                pass
            
        last_parameters = [ndim, labels, chain]
        self.last_params['chain'] = last_parameters
        
        if not ndim == 1:
            for i in range(ndim):
                ax = axes[i]
                ax.plot(chain[:, :, i], "k", alpha=0.1)
                ax.set_xlim(0, len(chain))
                h = ax.set_ylabel(labels[i], rotation = 75)
                ax.yaxis.set_label_coords(-0.1, 0.5)
            
            axes[-1].set_xlabel("step number")
        else:
            ax = axes
            ax.plot(chain[:, :, 0], "k", alpha=0.1)
            ax.set_xlim(0, len(chain))
            ax.set_ylabel(labels[0])
            ax.yaxis.set_label_coords(-0.1, 0.5)
            ax.set_xlabel("step number")
        
        if save is not None:
            plt.savefig(save, bbox_inches = 'tight')#, overwrite = self.overwrite)
            plt.close()
    
    def cornerPlot(self, discard = None,
                   thin = 15,
                   chi2max = None,
                   save = None,
                   use_last_params = False,
                   fontsize = None,
                   flat_samples = None,
                   mode = 'peak',
                   factor = 3,
                   zoom = None,
                   bins = 30,
                   plot_hists = False,
                   maxfev = 10000,
                   **kwargs):
        """
        Plots the Corner Plot corresponding to the fit.
        See Corner package documentation for more details.

        Parameters
        ----------
        discard : int, optional
            Discard the first N iterations of the chain, typically the burn-in phase. The default is None.
        thin : int, optional
            Uses only a thin-th of the samples. Used make lighter corner plots. The default is 15.
        chi2max : None/float/str, optional
            - If 'float' : only uses points that have a chi2 under the given value.
            - If 'str' : same as if 'float', but computes the value according to the string.
                            Example : 'min*3', 'min+3', 'max +12 - min +3' are all valid inputs
                                      'min*3' will compute the minimum value of chi2 +3
        save : str, optional
            Name of the file to save the plot to. The default is None.
        use_last_params : bool, optional
            True/False. If True, ignores all other given parameters and use the last given parameters. The default is False.
        fontsize : int, optional
            If given, size of the font on the plot. The default is None.
        flat_samples : numpy.ndarray, optional
            flatten samples to plot. If not given, will flatten the MCMC data. The default is None.
        mode : str, optional
            Either 'best', 'peak', or 'median'. See 'getBestFit' for more details. The default is 'peak'
                - 'best' : Finds the iteration with the lowest chi2 to plot on the corner plot and plots it in blue.
                - 'peak' : Fits 1D histograms with a two-sigmas gaussian. Plots in blue : Peak of the gaussian. Plots in purple : 'best'-mode result.
                - 'median' : Finds the median value for each parameter and plots it in blue. Errors are computed with the 16 and 84 percentiles.
        factor : int, optional
            Passed to getBestFit. The default is 3.
        zoom : float, optional
            Zooms the corner plot into a 'zoom' number of sigmas. The higher the 'zoom' parameter, the less zoomed it is. Only works with 'peak' mode. The default is None.
        bins : int, optional
            Number of bins on the 1D-Histograms. The default is 30.
        plot_hists : bool, optional
            True/False, Debugging purpose. The default is False.
        maxfev : int, optional
            Max number of iterations to fit the gaussian 1D-Histograms. Only works with 'peak' mode. The default is 10000.
        **kwargs : Keyword Arguments
            Passed to the 'corner.corner' function.

        """
        
        
        if use_last_params == True:
            try:
                flat_samples = self.last_params['corner'][0]
                labels = self.last_params['corner'][1]
                fontsize = self.last_params['corner'][2]
                mode = self.last_params['corner'][3]
                chi2max = self.last_params['corner'][4]
                discard = self.last_params['corner'][5]
                zoom = self.last_params['corner'][6]
                maxfev = self.last_params['corner'][7]
                factor = self.last_params['corner'][8]
                bins = self.last_params['corner'][9]
            except Exception as err:
                print("'cornerPlot' could not load parameters")
                print("Error : ", err)
        
        
        
        discard = self._discardError(discard)
        self.getBestFit(discard = discard, thin = thin, chi2max = chi2max, mode = mode, factor = factor, plot_hists = plot_hists, maxfev = maxfev)
        modelchi2 = self.computechi2(reduce = True)
        print("model chi2 = {}".format(modelchi2))
        bestvalues = [self.best_fit[key][0] for key in self.best_fit]
        
        if flat_samples is None:
            if not self.dirty_load:
                flat_samples = self.sampler.get_chain(discard=discard, thin=thin, flat=True)
            else:
                flat_samples = get_value(self.dirty_chain, True, thin, discard)
        
        if chi2max is not None:
            if not self.dirty_load:
                chi2s = -2 * self.sampler.get_log_prob(discard = discard, thin = thin, flat = True)
            else:
                chi2s = -2* get_value(self.dirty_logprob, True, thin, discard)
            
            if type(chi2max) == type(""):
                chi2max = chi2max.replace("max", "np.max(chi2s)")
                chi2max = chi2max.replace("min", "np.min(chi2s)")
                chi2max = eval(chi2max)
                # print("chi2max = {}".format(chi2max))
            
            if not self.dirty_load:
                flat_samples = self.sampler.get_chain(discard=discard, thin=thin, flat=True)
            else:
                flat_samples = get_value(self.dirty_chain, True, thin, discard)
            
            idx = np.where(chi2s <= chi2max)[0]
            print("Based on 'chi2max' parameter, plotting {} points out of {}".format(len(idx), len(flat_samples)))
            flat_samples = flat_samples[idx]
            
        labels = self.labels       
        
        
            
        last_parameters = [flat_samples, labels, fontsize, mode, chi2max, discard, zoom, maxfev, factor, bins]
        self.last_params['corner'] = last_parameters
        
        
        # Building 1D-Hists keywords
        hist_kwargs = {"density":False}
        # hist_kwargs["bins"] = bins
        
        
        therange = None
        if zoom is not None:
            if not hasattr(zoom, "__iter__"):
                new_range = []
                for key, vals in self.best_fit.items():
                    new_range.append((np.max((vals[0]+zoom*vals[1], self.model.params[key].min)), np.min((vals[0]+zoom*vals[2], self.model.params[key].max))))
                therange = new_range
            else:
                therange = zoom
            
        show_titles = False
        quantiles = None
        
        if mode == 'median':
            show_titles = True
            quantiles = (0.16,0.5, 0.84)
        
        fig = corner.corner(flat_samples, bins = bins, labels=labels, quantiles = quantiles, show_titles = show_titles, truths = bestvalues, range = therange, hist_kwargs = hist_kwargs, **kwargs)
        
        
        # Plotting fitted parameters on 1D-Hists
        if mode == 'best':
            lenmax = int(np.sqrt(len(fig.axes)))
            for i in range(lenmax):
                idx = int(i*(lenmax+1))
                ax = fig.axes[idx]
                key = self.labels[i]
                ax.title._text = "{} = ${:.3f}$".format(key, self.best_fit[key][0])
                
        if mode == "peak":
            lenmax = int(np.sqrt(len(fig.axes)))
            if therange == None:
                therange = [None for i in range(lenmax)]
            for i in range(lenmax):
                idx = int(i*(lenmax+1))
                ax = fig.axes[idx]
                
                
                # Rescale the plot to te current histogram size, by mutlitplying by ratio of bin sizes
                val = flat_samples[:,i]
                h, bin_corner = np.histogram(val, bins = bins, range = therange[i])
                rescale = (self.peak_plots[i][0][1]-self.peak_plots[i][0][0]) / (bin_corner[1]-bin_corner[0])
                #TODO
                # There is probably a better way to do this rescaling. This works well when the
                # graphs are quite gaussian, but this should be done by forcing the histogram
                # peak and the curve peak to be at the same height
                
                key = self.labels[i]
                # Plot the fitted histogram
                xs = self.peak_plots[i][0]
                ys = self.peak_plots[i][1] / rescale
                lims = (np.min(xs), np.max(xs))
                for slc in ut.unlink_wrap(xs,lims):
                    ax.plot(xs[slc], ys[slc], c = "red", alpha = 0.5)
                
                
                # Adding uncertainties lines
                
                ax.axvline(x= self.best_fit[key][0] - np.abs(self.best_fit[key][1]), color = "k", linestyle = "--", alpha = 0.7)
                ax.axvline(x= self.best_fit[key][0] + np.abs(self.best_fit[key][2]), color = "k", linestyle = "--", alpha = 0.7)
                
                # Adding title. That's a really dirty way to add the title... But it works !
                ax.title._text = "{} = ${:.3f}_{{-{:.3f}}}^{{+{:.3f}}}$".format(key, self.best_fit[key][0], np.abs(self.best_fit[key][1]), np.abs(self.best_fit[key][2]))
        
        
        
        to_plot = ""
        
        # Adding best_chi2 model to the plot
        if mode != "best":
            old_model = deepcopy(self.model)
            old_fit = deepcopy(self.best_fit)
            self.getBestFit(discard = discard, thin = thin, chi2max = chi2max, mode = "best", factor = factor, verbose = False)
            bestchi2 = bestchi2 = self.computechi2(reduce = True)
            bestvalues = [self.best_fit[key][0] for key in self.best_fit]
            self.model = old_model
            self.best_fit = old_fit
            
            ndim = len(self.labels)
            
            axes = np.array(fig.axes).reshape((ndim, ndim))
            
            # Loop over the diagonal
            for i in range(ndim):
                ax = axes[i, i]
                val = bestvalues[i]
                ax.axvline(val, color="purple")
            
            # Loop over the histograms
            for yi in range(ndim):
                for xi in range(yi):
                    ax = axes[yi, xi]
                    valv = bestvalues[xi]
                    valh = bestvalues[yi]
                    ax.axvline(valv, color="purple")
                    ax.axhline(valh, color="purple")
                    ax.plot(valv, valh, color = "purple", marker = "s")
            
            to_plot +=  "Best chi2 = {:.3f}\n".format(bestchi2)
            
            
            
            
        
        to_plot += "Model chi2 = {:.3f}\n".format(modelchi2)
        
        for key in self.best_fit:
            to_plot += "{} = {:.3f} ".format(key, self.best_fit[key][0])
            if type(self.best_fit[key][2]) == type(None):
                to_plot += "\n"
            else:
                to_plot += "+ {:.3f} - {:.3f} \n".format(self.best_fit[key][2], np.abs(self.best_fit[key][1]))
        plt.figtext(0.5, 0.8, to_plot, fontsize = fontsize)
        
        
        if save is not None:
            plt.savefig(save, bbox_inches = 'tight')
            plt.close()
        
        
    def getBestFit(self, discard = None, mode = "peak", thin = 15, chi2max = None, factor = 3, plot_hists = False, no_self = False, verbose =True, **kwargs):
        """
        

        Parameters
        ----------
        discard : int, optional
            Discard the first N iterations of the chain, typically the burn-in phase. The default is None.
        mode : str, optional
            Either 'best', 'peak', or 'median'. The default is 'peak'.
                - 'best' : Finds the iteration with the lowest chi2 and returns that as the best fit.
                - 'peak' : Fits 1D histograms with a two-sigmas gaussian. Peak of these histograms are returned as the best fit, with sigmas being the errors.
                - 'median' : Finds the median value for each parameter. Errors are computed with the 16 and 84 percentiles.
        thin : int, optional
            Uses only a thin-th of the samples. Used make lighter corner plots. The default is 15.
        chi2max : None/float/str, optional
            - If 'float' : only uses points that have a chi2 under the given value.
            - If 'str' : same as if 'float', but computes the value according to the string.
                            Example : 'min*3', 'min+3', 'max +12 - min +3' are all valid inputs
                                      'min*3' will compute the minimum value of chi2 +3
        factor : int, optional
            Unused. The default is 3.
        plot_hists : bool, optional
            True/False, debugging purpose. The default is False.
        no_self : bool, optional
            True/False. If True, returns the best fit without editing the model. The default is False.
        verbose : bool, optional
            True/False. If False, does not print out anything. The default is True.

        Returns
        -------
        best_fit : Dictionnary
            The best fit.

        """
        
        
        
        """
        Sets the best fit according to the given 'mode'. 'mode' can be either 'chi2', 'median' or 'peak'
            - 'chi2' : return the parameters of the model with the lowest chi2. Errors are None.
            - 'median' : returns the median value for each parameter. Errors are computed with the 16 and 84 percentiles
            - 'peak' : fits a gaussian at the peak value, with two different STD below and above that value. Errors are these two STD values
        """
        
        modes = ['best', 'median', 'peak']
        if mode not in modes:
            raise ValueError("'mode' should be one of {}".format(modes))
            
        discard = self._discardError(discard)
        
        if verbose:
            print("Best fit using '{}' mode".format(mode))        
        
        fit_results = self._fitErrors(discard, mode, thin, chi2max, factor = factor, plot = plot_hists, **kwargs)
        
        best_fit = {}
        for key,val in zip(self.labels, fit_results):
            best_fit[key] = val
        
        if not no_self:
            self.best_fit = best_fit
            for key in self.best_fit:
                theval = self.best_fit[key]
                self.model.params[key].value = theval[0]
                self.model.params[key].error = (theval[1], theval[2])
        
        return best_fit
                
    
    def _fitErrors(self, discard, mode, thin, chi2max, factor = 3, fit_bins = 50, plot = False, maxfev = 10000, verbose = True, **kwargs):
        """
        Return the best fit values and errors
        """
        self._fit_bins = fit_bins
        
        if not self.dirty_load:
            flat_samples = self.sampler.get_chain(discard=discard, thin=thin, flat=True)
        else:
            flat_samples = get_value(self.dirty_chain, True, thin, discard)
        if not self.dirty_load:
            chi2s = -2 * self.sampler.get_log_prob(discard = discard, thin = thin, flat = True)
        else:
            chi2s = -2* get_value(self.dirty_logprob, True, thin, discard)
        
        if mode == 'best':
            idx = np.where(chi2s == np.min(chi2s))[0][0]
            vals = flat_samples[idx]
            fit_results = []
            for val in vals:
                fit_results.append((val, None, None))
            return fit_results
        
        
        if mode == 'median':
            if chi2max is not None:
                if type(chi2max) == type(""):
                    chi2max = chi2max.replace("max", "np.max(chi2s)")
                    chi2max = chi2max.replace("min", "np.min(chi2s)")
                    chi2max = eval(chi2max)
                    if verbose:
                        print("chi2max = {}".format(chi2max))
                
                idx = np.where(chi2s <= chi2max)[0]
                flat_samples = flat_samples[idx]
            
            fit_results = []
            for i in range(self.ndim):
                mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
                param_result = (mcmc[1], -np.abs(mcmc[1]-mcmc[0]), np.abs(mcmc[1]-mcmc[2]))
                fit_results.append(param_result)
            
            return fit_results
        
        
        if mode == 'peak':
            if chi2max is not None:
                if type(chi2max) == type(""):
                    chi2max = chi2max.replace("max", "np.max(chi2s)")
                    chi2max = chi2max.replace("min", "np.min(chi2s)")
                    chi2max = eval(chi2max)
                    if verbose:
                        print("chi2max = {}".format(chi2max))
                
                idx = np.where(chi2s <= chi2max)[0]
                flat_samples = flat_samples[idx]
            
            fit_results = []
            peak_plots = []
            for i in range(len(self.labels)):
                key = self.labels[i]
                if self.model.params[key].unit == u.deg:
                    cyclic = 'deg'
                elif self.model.params[key].unit == u.rad:
                    cyclic = 'rad'
                else:
                    cyclic = None
                    
                val = flat_samples[:,i]
                real_val = val
                # if cyclic, duplicates vals with a 180°/2pi offset
                n_bins_fact = 1
                therange = None
                if cyclic is not None:
                    if cyclic == 'deg':
                        offset = 180
                        valmin = self.model.params[key].min
                    elif cyclic == 'rad':
                        offset = np.pi
                        valmin = self.model.params[key].min
                    val_offset = val - offset
                    val = np.append(val,val_offset)
                    n_bins_fact = 2
                    therange = (valmin-offset, valmin +offset)
                h, bins = np.histogram(val, bins = self._fit_bins*n_bins_fact, density = False, range = therange)
                
                while True:
                    if cyclic is None:
                        imax = np.argmax(h)     # idx of the maximum bin
   
                    # if cyclic (eg PA), searches for the position of the maximum peak that is closest to the center
                    else:
                        imaxs = np.argwhere(h == np.amax(h)).flatten()
                        idx = imaxs[0]
                        middle = np.mean(bins)
                        for i in imaxs:
                            bin_i = (bins[i]+ bins[i+1])/2
                            bin_idx = (bins[idx]+ bins[idx+1])/2
                            if np.abs(bin_i - middle) < np.abs(bin_idx - middle):
                                idx = i
                        imax = idx
                    
                    
                    bin_max = (bins[imax]+ bins[imax+1])/2      # middle position of the maximum bin
                    idx_min = np.where(h < (h[imax])/2)[0]      # idxs where bins have value > half the max bin

                    idx_min_low = idx_min[idx_min < imax]
                    idx_min_up = idx_min[idx_min >= imax]
                    if len(idx_min_low) == 0:
                        idx_min_low = [0]
                    if len(idx_min_up) == 0:
                        idx_min_up = [len(bins) -1]

                    if len(idx_min) == 0:
                        new_range = (np.min(bins), np.max(bins))
                    else:
                        idx_hwhm_offset_low = np.min(np.abs(imax - idx_min_low))    #finding bin idx of the position of hwhm 
                        idx_hwhm_offset_up = np.min(np.abs(imax - idx_min_up))
                        idx_hwhm_up = np.min((len(bins)-1,imax + idx_hwhm_offset_up))
                        idx_hwhm_low = np.max((0,imax - idx_hwhm_offset_low))
                        
                        hwhm_up = np.max((bins[idx_hwhm_up] - bins[imax], bins[1]-bins[0])) # the hwhm can't be smaller than a bin size
                        hwhm_low = np.min((bins[idx_hwhm_low] - bins[imax], -(bins[1]-bins[0]))) 
                        
                        if hwhm_up < 0:
                            raise ValueError("Something went wrong, 'hwhm_up' should be positive")
                        if hwhm_low > 0:
                            raise ValueError("Something went wrong, 'hwhm_low' should be negative")
                        hwhm_low = np.abs(hwhm_low)
                        new_range = (np.max([np.min(bins), bin_max - factor*hwhm_low]), np.min([np.max(bins), bin_max + factor*hwhm_up]))

                    h, bins = np.histogram(val, bins = self._fit_bins*n_bins_fact, range = new_range, density = False)

                    if h[imax] < 0.5*np.sum(h):
                        break

                h, bins = np.histogram(val, bins = self._fit_bins*n_bins_fact, range = new_range, density = False)
                

                mid_bins = [(bins[i]+bins[i+1])/2 for i in range(len(bins) -1)]

                # Fitting
                if cyclic is None:
                    bounds = ((0, self.model.params[key].min, 0, 0, 0, 0),
                              (np.inf, self.model.params[key].max, np.inf, np.inf, (np.max(h) + np.mean(h))/2, (np.max(h) + np.mean(h))/2))
                else:
                    bounds = ((0, -np.inf, 0, 0, 0, 0),
                              (np.inf, np.inf, np.inf, np.inf, (np.max(h) + np.mean(h))/2, (np.max(h) + np.mean(h))/2))
                

                p0 = [np.max(h), np.mean(mid_bins), np.mean(mid_bins)- np.min(mid_bins), np.mean(mid_bins)- np.min(mid_bins), np.min(h), np.min(h)]                

                try:
                    fun = double_gauss
                    popt, pcov = curve_fit(fun, np.array(mid_bins), np.array(h), p0 = p0, bounds = bounds, maxfev = maxfev, **kwargs)
                except Exception as err:
                    print("Failed to fit parameter '{}' with a 2-base-levels gaussian, retrying with a 1-base-level gaussian".format(key))
                    print("Error was : ", err)
                    fun = double_gauss_simple_cte
                    p0 = np.delete(p0, -1)
                    bounds = np.delete(bounds, -1, axis = 1)
                    
                    popt, pcov = curve_fit(fun, np.array(mid_bins), np.array(h), p0 = p0, bounds = bounds, maxfev = maxfev, **kwargs)
                    
                
                
                
                if cyclic is not None:
                    real_bins = []
                    for mid_val in mid_bins:
                        if mid_val < valmin:
                            real_bins.append(mid_val + offset)
                        else:
                            real_bins.append(mid_val)
                    for i in range(len(val)):
                        if val[i] < valmin:
                            val[i] = val[i] + offset
                        
                else:
                    real_bins = mid_bins
                
                peak_plots.append((real_bins, fun(mid_bins, *popt)))
                
                if plot == True:
                    plt.figure()
                    plt.hist(real_val, bins = mid_bins, density = False, zorder = -1)
                    plt.scatter(mid_bins, peak_plots[-1][1], c= 'orange', marker = "+")
                    plt.xlabel(key)
                
                if cyclic is not None:
                    if popt[1] < valmin:
                        popt[1] = popt[1] + offset
                
                param_result = (popt[1], -np.abs(popt[2]), np.abs(popt[3]))
                fit_results.append(param_result)
                    
            self.peak_plots = peak_plots
            return fit_results
        
        
        

    
    def _discardError(self, discard):
        """
        Returns the value of self.discard
        Raises and error if discard is not given and was never given.
        """
        if discard is None and self.discard is None:
            raise ValueError("self.discard hasn't been initialized yet, please give a value using the 'discard' parameter")
        if discard is not None:
            self.discard = discard
        else:
            discard = self.discard
        
        return discard

    
    def saveImages(self, directory = None, **kwargs):
        """
        Saves Chain and Corner Plot in the given directory, using the last parameters used for both 'plotChain' and 'cornerPlot'
        """
        
        if directory is None:
            directory = ""
        elif not directory.endswith("/"):
            directory = directory + "/"
        
        try:
            self.plotChain(save = directory + "chain.png", use_last_params = True)
        except:
            print("Couldn't save the chain")
        
        try:            
            self.cornerPlot(save = directory + "corner_plot.png", use_last_params = True)
        except Exception as err:
            print("Couldn't save the cornerPlot")
            print("Error : ", err)


    def save(self, filename = "fitter.dill", ignoreObject = True):
        """
        Saves the fitter data (state of the chain, and update parameters)
        
        
        Parameters
        ----------
        filename : str, optional
            Name of the file to save the dill object to. Works if 'ignoreObject = False'. The default is "fitter.dill".
        ignoreObject : bool, optional
            True/False. If False, allows to save the fitter object to a dill file. The default is False.
        """
        if not self.dirty_load:
            chain = self.sampler.get_chain()
            logprob = self.sampler.get_log_prob()
        else:
            chain = self.dirty_chain
            logprob = self.dirty_logprob
        last_update_params = self.last_update_params
        
        np.save("emcee_chain.npy", chain)
        np.save("emcee_logprob.npy", logprob)
        json.dump(last_update_params, open("emcee_update_params.json", 'w+'), sort_keys = True, indent = 4)
        
        if not ignoreObject:
            with open(filename, "wb") as dill_file:
                dill.dump(copy.deepcopy(self), dill_file)
        
           

    def load(self, dirty_load = False, fitterobj = None, backend = None, oifits = None, verbose = True):
        if backend is None and fitterobj is not None:
            with open(fitterobj, 'rb') as dill_file:
                data = dill.load(dill_file)
            # return data
            for key in data.__dict__:
                self.__dict__[key] = data.__dict__[key]
        else:
            if oifits is None:
                raise ValueError("Please provide oifits files")
            if oifits != "skip":
                self.data = oiut.open_and_list(oifits)
            
            with open("emcee_update_params.json") as json_file:
                last_update_params = json.load(json_file)
            self.update(**last_update_params)
            
            if verbose:
                print(last_update_params)
            
            if backend is not None:
                self.sampler = emcee.backends.HDFBackend(backend)
            
            if dirty_load:
                self.dirty_chain = np.load("emcee_chain.npy")
                self.dirty_logprob = np.load("emcee_logprob.npy")
                
                self.dirty_load = True



def get_value(dat, flat=False, thin=1, discard=0):
    """
    Copy from emcee.backends.backend.Backend.get_value
    - Arranged to match data shape
    """
    v = dat[discard + thin - 1 : len(dat) : thin]
    if flat:
        s = list(v.shape[1:])
        s[0] = np.prod(v.shape[:2])
        return v.reshape(s)
    return v




def double_gauss(xs, a, x0, sigma1, sigma2, cte1, cte2):
    
    # if cte1 > a:
    #     cte1 = np.abs(a-cte1)
    # if cte2 > a:
    #     cte2 = np.abs(a-cte2)
    
    vals = []
    for x in xs:
        if x <= x0:
            vals.append((a-cte1)*np.exp(-(x-x0)**2/(2*sigma1**2)) + cte1)
        else:
            vals.append((a-cte2)*np.exp(-(x-x0)**2/(2*sigma2**2)) + cte2)
    return np.array(vals)


def double_gauss_simple_cte(xs, a, x0, sigma1, sigma2, cte):
    vals = []
    for x in xs:
        vals.append((a-cte)*np.exp(-(x-x0)**2/(2*sigma1**2)) + cte)
    return np.array(vals)
