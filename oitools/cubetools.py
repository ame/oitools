# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:21:24 2019

@author: ame
"""

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from astropy.io import fits
from scipy import interpolate
from matplotlib.widgets import Slider
import scipy.ndimage as ndimage




"""-----------------------------oifitsSimulator--------------------------------

 Simulate interfrometric observations using a model in an image cube in
 fits format and a list of oifits files.

 The simulated  observations are in oifits format (using astropy fits classes)
 and can be saved to oifits files. The chi square between the data and the
 simulated observations can also be computed.

 sim=cubeTools.oifitsSimulator([oifits1,oifits2],cubemodel)
 sim.compute()
 sim.computeChi2()
 chi2r=sim.chi2r

----------------------------------------------------------------------------"""

_fit_quantities=["VIS2DATA","VISAMP","VISPHI","T3AMP","T3PHI","FLUXDATA"]
_fit_errors=["VIS2ERR","VISAMPERR","VISPHIERR","T3AMPERR","T3PHIERR","FLUXERR"]
_fit_default=[1,0,1,0,1,0]
_fit_array=["OI_VIS2","OI_VIS","OI_VIS","OI_T3","OI_T3","OI_FLUX"]              
_fit_type_name=["Non","Absolute","Differential"]       
_fit_type_short=[0,1,2]

class _fitting:
    def __init__(self,data):
        extnames=[di.name for di in data]
        for i in range(len(_fit_quantities)):
           self.__dict__[_fit_quantities[i]]=0
           if _fit_array[i] in extnames:
               self.__dict__[_fit_quantities[i]]=_fit_default[i]
  
    def __str__(self):
        str=""
        for i in range(len(_fit_quantities)):
            j=self.__dict__[_fit_quantities[i]]
            str+="{0}={1} ".format(_fit_quantities[i],_fit_type_name[j])
        return str

class oifitsSimulator:

    def __init__(self,oifits=None,model=None):
        self.data=[]
        self.simulatedData=[]
        self.fit=[]
        self.setModel(model)
        if oifits!=None:
            self.addData(oifits)
        self.isComputed=False
        self.chi2List=[]
        
       

    def setModel(self,model=None):
        if type(model)==type(""):
            self.model=fits.open(model)
        elif type(model)==type(None):
            self.model=None
        else:
            self.model=model

        if model:
          self.computeModelWavelengthVector()
          self.pixsize=self.model.header['CDELT1']
          try:
              self.angle=np.deg2rad(self.model.header['CROTA2'])
          except:
             self.angle=0
        self.isComputed=False

    def addData(self,oifits=None):
        if type(oifits)==type(""):
            self.data.append(fits.open(oifits))
            self.simulatedData.append(fits.open(oifits))
            self.fit.append(_fitting(self.data[-1]))
        elif type(oifits)==type([]):
            for item in oifits:
                if type(item)==type(""):
                    self.data.append(fits.open(item))
                    self.fit.append(_fitting(self.data[-1]))
                    self.simulatedData.append(fits.open(item))
                else:
                    self.data.append(item)
                    self.fit.append(_fitting(item))
                    c=[]
                    for itemi in item:    
                        c.append(itemi.copy())
                    item_copy=fits.HDUList(c)
                    self.simulatedData.append(item_copy)
                    
        else:
            self.data.append(oifits)
            self.fit.append(_fitting(oifits))
            c=[]
            for itemi in oifits:               
                c.append(itemi.copy())

            item_copy=fits.HDUList(c)
            self.simulatedData.append(item_copy)

        self.isComputed=False

    def computeModelWavelengthVector(self):
       dlam=self.model.header['CDELT3']
       nlam=self.model.header['NAXIS3']
       lam0=self.model.header['CRVAL3']
       try:
           x0=self.model.header['CRPIX3']
       except:
           x0=0
       self.wl= lam0+(np.arange(nlam)-x0)*dlam

    def compute(self,precision=1,normalizeV2=False,normalizeV=True):
        try:
              self.angle=np.deg2rad(self.model.header['CROTA2'])
              self.computeModelWavelengthVector()
              self.pixsize=self.model.header['CDELT1']
        except:
             self.angle=0
        self.compute2DFFT(precision)
        self.computeObservables(normalizeV2,normalizeV)
        self.isComputed=True


    def compute2DFFT(self,precision):
        s=np.shape(self.model.data)
        self.fft2D=np.fft.fftshift(np.fft.fft2(np.fft.fftshift(self.model.data,axes=[2,1]),s=[s[2]*precision,s[1]*precision],axes=[-2,-1]),axes=[2,1])

        self.flux=np.max(np.abs(self.fft2D),axis=(1,2))
        for i in range(len(self.wl)):
            self.fft2D[i,:,:]=self.fft2D[i,:,:]/self.flux[i]

        self.freqVect=np.fft.fftshift(np.fft.fftfreq(s[2]*precision,self.pixsize))

    def computeObservables(self,normalizeV2,normalizeV):
        self.vcompl=[]
        for idata in range(len(self.data)):        
            try:
                um0=self.data[idata]["OI_VIS2"].data['UCOORD']
                vm0=self.data[idata]["OI_VIS2"].data['VCOORD']
            except:
                um0=self.data[idata]["OI_VIS"].data['UCOORD']
                vm0=self.data[idata]["OI_VIS"].data['VCOORD']    
            
            nuv=np.size(um0)
            lam=self.data[idata]['OI_WAVELENGTH'].data['EFF_WAVE']
            nlam=np.size(lam)

            um=um0*np.cos(-self.angle)-vm0*np.sin(-self.angle)
            vm=um0*np.sin(-self.angle)+vm0*np.cos(-self.angle)

            self.uf=(np.outer(um,1./lam))
            self.vf=(np.outer(vm,1./lam))
            self.wlf=(np.outer(np.zeros(np.size(um))+1,lam))

            self.coord=np.transpose([self.wlf.flatten(),self.uf.flatten(),self.vf.flatten()])
            self.grid=(self.wl,self.freqVect,self.freqVect)
            real=interpolate.interpn(self.grid,np.real(self.fft2D),self.coord,method='linear',bounds_error=False,fill_value=None)
            imag=interpolate.interpn(self.grid,np.imag(self.fft2D),self.coord,method='linear',bounds_error=False,fill_value=None)

            if nlam!=1:
                vcompl=np.reshape(real+imag*np.complex(0,1),[nuv,nlam])
            else:
                vcompl=np.reshape(real+imag*np.complex(0,1),[nuv])
            self.vcompl.append(vcompl)
            
            ### This depends on the instruments:

            nextension=len(self.data[idata])

            for iext in range(1,nextension):
                extname=self.data[idata][iext].header['EXTNAME']

                if extname=="OI_VIS2":
                    self.simulatedData[idata][iext].data['VIS2DATA']=np.abs(vcompl)**2
                    if normalizeV2:
                        for iB in range(nuv):
                            self.simulatedData[idata][iext].data['VIS2DATA'][iB,:]/= self.simulatedData[idata][iext].data['VIS2DATA'][iB,0]

                    self.simulatedData[idata][iext].data['VIS2ERR']*=0

                elif extname=="OI_VIS":
                    self.simulatedData[idata][iext].data['VISAMP']=np.abs(vcompl)
                    self.simulatedData[idata][iext].data['VISAMPERR']*=0
                    
                    if normalizeV:
                        for iB in range(nuv):
                            self.simulatedData[idata][iext].data['VISAMP'][iB,:]/= self.simulatedData[idata][iext].data['VISAMP'][iB,0]

                    phi=np.rad2deg(np.angle(vcompl))
                    for i in range(nuv):
                        phi[i,:]=phi[i,:]-np.mean(phi[i,:])
                    self.simulatedData[idata][iext].data['VISPHI']=phi
                    self.simulatedData[idata][iext].data['VISAMPERR']*=0
                
                elif extname=="OI_T3":
                    
                    
                    ncp=np.shape(self.simulatedData[idata]['OI_T3'].data['T3PHI'])[0]
                    for icp in range(ncp):
                        #Tambouille pas très propre à reprendre
                        idx1=np.where(self.simulatedData[idata]['OI_VIS2'].data['UCOORD'] == self.simulatedData[idata][iext].data['U1COORD'][icp])[0]
                        idx2=np.where(self.simulatedData[idata]['OI_VIS2'].data['UCOORD'] == self.simulatedData[idata][iext].data['U2COORD'][icp])[0]
                        sta_idx1=self.simulatedData[idata]['OI_VIS2'].data['STA_INDEX'][idx1,0]
                        sta_idx2=self.simulatedData[idata]['OI_VIS2'].data['STA_INDEX'][idx2,1]
                        idx3= np.where((self.simulatedData[idata]['OI_VIS2'].data['STA_INDEX'][:,0]==sta_idx1) &
                                      (self.simulatedData[idata]['OI_VIS2'].data['STA_INDEX'][:,1]==sta_idx2) &
                                        (self.simulatedData[idata]['OI_VIS2'].data['MJD']==self.simulatedData[idata][iext].data['MJD'][icp]))[0]

                        BS=self.vcompl[idata][idx1,:]*self.vcompl[idata][idx2,:]*np.conjugate(self.vcompl[idata][idx3,:])
                        self.simulatedData[idata][iext].data['T3PHI'][icp,:]=np.rad2deg(np.angle(BS))
                    
                elif extname=="OI_FLUX":
                    nflx=np.shape(self.simulatedData[idata]['OI_FLUX'].data['FLUXDATA'])[0]
                    flx=interpolate.interp1d(self.wl,self.flux,kind='linear',bounds_error=False,fill_value="extrapolate")(self.wlf[0,:])
                    for iflx in range(nflx):
                        self.simulatedData[idata][iext].data["FLUXDATA"][iflx,:]=flx
                        self.simulatedData[idata][iext].data['FLUXERR']*=0

    def computeChi2(self,wlrange=[0,1e99],nfree=None):
        self.sim_mean=[]
        self.data_mean=[]
        if self.isComputed:
            self.chi2List=[]
            for idata in range(len(self.data)):
                self.sim_mean.append({})
                self.data_mean.append({})
                lam=self.data[idata]['OI_WAVELENGTH'].data['EFF_WAVE']
                idx=np.where((lam > wlrange[0]) & (lam < wlrange[1]))[0]
                
                if np.size(idx)>0:
                    if np.size(lam)>1:
                        for iquantity,quantity in enumerate(_fit_quantities):
                            fit=self.fit[idata].__dict__[quantity]
                            arr=_fit_array[iquantity]
                            err=_fit_errors[iquantity]
                            dim=np.size(np.shape(fit))
                            if dim==0:
                                if fit==1:
                                     self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:,idx]-
                                                           self.data[idata][arr].data[quantity][:,idx])**2/
                                                          self.data[idata][arr].data[err][:,idx]**2)
                                if fit==2:
                                     sim_mean=np.transpose(np.tile(np.mean(self.simulatedData[idata][arr].data[quantity][:,idx],axis=1),(np.size(idx),1)))
                                     data_mean=np.transpose(np.tile(np.mean(self.data[idata][arr].data[quantity][:,idx],axis=1),(np.size(idx),1)))
                                     self.sim_mean[idata][quantity]=sim_mean
                                     self.data_mean[idata][quantity]=data_mean
                                     self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:,idx]/sim_mean-
                                                           self.data[idata][arr].data[quantity][:,idx]/data_mean)**2/
                                                          (self.data[idata][arr].data[err][:,idx]/data_mean)**2)
                    else:
                        for iquantity,quantity in enumerate(_fit_quantities):
                           fit=self.fit[idata].__dict__[quantity]
                           arr=_fit_array[iquantity]
                           err=_fit_errors[iquantity]
                           dim=np.size(np.shape(fit))
                           if dim==0:
                               if fit==1:
                                    self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:]-
                                                          self.data[idata][arr].data[quantity][:])**2/
                                                         self.data[idata][arr].data[err][:]**2)
                               if fit==2:
                                    sim_mean=np.transpose(np.tile(np.mean(self.simulatedData[idata][arr].data[quantity][:],axis=1),(np.size(idx),1)))
                                    data_mean=np.transpose(np.tile(np.mean(self.data[idata][arr].data[quantity][:],axis=1),(np.size(idx),1)))
                                    self.sim_mean[idata][quantity]=sim_mean
                                    self.data_mean[idata][quantity]=data_mean
                                    self.chi2List.append((self.simulatedData[idata][arr].data[quantity][:]/sim_mean-
                                                          self.data[idata][arr].data[quantity][:]/data_mean)**2/
                                                         (self.data[idata][arr].data[err][:]/data_mean)**2)
                      
                               
                #self.chi2List.append((self.simulatedData[idata]['OI_VIS2'].data['VIS2DATA'][:,idx]-self.data[idata]['OI_VIS2'].data['VIS2DATA'][:,idx])**2/self.data[idata]['OI_VIS2'].data['VIS2ERR'][:,idx]**2)
                #self.chi2List.append((self.simulatedData[idata]['OI_VIS'].data['VISPHI'][:,idx]-self.data[idata]['OI_VIS'].data['VISPHI'][:,idx])**2/self.data[idata]['OI_VIS'].data['VISPHIERR'][:,idx]**2)

        chi2=0
        nel=0
        for c in self.chi2List:
            nel+=np.size(c[~np.isnan(c)])
            chi2+=np.nansum(c)
        self.chi2r=chi2/nel



###############################################################################

class wavelength:
    def __init__(self,lam0,dlam,nlam,pix0,res):
        c = 3e5
        self.lam0 = lam0
        self.pix0 = pix0
        self.dlam = dlam
        self.nlam = nlam
        self.res  = res
        #self.lam = (np.arange(nlam)-(nlam-1.)/2.)*dlam+lam0
        self.lam = (np.arange(nlam)-pix0)*dlam+lam0
        self.dv = dv=dlam*c/lam0
        self.v=(np.arange(nlam)-nlam/2.)*dv
    def __getitem__(self, index) :
        return self.lam[index]


###############################################################################


class showCube:
    def __init__(self,cube,showSpectrum=False,cmap="hot",showVelocity=False):
        self.ilam=0
        self.pow=1
        self.cube=cube
        self.fig = plt.figure()
        self.ax_im = plt.subplot(121)
        self.ax_sp = plt.subplot(122)
        self.fig.subplots_adjust(left=0.1, right=0.9, bottom=0.3, top=0.9)
        self.pixsize=self.cube.header['CDELT1']*180/np.pi*3600*1000
        self.dim=self.cube.header['NAXIS1']
        self.showVelocity=showVelocity
        extent=[-self.pixsize*self.dim/2,self.pixsize*self.dim/2,-self.pixsize*self.dim/2,self.pixsize*self.dim/2]
        try:
            angle=self.cube.header['CROTA2']
        except:
            angle=0
        self.plt_im = self.ax_im.imshow(ndimage.rotate(self.cube.data[self.ilam,:,:]**self.pow/np.max(self.cube.data[self.ilam,:,:]**self.pow),angle,reshape=False), aspect='equal',extent=extent,cmap=cmap)
        self.ax_im.set_xlabel('x (mas)')
        self.ax_im.set_ylabel('y (mas)')
        self.wl =wavelength( self.cube.header['CRVAL3'],self.cube.header['CDELT3'],self.cube.header['NAXIS3'],self.cube.header['CRPIX3'],1)
        self.sp=np.sum(self.cube.data,axis=(1,2))
        self.plt_sp=self.ax_sp.plot(self.wl.lam*1e9,self.sp)
        self.plt_sp2=self.ax_sp.plot(np.zeros([2])+self.wl.lam[self.ilam]*1e9,[0,self.sp[self.ilam]])
        self.ax_sp.set_xlabel("$\lambda$ ($\mu$m)")
        self.ax_sp.set_ylabel("Flux")

        axcolor = 'lightgoldenrodyellow'
        self.ax = self.fig.add_axes([0.1, 0.11, 0.8, 0.03], facecolor =axcolor)
        self.ax2 = self.fig.add_axes([0.1, 0.01, 0.8, 0.03], facecolor =axcolor)
        if self.showVelocity==True:
            self.ax.set_title('{0:.3f}$\mu$m ({1:.0f}km/s)'.format(self.wl.lam[0]*1e9,self.wl.v[0]))
        else:
            self.ax.set_title('{0:.3f}$\mu$m'.format(self.wl.lam[0]*1e9))
        self.ax2.set_title('Color Scale')
        self.sliderLam = Slider(self.ax, '', 0, self.wl.nlam-1,valinit=self.ilam, valfmt='%i')
        self.sliderLam.on_changed(self.updateLam)
        self.sliderPow = Slider(self.ax2, '', 0,1,valinit=self.pow)
        self.sliderPow.on_changed(self.updatePow)

    def updatePow(self,val):
        self.pow=val
        self.update()

    def updateLam(self,val):
        self.ilam=int(val)
        self.update()

    def update(self):
        try:
            angle=self.cube.header['CROTA2']
        except:
            angle=0
        self.plt_im.set_data(ndimage.rotate(self.cube.data[self.ilam,:,:]**self.pow/np.max(self.cube.data[self.ilam,:,:]**self.pow),angle,reshape=False))
        
        if self.showVelocity==True:
            self.ax.set_title('{0:.3f}$\mu$m ({1:.0f}km/s)'.format(self.wl.lam[self.ilam]*1e9,self.wl.v[self.ilam]))
        else:
            self.ax.set_title('{0:.3f}$\mu$m'.format(self.wl.lam[self.ilam]*1e9))
        self.ax2.set_title('Color Scale')
        self.plt_sp2[0].set_data(np.zeros([2])+self.wl.lam[self.ilam]*1e9,[0,self.sp[self.ilam]])
        self.fig.canvas.draw()


def computeWavelengthFromCube(cube):
       dlam=cube.header['CDELT3']
       nlam=cube.header['NAXIS3']
       lam0=cube.header['CRVAL3']
       try:
           x0=cube.header['CRPIX3']
       except:
           x0=0
       
       wl= lam0+(np.arange(nlam)-x0)*dlam
       return wl


"""
class dft2D:
    def __init__(self,dim,pix,freqs):
        self.pix=pix
        self.freqs=freqs
        self.dim=dim
        self.nfreq=np.shape(freqs)[0]
        self.kernel=np.ndarray([dim,dim,self.nfreq],dtype='complex')
        self.x=(np.arange(dim*dim).reshape([dim,dim]) % dim)*self.pix
        self.y=np.transpose(self.x)
        self.ft=np.ndarray([self.nfreq],dtype='complex')

        for i in range(self.nfreq):
            self.kernel[:,:,i]=np.exp(-np.complex(0,1)*(self.x*self.freqs[i][0]+self.y*self.freqs[i][1]))



    def compute(self,image):
        for i in range(self.nfreq):
            self.ft[i]=np.sum(self.kernel[:,:,i]*image)

"""