# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 10:10:27 2019

@author: ame
"""
"""
testttt
"""


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.cm as cm
import matplotlib.colors as mcolors
from datetime import datetime
from astropy.io import fits
from scipy import interpolate
from astroquery.simbad import Simbad
from  astropy.coordinates import Angle
import astropy.units as units
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm

cut_wl=np.array(['EFF_WAVE','EFF_BAND'])
cut_vis2=np.array(['VIS2DATA','VIS2ERR','FLAG'])
cut_vis=np.array(['VISAMP','VISAMPERR','VISPHI','VISPHIERR','FLAG'])
cut_t3=np.array(['T3AMP','T3AMPERR','T3PHI','T3PHIERR','FLAG'])
cut_flux=np.array(['FLUXDATA','FLUXERR','FLAG'])
cut_tf2=np.array(['TF2','TF2ERR','TF','TFERR','FLAG'])


rad2deg=180/np.pi
rad2arcsec=rad2deg*3600
rad2mas=rad2arcsec*1000

def copy(hdulist):
    return fits.HDUList([hdu.copy() for hdu in hdulist])


def flagWavelengthRange(oifits,wlRange,verbose=False,value=True):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))]) 
    wl_idx=np.where(extnames=="OI_WAVELENGTH")[0]
    vis_idx=np.where(extnames=="OI_VIS")[0]
    vis2_idx=np.where(extnames=="OI_VIS2")[0]
    t3_idx=np.where(extnames=="OI_T3")[0]
    flux_idx=np.where(extnames=="OI_FLUX")[0]    

    for i in wl_idx:
        if verbose :
            print("OI_WAVELENGTH table at index {0}".format(i))
        insname=data[i].header['INSNAME']
        if verbose :
            print("INSNAME = {0}".format(insname))

    
        idx_wl_flag=np.where((data[i].data['EFF_WAVE']>=wlRange[0]) &
                            (data[i].data['EFF_WAVE']<=wlRange[1]))[0]
        #nwl_flag=len(idx_wl_flag)
        
        for j in range(len(extnames)):
            if extnames[j] in ["OI_VIS2","OI_VIS","OI_T3","OI_FLUX"]:
                if data[j].header['INSNAME']==insname:
                    dimFlag=np.shape(data[j].data['FLAG'])
                    if verbose :
                        print("Flagging {0} table {1} (size={2})".format(extnames[j],j,dimFlag)) 
                    
                    if len(dimFlag)==1:
                        data[j].data['FLAG'][idx_wl_flag]=value
                    elif len(dimFlag)==2:
                        data[j].data['FLAG'][:,idx_wl_flag]=value
                    
    

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]

def cutWavelengthRange(oifits,wlRange = None, idx_wl_cut = None, verbose=False):
    
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])

    wl_idx=np.where(extnames=="OI_WAVELENGTH")[0]
    vis_idx=np.where(extnames=="OI_VIS")[0]
    vis2_idx=np.where(extnames=="OI_VIS2")[0]
    t3_idx=np.where(extnames=="OI_T3")[0]
    flux_idx=np.where(extnames=="OI_FLUX")[0]
    tf2_idx=np.where(extnames=="TF2")[0]  #MATISSE ONLY
    if verbose:
        print("{0} OI_VIS table found at {1}".format(len(vis_idx),vis_idx))
        print("{0} OI_VIS2 table found at {1}".format(len(vis2_idx),vis2_idx))
        print("{0} OI_T3 table found at {1}".format(len(t3_idx),t3_idx))
        print("{0} OI_FLUX table found at {1}".format(len(flux_idx),flux_idx))
        print("{0} TF2 table found at {1}".format(len(tf2_idx),tf2_idx))

    for i in wl_idx:
        if verbose :
            print("OI_WAVELENGTH table at index {0}".format(i))
        insname=data[i].header['INSNAME']
        if verbose :
            print("INSNAME = {0}".format(insname))
            
        if idx_wl_cut is None:
            if wlRange is None:
                raise TypeError("'wlRange' should not be none if 'idx_wl_cut' is not given")
            if not hasattr(wlRange, "__iter__"):
                idx_wl_cut, val = find_nearest(data[i].data['EFF_WAVE'], wlRange)
                print("Cutting at nearest value {}, idx = {}".format(val, idx_wl_cut))
                idx_wl_cut = [idx_wl_cut]
                
            elif not hasattr(wlRange[0], "__iter__"):
                idx_wl_cut=np.where((data[i].data['EFF_WAVE']>=wlRange[0]) &
                                    (data[i].data['EFF_WAVE']<=wlRange[1]))[0]
            
            else:
                # Building condition
                cond = ""
                for k in range(len(wlRange)):
                    cond += "((data[i].data['EFF_WAVE']>=wlRange[{}][0]) & (data[i].data['EFF_WAVE']<=wlRange[{}][1]))".format(k,k)
                    if k != len(wlRange) -1:
                        cond += " | "
                # print(cond)
                idx_wl_cut=eval("np.where(" + cond + ")[0]")
        nwl_cut=len(idx_wl_cut)
        # print(idx_wl_cut)

        data[i].data=np.take(data[i].data,idx_wl_cut)
        data[i].update()

        for j in vis_idx:
            if data[j].header['INSNAME']==insname:
                if verbose :
                    print("modifying OI_VIS table {0}".format(j))
                colDefs=[]
                for col in data[j].columns:
                    if np.isin(col.name,cut_vis):
                        format0=col.format[-1]
                        arr=np.take(data[j].data[col.name],idx_wl_cut, axis=1)
                        # print("_____")
                        # print(np.shape(arr))
                        if verbose :
                            print("cutting {0} from {1} to {2}".format(col.name,np.shape(data[j].data[col.name]),np.shape(arr)))
                        colDefs.append(fits.Column(name=col.name,format="{0}{1}".format(nwl_cut,format0),array=arr,unit=col.unit))
                    else:
                        colDefs.append(fits.Column(name=col.name,format=col.format,array=data[j].data[col.name],unit=col.unit))
                        # print(np.shape(data[j].data[col.name]))
                        # print(2)
                cols = fits.ColDefs(colDefs)
                # print(np.shape(cols))
                # print(cols)
                hdu=fits.BinTableHDU.from_columns(cols)
                hdu.header=data[j].header
                hdu.update()
                data[j]=hdu
        for j in vis2_idx:
            if data[j].header['INSNAME']==insname:
                if verbose :
                    print("modifying OI_VIS2 table {0}".format(j))
                colDefs=[]
                for col in data[j].columns:
                    if np.isin(col.name,cut_vis2):
                        format0=col.format[-1]
                        arr=np.take(data[j].data[col.name],idx_wl_cut, axis=1)
                        if verbose :
                            print("cutting {0} from {1} to {2}".format(col.name,np.shape(data[j].data[col.name]),np.shape(arr)))
                        colDefs.append(fits.Column(name=col.name,format="{0}{1}".format(nwl_cut,format0),array=arr,unit=col.unit))
                    else:
                        colDefs.append(fits.Column(name=col.name,format=col.format,array=data[j].data[col.name],unit=col.unit))
                cols = fits.ColDefs(colDefs)
                hdu=fits.BinTableHDU.from_columns(cols)
                hdu.header=data[j].header
                hdu.update()
                data[j]=hdu
        for j in t3_idx:
            if data[j].header['INSNAME']==insname:
                if verbose :
                    print("modifying OI_T3 table {0}".format(j))
                colDefs=[]
                for col in data[j].columns:
                    if np.isin(col.name,cut_t3):
                        format0=col.format[-1]
                        arr=np.take(data[j].data[col.name],idx_wl_cut, axis=1)
                        if verbose :
                            print("cutting {0} from {1} to {2}".format(col.name,np.shape(data[j].data[col.name]),np.shape(arr)))
                        colDefs.append(fits.Column(name=col.name,format="{0}{1}".format(nwl_cut,format0),array=arr,unit=col.unit))
                    else:
                        colDefs.append(fits.Column(name=col.name,format=col.format,array=data[j].data[col.name],unit=col.unit))
                cols = fits.ColDefs(colDefs)
                hdu=fits.BinTableHDU.from_columns(cols)
                hdu.header=data[j].header
                hdu.update()
                data[j]=hdu
        for j in flux_idx:
            if data[j].header['INSNAME']==insname:
                if verbose :
                    print("modifying OI_FLUX table {0}".format(j))
                colDefs=[]
                for col in data[j].columns:
                    if np.isin(col.name,cut_flux):
                        format0=col.format[-1]
                        if data[j].data[col.name].ndim==2:
                            arr=np.take(data[j].data[col.name],idx_wl_cut, axis=1)  
                        else: 
                            arr=np.take(data[j].data[col.name],idx_wl_cut).reshape(1,nwl_cut)
                        if verbose :
                            print("cutting {0} from {1} to {2}".format(col.name,np.shape(data[j].data[col.name]),np.shape(arr)))
                        colDefs.append(fits.Column(name=col.name,format="{0}{1}".format(nwl_cut,format0),array=arr,unit=col.unit))
                    else:
                        colDefs.append(fits.Column(name=col.name,format=col.format,array=data[j].data[col.name],unit=col.unit))
                cols = fits.ColDefs(colDefs)
                hdu=fits.BinTableHDU.from_columns(cols)
                hdu.header=data[j].header
                hdu.update()
                data[j]=hdu
                
        for j in tf2_idx:
          if data[j].header['INSNAME']==insname:
              if verbose :
                  print("modifying TF2 table {0}".format(j))
              colDefs=[]
              for col in data[j].columns:
                  if np.isin(col.name,cut_tf2):
                      format0=col.format[-1]
                      arr=np.take(data[j].data[col.name],idx_wl_cut, axis=1)
                      if verbose :
                          print("cutting {0} from {1} to {2}".format(col.name,np.shape(data[j].data[col.name]),np.shape(arr)))
                      colDefs.append(fits.Column(name=col.name,format="{0}{1}".format(nwl_cut,format0),array=arr,unit=col.unit))
                  else:
                      colDefs.append(fits.Column(name=col.name,format=col.format,array=data[j].data[col.name],unit=col.unit))
              cols = fits.ColDefs(colDefs)
              hdu=fits.BinTableHDU.from_columns(cols)
              hdu.header=data[j].header
              hdu.update()
              data[j]=hdu        
                
                
    return data

def normalizeV2(oifits,nval=1,nval2=0):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])
    vis2_idx=np.where(extnames=="OI_VIS2")[0]
    for i in vis2_idx:
        nB=np.shape(data[i].data['UCOORD'])[0]
        for iB in range(nB):
            meanV2= np.mean(data[i].data['VIS2DATA'][iB,0:nval])
            if nval2==0:
                data[i].data['VIS2ERR'][iB,:]= data[i].data['VIS2ERR'][iB,:]/meanV2
                data[i].data['VIS2DATA'][iB,:]/= meanV2
            else:

                nwl=np.size(data[i].data['VIS2DATA'][iB,:])
                
                x=np.arange(nwl)
                idx=np.where((x<nval) | ((nwl-x-1)<nval2))[0]
                p=np.poly1d(np.polyfit(x[idx],data[i].data['VIS2DATA'][iB,idx],1))
                V2fit=p(x)
                data[i].data['VIS2ERR'][iB,:]= data[i].data['VIS2ERR'][iB,:]/V2fit
                data[i].data['VIS2DATA'][iB,:]/= V2fit
                



def normalizeFlux(oifits,nval=1,nval2=0):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])
    flx_idx=np.where(extnames=="OI_FLUX")[0]
    for i in flx_idx:
        nB=np.shape(data[i].data['FLUXDATA'])[0]
        for iB in range(nB):
            meanFlx= np.mean(data[i].data['FLUXDATA'][iB,0:nval])
            if nval2==0:
                data[i].data['FLUXERR'][iB,:]= data[i].data['FLUXERR'][iB,:]/meanFlx
                data[i].data['FLUXDATA'][iB,:]/= meanFlx
            else:

                nwl=np.size(data[i].data['FLUXDATA'][iB,:])
                
                x=np.arange(nwl)
                idx=np.where((x<nval) | ((nwl-x-1)<nval2))[0]
                p=np.poly1d(np.polyfit(x[idx],data[i].data['FLUXDATA'][iB,idx],1))
                Flxfit=p(x)
                data[i].data['FLUXERR'][iB,:]= data[i].data['FLUXERR'][iB,:]/Flxfit
                data[i].data['FLUXDATA'][iB,:]/= Flxfit
                


def computeDifferentialError(oifits,nval=10,nval2=0,arr=["OI_VIS2","OI_VIS","OI_T3"]):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])
    
    if "OI_VIS2" in arr:
        vis2_idx=np.where(extnames=="OI_VIS2")[0]
       
        for i in vis2_idx:
            nB=np.shape(data[i].data['UCOORD'])[0]
            nwl=np.size(data[i].data['VIS2DATA'][0,:])
            x=np.arange(nwl)
            idx=np.where((x<nval) | ((nwl-x-1)<nval2))[0]
            for iB in range(nB):
                err= np.std(data[i].data['VIS2DATA'][iB,idx])
                data[i].data['VIS2ERR'][iB,:]=err
                
    if "OI_VIS" in arr:
        vis_idx=np.where(extnames=="OI_VIS")[0]
        for i in vis_idx:
            nB=np.shape(data[i].data['UCOORD'])[0]
            nwl=np.size(data[i].data['VISPHI'][0,:])
            x=np.arange(nwl)
            idx=np.where((x<nval) | ((nwl-x-1)<nval2))[0]
            for iB in range(nB):
                errPhi= np.std(data[i].data['VISPHI'][iB,idx])
                data[i].data['VISPHIERR'][iB,:]=errPhi
                errVis= np.std(data[i].data['VISAMP'][iB,idx])
                data[i].data['VISAMPERR'][iB,:]=errVis                
    if "OI_T3" in arr:
        t3_idx=np.where(extnames=="OI_T3")[0]
        for i in t3_idx:
            nB=np.shape(data[i].data['U1COORD'])[0]
            nwl=np.size(data[i].data['T3PHI'][0,:])
            x=np.arange(nwl)
            idx=np.where((x<nval) | ((nwl-x-1)<nval2))[0]
            for iB in range(nB):
                err= np.std(data[i].data['T3PHI'][iB,idx])
                data[i].data['T3PHIERR'][iB,:]=err







def shiftWavelength(oifits,shift,verbose=True):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])

    wl_idx=np.where(extnames=="OI_WAVELENGTH")[0]
    for i in wl_idx:
        if verbose :
            print("OI_WAVELENGTH table at index {0}".format(i))
        insname=data[i].header['INSNAME']
        if verbose :
            print("INSNAME = {0}".format(insname))
        data[i].data['EFF_WAVE']+=shift


def uvplot(oifits,extension="OI_VIS2",marker="o", facecolors='red', edgecolors='k',size=10,axe=None,maxi=None,xytitle=[True,True],title=None,gridcolor="k",grid=True,fontsize=None):

    data=[]
    u=np.array([])
    v=np.array([])
    if type(oifits)==type(""):
        data.append(fits.open(oifits))
    elif type(oifits)==type([]):
        for item in oifits:
            if type(item)==type(""):
                data.append(fits.open(item))
            else:
                data.append(item)
    else:
        data.append(oifits)


    for datai in data:

        extnames=np.array([datai[i].name for i in range(len(datai))])
        idx=np.where(extnames==extension)[0]
        for j in idx:
            u=np.append(u,datai[j].data['UCOORD'])
            v=np.append(v,datai[j].data['VCOORD'])


    if not(axe):
        fig,axe=plt.subplots(nrows=1,ncols=1)
    axe.scatter(u,v,marker=marker,facecolors=facecolors, edgecolors=edgecolors,s=size,zorder=10,lw=1)
    axe.scatter(-u,-v,marker=marker,facecolors=facecolors, edgecolors=edgecolors,s=size,zorder=10,lw=1)
    if not(maxi):
        maxi=1.1*np.max(np.abs(np.array([u,v])))
    if grid:
        axe.plot([-maxi,maxi],[0,0],linewidth=1,color=gridcolor,zorder=5)
        axe.plot([0,0],[-maxi,maxi],linewidth=1,color=gridcolor,zorder=5)
    axe.set_aspect('equal','box')
    axe.set_xlim([maxi,-maxi])
    axe.set_ylim([-maxi,maxi])
    if xytitle[0]:
        axe.set_xlabel('u (m)',fontsize=fontsize)
    if xytitle[1]:
        axe.set_ylabel('v (m)',fontsize=fontsize)
    if title:
        axe.set_title(title)
    return [axe]


def create_oi_array(arrname,arrx,arry,arrz,sta_name,tel_name,diameter,staxyz):
    nstation=np.size(sta_name)
    tel_name=fits.Column(name="TEL_NAME",format="A16",array=np.array(tel_name))
    sta_name=fits.Column(name="STA_NAME",format="A16",array=np.array(sta_name))
    sta_index=fits.Column(name="STA_INDEX",format="I2",array=np.arange(1,nstation+1))
    diameter=fits.Column(name="DIAMETER",format="E",array=np.array(diameter),unit='m')
    staxyz=fits.Column(name="STAXYZ",format="3D",array=np.array(staxyz),unit='m')

    cols=[tel_name,sta_name,sta_index,diameter,staxyz]
    arr=fits.BinTableHDU.from_columns(cols)

    arr.header['EXTVER']=(1,'ID number of this OI_ARRAY')
    arr.header['ARRAYX']=float(arrx)#,'[m] Array center X coordinate')
    arr.header['ARRAYY']=float(arry)#,'[m] Array center Y coordinate')
    arr.header['ARRAYZ']=float(arrz)#,'[m] Array center Z coordinate')
    arr.header['FRAME']=('GEOCENTRIC','Coordinate frame')
    arr.header['EXTNAME']='OI_ARRAY'
    arr.header['OI_REVN']=(1,'Revision number of the table definition')
    arr.header['ARRNAME']=(arrname,'Array name')

    return arr

def create_oi_target_from_simbad(names):
    customSimbad = Simbad()
    customSimbad.add_votable_fields('plx','plx_error','propermotions','sptype','velocity')
    if type(names)==type(""):
        names=[names]
    data=customSimbad.query_objects(names)

    ntargets=len(names)
    rad=Angle(data['RA'],unit="hourangle").deg
    dec=Angle(data['DEC'],unit="deg").deg
    ra_err=(data['COO_ERR_MAJA'].data*units.mas).to_value(unit='deg')
    dec_err=(data['COO_ERR_MINA'].data*units.mas).to_value(unit='deg')
    pmra=(data['PMRA'].data*units.mas).to_value(unit='deg')
    pmdec=(data['PMDEC'].data*units.mas).to_value(unit='deg')
    pmra_err=(data['PM_ERR_MAJA'].data*units.mas).to_value(unit='deg')
    pmdec_err=(data['PM_ERR_MINA'].data*units.mas).to_value(unit='deg')
    plx_value=(data['PLX_VALUE'].data*units.mas).to_value(unit='deg')
    plx_error=(data['PLX_ERROR'].data*units.mas).to_value(unit='deg')

    target_id=fits.Column(name="TARGET_ID",format="I",array=np.arange(1,ntargets+1))
    target=fits.Column(name="TARGET",format="16A",array=names)
    raep0=fits.Column(name="RAEP0",format="D",array=rad,unit="deg")
    decep0=fits.Column(name="DECEP0",format="D",array=dec,unit="deg")
    equinox=fits.Column(name="EQUINOX",format="E",array=np.repeat(2000,ntargets),unit="yr")
    ra_err=fits.Column(name="RA_ERR",format="D",array=ra_err,unit="deg")
    dec_err=fits.Column(name="DEC_ERR",format="D",array=dec_err,unit="deg")
    sysvel=fits.Column(name="SYSVEL",format="D",array=np.zeros([ntargets]),unit="m/s")   #TODO
    veltyp=fits.Column(name="VELTYP",format="8A",array=np.repeat("UNKNOWN",ntargets))  #TODO
    veldef=fits.Column(name="VELDEF",format="8A",array=np.repeat("OPTICAL",ntargets)) #TODO
    pmra=fits.Column(name="PMRA",format="D",array=pmra,unit="deg/yr")
    pmdec=fits.Column(name="PMDEC",format="D",array=pmdec,unit="deg/yr")
    pmra_err=fits.Column(name="PMRA_ERR",format="D",array=pmra_err,unit="deg/yr")
    pmdec_err=fits.Column(name="PMDEC_ERR",format="D",array=pmdec_err,unit="deg/yr")
    parallax=fits.Column(name="PARALLAX",format="E",array=plx_value,unit="deg")
    para_err=fits.Column(name="PARA_ERR",format="E",array=plx_error,unit="deg")
    spectyp=fits.Column(name="SPECTYP",format="16A",array=data['SP_TYPE'])


    cols=[target_id,target,raep0,decep0,equinox,ra_err,dec_err,sysvel,veltyp,
          veldef,pmra,pmdec,pmra_err,pmdec_err,parallax,para_err,spectyp]

    tar=fits.BinTableHDU.from_columns(cols)

    tar.header['OI_REVN']=(1,'Revision number of the table definition')
    tar.header['EXTVER']=(1,'ID number of this OI_TARGET')
    tar.header['EXTNAME']='OI_TARGET'
    return tar

def create_oi_wavelength(insname,eff_wave,eff_band):
    eff_wave=fits.Column(name="EFF_WAVE",format="E",array=np.array(eff_wave),unit='m')
    eff_band=fits.Column(name="EFF_BAND",format="E",array=np.array(eff_band),unit='m')

    cols=[eff_wave,eff_band]
    wave=fits.BinTableHDU.from_columns(cols)
    wave.header['EXTNAME']='OI_WAVELENGTH'
    wave.header['EXTVER']=(1,'ID number of this OI_WAVELENGTH')
    wave.header['OI_REVN']=(1,'Revision number of the table definition')
    wave.header['INSNAME']=(insname,'Identifies corresponding OI_WAVELENGTH')

    return wave

def create_oi_vis2(arrname,insname,target_id,time,mjd,int_time,vis2data,vis2err,ucoord,vcoord,sta_index,flag,dateobs):
    
    nb=len(target_id)
    
    v2shape=np.shape(vis2data)
    v2dim=len(v2shape)
    
    if (v2dim==2):
        nlam=v2dim[1]
    elif (v2dim==1):
        if v2shape[0]==nb:
            nlam=1
        else:
            nlam=v2shape[0]

    
    target_id=fits.Column(name="TARGET_ID",format="I",array=np.array(target_id))
    time=fits.Column(name="TIME",format="D",array=np.array(time),unit="sec")
    mjd=fits.Column(name="MJD",format="D",array=np.array(mjd),unit="day")
    int_time=fits.Column(name="INT_TIME",format="D",array=np.array(int_time),unit="sec")
    vis2data=fits.Column(name="VIS2DATA",format="{0}D".format(nlam),array=np.array(vis2data))
    vis2err=fits.Column(name="VIS2ERR",format="{0}D".format(nlam),array=np.array(vis2err))
    ucoord=fits.Column(name="UCOORD",format="1D",array=np.array(ucoord),unit="m")
    vcoord=fits.Column(name="VCOORD",format="1D",array=np.array(vcoord),unit="m")
    sta_index=fits.Column(name="STA_INDEX",format="2I",array=np.array(sta_index))
    flag=fits.Column(name="FLAG",format="L",array=np.array(flag))
    
    cols=[target_id,time,mjd,int_time,vis2data,vis2err,ucoord,vcoord,sta_index,flag]
      
    oivis2=fits.BinTableHDU.from_columns(cols)
    oivis2.header['EXTNAME']='OI_VIS2'
    oivis2.header['EXTVER']=(1,'ID number of this OI_VIS2')
    oivis2.header['OI_REVN']=(1,'Revision number of the table definition')
    oivis2.header['INSNAME']=(insname,'Identifies corresponding OI_WAVELENGTH')
    oivis2.header['DATE-OBS']=dateobs
    oivis2.header['ARRNAME']=arrname

    return oivis2
    

def smooth(oifits,size,verbose=True):

    kernel =  np.ones(size)/size

    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    extnames=np.array([data[i].name for i in range(len(data))])

    extidx=np.where((extnames=="OI_VIS") | (extnames=="OI_VIS2") |
                (extnames=="OI_T3")  | (extnames=="OI_FLUX") | (extnames=="TF2"))[0]

    for iext in extidx:
        if extnames[iext]=="OI_VIS2":
            s=np.shape(data[iext].data['VIS2DATA'])
            if len(s)==2:
                nB=s[0]
                for iB in range(nB):
                    data[iext].data['VIS2DATA'][iB,:]=np.convolve(data[iext].data['VIS2DATA'][iB,:],kernel,'same')
            else:
                    data[iext].data['VIS2DATA']=np.convolve(data[iext].data['VIS2DATA'],kernel,'same')
        if extnames[iext]=="TF2":
            s=np.shape(data[iext].data['TF2'])
            if len(s)==2:
                nB=s[0]
                for iB in range(nB):
                    data[iext].data['TF2'][iB,:]=np.convolve(data[iext].data['TF2'][iB,:],kernel,'same')
            else:
                    data[iext].data['TF2']=np.convolve(data[iext].data['TF2'],kernel,'same')

cut_vis2=np.array(['VIS2DATA','VIS2ERR','FLAG'])
cut_vis=np.array(['VISAMP','VISAMPERR','VISPHI','VISPHIERR','FLAG'])
cut_t3=np.array(['T3AMP','T3AMPERR','T3PHI','T3PHIERR','FLAG'])
cut_flux=np.array(['FLUXDATA','FLUXERR','FLAG'])



def _rebin(arr, binsize,median=True):
    newsize = (arr.shape[0] / int(binsize)) * binsize
    arr = arr [:newsize]
    shape = (arr.shape[0]/ binsize, binsize)
    if median:
        res=np.median(arr.reshape(shape),axis=-1)
    else:
        res=arr.reshape(shape).mean(-1)
    return res


def _rebinHdu(hdu,binsize,exception=[]):
    cols = hdu.data.columns
    newcols=[]

    dim2 = 2 in [len(np.shape(hdu.data[coli.name])) for coli in cols]

    if dim2==True:
        for coli in cols:
            #print(coli)
            newformat=coli.format
            shape = np.shape(hdu.data[coli.name])
            if len(shape) == 2 and not(coli.name in exception):
                bini =[]
                for jB in range(shape[0]):
                    binij = _rebin(hdu.data[coli.name][jB,:],binsize)
                    bini.append(binij)
                bini = np.array(bini)
                #print(np.shape(bini))
                newformat = "{0}{1}".format(shape[1]/binsize,coli.format[-1])
            else:
                bini = hdu.data[coli.name]
                #print(np.shape(bini))

            newcoli=fits.Column(name=coli.name,array=bini,unit=coli.unit,format=newformat)
            #print(newcoli)
            #print("#########")
            newcols.append(newcoli)
    else:
        for coli in cols:
            #print(coli)
            bini = _rebin(hdu.data[coli.name],binsize)
            newcoli=fits.Column(name=coli.name,array=bini,unit=coli.unit,format=coli.format)
            newcols.append(newcoli)
            #print(np.shape(bini))
            #print(coli)
            #print("#########")
    #print(newcols)
    newhdu=fits.BinTableHDU.from_columns(fits.ColDefs(newcols))
    newhdu.header=hdu.header
    newhdu.update()
    return newhdu
    #6print("##############################")

def binWavelength(oifits,binsize):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits

    tobin=["OI_WAVELENGTH","OI_VIS","OI_VIS2","OI_T3","OI_FLUX"]

    for i in range(1,len(data)):
        #print(data[i].name)
        if data[i].name in tobin:
            data[i]=_rebinHdu(data[i],binsize,exception=["STA_INDEX"])

    data["OI_VIS2"].data["VIS2ERR"]/=np.sqrt(binsize)
    data["OI_VIS"].data["VISPHIERR"]/=np.sqrt(binsize)
    data["OI_VIS"].data["VISAMPERR"]/=np.sqrt(binsize)
    data["OI_T3"].data["T3PHIERR"]/=np.sqrt(binsize)



#--------------------------------------------baselinename-------------------------

def baselinename(oifits,hduname="OI_VIS2",length=False,angle=False):
    stanames=oifits['OI_ARRAY'].data['STA_NAME']
    staindexes=oifits['OI_ARRAY'].data['STA_INDEX']

    staidx=oifits[hduname].data['STA_INDEX']

    shape=np.shape(staidx)
    name=[]
    if length or angle and hduname!="OI_T3":
        u=oifits[hduname].data['UCOORD']
        v=oifits[hduname].data['VCOORD']
        B=np.sqrt(u**2+v**2)
        PA=np.rad2deg(np.arctan2(u,v))
    for i in range(shape[0]):
        namei=""
        for j in range(shape[1]):
            namei+=stanames[np.where(staindexes==staidx[i,j])[0]][0]
            if j<shape[1]-1:
                namei+="-"
        if hduname!="OI_T3":
            if length:
                namei+=" {0:.0f}m".format(B[i])
            if angle:
                namei+=" {0:.0f}$^o$".format(PA[i])
        name.append(namei)

    return name
#--------------------------------------------spectralSmoothing-------------------------

def spectralSmoothing(oifits,binsize):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits

    kernel=np.ones(binsize)/binsize
    tableToSmooth=["OI_VIS","OI_VIS2","OI_T3","OI_FLUX"]
    colToSmooth=["VIS2DATA","VIS2ERR","VISAMP","VISAMPERR","VISPHI","VISPHIERR",
                 "T3AMP","T3AMPERR","T3PHI","T3PHIERR","FLUXDATA","FLUxDATAERR"]

    for i in range(1,len(data)):
        try:
            if data[i].name in tableToSmooth:
    
                cols = data[i].data.columns
    
                for coli in cols:
                    if coli.name in colToSmooth:
    
                        dims=np.shape(data[i].data[coli.name])
    
                        if len(dims)==2:
                            nB=dims[0]
                            for iB in range(nB):
                                data[i].data[coli.name][iB,:]=np.convolve(data[i].data[coli.name][iB,:],kernel,"same")
                        else:
                              data[i].data[coli.name]=np.convolve(data[i].data[coli.name],kernel,"same")
        except:
            pass

    try:
        data["OI_VIS2"].data["VIS2ERR"]/=np.sqrt(binsize)
    except:
        pass
    try:
        data["OI_VIS"].data["VISPHIERR"]/=np.sqrt(binsize)
    except:
        pass
    try:
        data["OI_VIS"].data["VISAMPERR"]/=np.sqrt(binsize)
    except:
        pass
    try:
        data["OI_T3"].data["T3PHIERR"]/=np.sqrt(binsize)
    except:
        pass
    try:
        data["OI_FLUX"].data["FLUXERR"]/=np.sqrt(binsize)
    except:
        pass

#--------------------------------------------oiPLot------------------------------------

plotname_param=np.array(["B","PA","UCOORD","VCOORD","SPAFREQ","EFF_WAVE","VIS2DATA","VISAMP","VISPHI","T3AMP","T3PHI","FLUXDATA"])
plotname_error=np.array(["","","","","","EFF_BAND","VIS2ERR","VISAMPERR","VISPHIERR","T3AMPERR","T3PHIERR","FLUXERR"])
plotname_arr=np.array(["","","","","","OI_WAVELENGTH","OI_VIS2","OI_VIS","OI_VIS","OI_T3","OI_T3","OI_FLUX"])
plotname_label=np.array(["Baseline Length","Baseline Orientation","U","V","Spatial Frequency","Wavelength","Square Visibility",
                "Differential Visibility","Differential Phase","CLosure Amplitude","Closure Phase","Flux"])
plotname_label_short=np.array(["B","PA","U","V","B/$\lambda$","$\lambda$","V$^2$","V$_{diff}$","$\phi_{diff}$","Clos. Amp.","CP","Flux"])
plotname_unit0=np.array(["m","$^o$","m","m","cycles/rad","$\mu$m","","","$^o$","","$^o$",""])
plotname_uvcoord=np.array([1,1,1,1,1,0,0,0,0,0,0,0])


defaultcolorTab=["b","g","r","c","m","y","orange","gray","indigo","maroon","olive"]


def _getNextColor(i,colorTab):
    if type(colorTab)==type(""):
        return colorTab
    else:
        n=len(colorTab)
        ii= i % n
        return colorTab[ii]

def oiPlot(oifitsList,xname,yname,xunitname=None,xunitmultiplier=1,yunitname=None,
           yunitmultiplier=1,xlim=None,ylim=None,xscale=None,yscale=None,shortLabel=True,axe=None,colors=None,errorbar=False,linestyle="solid",removeFlagged=False):

    if type(oifitsList)!=type([]):
        oifitsList=[oifitsList]
    
    icol=0
    if colors==None:
        colorTab=defaultcolorTab
    else :
        colorTab=colors
    ndata=len(oifitsList)
    idxX=np.where(plotname_param == xname)[0][0]
    idxY=np.where(plotname_param == yname)[0][0]

    xerrname=plotname_error[idxX]
    xarr=plotname_arr[idxX]
    if not(shortLabel):
        xlabel=plotname_label[idxX]
    else:
        xlabel=plotname_label_short[idxX]
        
    if xunitname:
        xunit0=xunitname
    else:
        xunit0=plotname_unit0[idxX]
    xIsUVcoord=plotname_uvcoord[idxX]


    if xunit0!="":
        xlabel+="("+xunit0+")"

    yerrname=plotname_error[idxY]
    yarr=plotname_arr[idxY]
    if not(shortLabel):
        ylabel=plotname_label[idxY]
    else:
        ylabel=plotname_label_short[idxY]
    yunit0=plotname_unit0[idxY]
    yIsUVcoord=plotname_uvcoord[idxY]

    if yunit0!="":
        ylabel+="  ("+yunit0+")"

    if not(xIsUVcoord):
        xdata=[d[xarr].data[xname] for d in oifitsList]
    elif xname=="SPAFREQ":
        xdata=[getSpaFreq(d,arr=yarr) for d in oifitsList]
    elif xname=="UCOORD" and yname!="VCOORD":
        pass
        #TODO
    elif xname=="B":
        xdata=[np.transpose(np.tile(getBaselineLengthAndPA(d,arr=yarr)[0],(np.shape(d[yarr].data[yname])[1],1))) for d in oifitsList]
        
    elif xname=="PA":
        xdata=[np.transpose(np.tile(getBaselineLengthAndPA(d,arr=yarr)[1],(np.shape(d[yarr].data[yname])[1],1))) for d in oifitsList]
         

    if not(yIsUVcoord):
        ydata=[d[yarr].data[yname] for d in oifitsList]
        ydataerr=[d[yarr].data[yerrname] for d in oifitsList]

    if axe:
        myPlot=axe
    else:
        myPlot=plt.axes()

    for idata in range(ndata):
        shapex=np.shape(ydata[idata])
        shapey=np.shape(xdata[idata])
        
        if (np.size(shapex)==2 and  np.size(shapey)==2):
            nB=shapex[0]
            for iB in range(nB):
                flags=oifitsList[idata][yarr].data["FLAG"][iB,:]
                if removeFlagged==True: 
                    idx=np.where(flags==False)[0]
                else:
                    nlam=len(flags)
                    idx=range(nlam)
                if linestyle!=None and linestyle!="":
                    myPlot.plot(xdata[idata][iB,idx]*xunitmultiplier,ydata[idata][iB,idx],color=_getNextColor(icol,colorTab))
                if errorbar==True:
                    myPlot.errorbar(xdata[idata][iB,idx]*xunitmultiplier,ydata[idata][iB,idx],yerr=ydataerr[idata][iB,idx],linestyle="",color=_getNextColor(icol,colorTab))
                icol+=1
        else:
            #TODO
            pass

            #myPlot.errorbar(xdata[idata]*1e6,ydata[idata][iB,:],yerr=ydataerr[idata][iB,:],linestyle="")

    if yscale!=None:
        myPlot.set_yscale(yscale)

    if xscale!=None:        
        myPlot.set_xscale(xscale)
    
    myPlot.set_xlim(xlim)
    myPlot.set_ylim(ylim)
    myPlot.set_xlabel(xlabel)
    myPlot.set_ylabel(ylabel)
#----------------------------------------------getBaselineLengthAndPA------------------------------------------

def getBaselineLengthAndPA(oifits,arr="OI_VIS2"):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits
    u=data[arr].data["UCOORD"]
    v=data[arr].data["VCOORD"]
    B=np.sqrt(u**2+v**2)
    PA=np.rad2deg(np.arctan2(u,v))

    # TODO OI_T3
    return B,PA

#-------------------------------------------------getSpaFreq--------------------------------------------------

def getSpaFreq(oifits,arr="OI_VIS2",unit=None):
    if type(oifits)==type(""):
        data=fits.open(oifits)
    else:
        data=oifits


    if arr!="OI_T3":
        B,PA=getBaselineLengthAndPA(data,arr)

    else:
        u1=data[arr].data["U1COORD"]
        v1=data[arr].data["V1COORD"]
        u2=data[arr].data["U2COORD"]
        v2=data[arr].data["V2COORD"]
        u3=u1+u2
        v3=v1+v2
        B1=np.sqrt(u1**2+v1**2)
        B2=np.sqrt(u2**2+v2**2)
        B3=np.sqrt(u3**2+v3**2)
        Bs=[B1,B2,B3]
        B=np.max(Bs,axis=0)

    lam=data["OI_WAVELENGTH"].data["EFF_WAVE"]
    nlam=np.size(lam)
    nB=np.size(B)
    if unit=="cycles/mas":
        mult=1./rad2mas
    elif unit=="cycles/arcsec":
        mult=1./rad2arcsec
    elif unit == "Mlam":
        mult = 1/(1e6)
    else:
        mult=1

    spaFreq=np.ndarray([nB,nlam])
    for iB in range(nB):
        spaFreq[iB,:]=B[iB]/lam*mult

    return spaFreq






keys_links = {"VIS2DATA": "OI_VIS2",
              "VIS2ERR": "OI_VIS2",
              "VISAMP": "OI_VIS",
              "VISAMPERR": "OI_VIS",
              "VISPHI": "OI_VIS",
              "VISPHIERR": "OI_VIS",
              "UCOORD": "OI_VIS",
              "VCOORD": "OI_VIS",
              "T3AMP": "OI_T3",
              "T3AMPERR": "OI_T3",
              "T3PHI": "OI_T3",
              "T3PHIERR": "OI_T3",
              "EFF_WAVE": "OI_WAVELENGTH"}


def get_data(oifits, dataName):
    data = open_or_itself(oifits)
    return data[keys_links[dataName]].data[dataName]


def open_or_itself(oifits, throw_error = False):
    if type(oifits) == type(""):
        return fits.open(oifits)
    if type(oifits) == type([]):
            return [open_or_itself(oi) for oi in oifits]
    return oifits

def open_and_list(oifits):
    if not type(oifits) == type([]):
        return open_or_itself([oifits])
    return open_or_itself(oifits)


def fmtflags(dat, idx):
    tempdat = [[] for i in range(np.max(idx[0])+1)]
    for m in range(len(idx[0])):
        i = idx[0][m]
        j = idx[1][m]
        tempdat[i].append(dat[i][j])
    dat = tempdat
    return dat

def flagOut(data, flags):
    out = []
    for i in range(len(data)):
        tmp = []
        for j in range(len(data[i])):
            if len(np.shape(flags)) == 1 :
                if flags[i][j] == False:
                    tmp.append(data[i][j])
            else:
                theflags = [flags[k][i][j] for k in range(len(flags))]
                if not (True in theflags):
                    tmp.append(data[i][j])
        out.append(tmp)
    
    return out


def newPlot(oifits, x = "SPA_FREQ", y = "VISAMP",
            spafreq_unit = "cycles/arcsec",
            figsize = (16,12),
            error_axis = "y",
            errorbars = True,
            use_flags = False,
            cmap = "jet",
            marker = ".",
            vmin = None,
            vmax = None,
            xlim = None,
            ylim = None,
            overlap = False,
            orientation = False,
            color_per_file = False,
            title = None,
            **kwargs):
    
    if x != "SPA_FREQ" and x not in keys_links:
         raise ValueError("Couldn't find '{}' in 'keys_links'".format(x))
    if y != "SPA_FREQ" and y not in keys_links:
        raise ValueError("Couldn't find '{}' in 'keys_links'".format(y))
    
    
    if type(oifits) != type([]):
        ois = [open_or_itself(oifits)]
    else:
        ois = [open_or_itself(m) for m in oifits]
    
    if overlap == True:
        ax = plt.gca()
    elif not type(overlap) == bool and overlap is not None:
        ax = overlap
    else:
        fig, ax = plt.subplots(figsize = figsize)
    
    # Searchng vmin and vmax for wavelengths in all files
    themin = None
    themax = None
    for data in ois:
        lams = np.dot(1e6,data["OI_WAVELENGTH"].data['EFF_WAVE'])
        lam_min = np.min(lams)
        lam_max = np.max(lams)
        if themin is None:
            themin = lam_min
        else:
            if lam_min < themin:
                themin = lam_min
        if themax is None:
            themax = lam_max
        else:
            if lam_max > themax:
                themax = lam_max
    
    if vmin is None:
        vmin = themin
    if vmax is None:
        vmax = themax
        
    for num,data in enumerate(ois):
        # If SPA_FREQ is asked
        try:
            a = data["OI_VIS"]
        except:
            continue
        if x == "SPA_FREQ":
            xdata = getSpaFreq(data, arr = keys_links[y], unit = spafreq_unit)
            xlabel = x + " ({})".format(spafreq_unit)
        else:
            xdata = data[keys_links[x]].data[x]
            xlabel = x
            if use_flags == True:
                xflags = data[keys_links[x]].data["FLAG"]
            if errorbars == True and error_axis == "x":
                if not x == "VIS2DATA":
                    xerr = data[keys_links[x]].data[x + "ERR"]
                else:
                    xerr = data[keys_links[x]].data["VIS2" + "ERR"]
            
        if y == "SPA_FREQ":
            ydata = getSpaFreq(data, arr = keys_links[x], unit = spafreq_unit)
            ylabel = y + " ({})".format(spafreq_unit)
        else:
            ydata = data[keys_links[y]].data[y]
            ylabel = y
            if use_flags == True:
                yflags = data[keys_links[y]].data["FLAG"]
            if errorbars == True and error_axis == "y":
                if not y == "VIS2DATA":
                    yerr = data[keys_links[y]].data[y + "ERR"]
                else:
                    yerr = data[keys_links[y]].data["VIS2" + "ERR"]
        
        # Converting wavelength to um
        lams = np.dot(1e6,data["OI_WAVELENGTH"].data['EFF_WAVE'])
        lams = np.array([lams for i in range(len(xdata))])
        # print(np.shape(lams))
        if orientation == True:
            us = data[keys_links[y]].data["UCOORD"]
            # print(np.shape(us))
            us = [[us[j] for i in range(len(lams[j]))] for j in range(len(us))]
            # print(np.shape(us))
            vs = data[keys_links[y]].data["VCOORD"]
            vs = [[vs[j] for i in range(len(lams[j]))] for j in range(len(vs))]
            
            # print(np.shape(vs))
            theta = np.arctan(np.divide(vs,us)) * 180/np.pi
            theta = np.add(theta,90)
            lams = theta
            vmin = 0
            vmax = 180
            # print(np.shape(lams))
        
        if use_flags == True:
            if x == "SPA_FREQ":
                xflags = yflags
            if y == "SPA_FREQ":
                yflags = xflags
            
            xidx = np.where(xflags == False)
            xdata = fmtflags(xdata, xidx)
            
            yidx = np.where(yflags == False)
            ydata = fmtflags(ydata, yidx)
            
            lams = fmtflags(lams, yidx)
            
            
            if errorbars == True:
                if error_axis == "x":
                    xerr = fmtflags(xerr, xidx)
                if error_axis == "y":
                    yerr = fmtflags(yerr, yidx)
                    
        norm = mcolors.Normalize(vmin=vmin, vmax=vmax, clip=True)
        mapper = cm.ScalarMappable(norm=norm, cmap=cmap)
        color_tab = np.array([(mapper.to_rgba(v)) for v in lams])
        
        
        
        
        
        for xval,yval, thelam, i in zip(xdata, ydata, lams, range(len(xdata))):
            
            if color_per_file == True:
                prop_cycle = plt.rcParams['axes.prop_cycle']
                cols = prop_cycle.by_key()['color']
                kwargs["color"] = cols[num%len(cols)]
                kwargs["label"] = data.filename()
                if i != 0:
                    del(kwargs["label"])
                
            if "color" in kwargs:
                cmap = None
            if cmap is None:
                thelam = None
            
            if errorbars:
                sc = ax.scatter(xval, yval, c = thelam, cmap = cmap, vmin = vmin, vmax = vmax, marker = marker, zorder = 1, s = 0, **kwargs)
            else:
                if thelam == None:
                    sc = ax.plot(xval, yval, zorder = 1, **kwargs)
                else:
                    meanlam = (np.array(thelam[:-1]) + np.array(thelam[1:]))/2
                    points = np.array([xval, yval]).T.reshape(-1, 1, 2)
                    segments = np.concatenate([points[:-1], points[1:]], axis=1)
                    
                    norm = plt.Normalize(np.min(thelam), np.max(thelam))
                    lc = LineCollection(segments, cmap=cmap, norm=norm, **kwargs)
                    lc.set_array(np.array(meanlam))
                    lc.set_linewidth(2)
                    sc = ax.add_collection(lc)

                
                
            if errorbars == True:
                if error_axis == "y":
                    xer = None
                    yer = yerr[i]
                if error_axis == "x":
                    xer = xerr[i]
                    yer = None
                ax.errorbar(xval, yval, yerr = yer, xerr = xer, marker = " ", ecolor = color_tab[i], zorder = -1,linestyle='', **kwargs)

                
               
             
    ax.set_xlim(np.nanmin(xdata), np.nanmax(xdata))
    ax.set_ylim(np.nanmin(ydata), np.nanmax(ydata))
    
    
    
    if x == "VISAMP" or x == "VIS2DATA" and xlim is None:
        print("'{}' asked on x axis, setting limits to (-0.1, 1.1)".format(x))
        ax.set_xlim(-0.1,1.1)
    if y == "VISAMP" or y == "VIS2DATA" and ylim is None:
        print("'{}' asked on y axis, setting limits to (-0.1, 1.1)".format(y))
        ax.set_ylim(-0.1,1.1)
    if x == "SPA_FREQ" and xlim is None:
        print("'{}' asked on x axis, setting lower limit to 0".format(x))
        ax.set_xlim(left = 0)
    if y == "SPA_FREQ" and ylim is None:
        print("'{}' asked on y axis, setting lower limit to 0".format(y))
        ax.set_ylim(bottom = 0)
    
    if color_per_file == True:
        ax.legend()
    
    if xlim is not None:
        ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    if title is not None:
        ax.set_title(title)
        
    if overlap == False and color_per_file != True:
        plt.gca()
        cb = plt.colorbar(sc, ax = ax)
        cb.set_label("Wavelength (um)")
        if orientation == True:
            cb.set_label("Baseline orientation (deg)")



def myplot(oifits, x, y, vmin = None, vmax = None, cmap = "jet",
           xlim = None, ylim = None,
           single_plot = True,
           figsize = (15,10),
           spafreq_unit = "Mlam",
           use_flag = None,
           title = None,
           **kwargs):
    """
    Note : 'use_flag' must be in the format ("OI_VIS2", "FLAG_ERR")
    """
    
    data = open_or_itself(oifits)
    
    if x == y:
        raise ValueError("'x' and 'y' have the same values")
    
    # Making sure that x and y are both 'searchable'
    if x != "SPA_FREQ" and y != "SPA_FREQ":
        if not x in keys_links:
            raise ValueError("Couldn't find '{}' in 'keys_links'".format(x))
        
        if not y in keys_links:
            raise ValueError("Couldn't find '{}' in 'keys_links'".format(y))
    
    # If SPA_FREQ is asked
    if x == "SPA_FREQ":
        ykey = keys_links[y]
        xdata = getSpaFreq(data, arr = ykey, unit = spafreq_unit)
        ydata = data[ykey].data[y]
        
        x = x + " ({})".format(spafreq_unit)
        
    elif y == "SPA_FREQ":
        xkey = keys_links[x]
        ydata = getSpaFreq(data, arr = xkey, unit = spafreq_unit)
        xdata = data[xkey].data[x]
        
        y = y + " ({})".format(spafreq_unit)
        
    else:
        xkey = keys_links[x]
        ykey = keys_links[y]
        xdata = data[xkey].data[x]
        ydata = data[ykey].data
    
    
    # Converting wavelength to um
    lams = np.dot(1e6,data["OI_WAVELENGTH"].data['EFF_WAVE'])
    
    # Wavelengths with the same dimensions as the rest
    lams = [lams for i in range(len(xdata))]
    
    if vmin is None:
        vmin = np.min(lams)
    if vmax is None:
        vmax = np.max(lams)
        
    # if colors is None:
    #     colors = lams
    
    
    # In case of use in multiplot() function
    if single_plot == True:
        plt.figure(figsize = figsize)
    else:
        plt.gca()
        
    # Ignoring flagged values
    if use_flag is not None:
        if len(np.shape(use_flag)) == 1 :
            use_flag = [use_flag]
        flags = [data[flag[0]].data[flag[1]] for flag in use_flag]
        xdata = flagOut(xdata, flags)
        ydata = flagOut(ydata, flags)
        lams = flagOut(lams, flags)
        
    if not "marker" in kwargs:
        kwargs["marker"] = "+"
    
    for xval,yval, thelam in zip(xdata, ydata, lams):
        if cmap is None:
            thelam = None
        plt.scatter(xval, yval, c = thelam, cmap = cmap, vmin = vmin, vmax = vmax, **kwargs)
    
    
    if x == "VISAMP" or x == "VIS2DATA":
        print("'{}' asked on x axis, setting limits to (-0.1, 1.1)".format(x))
        plt.xlim(-0.1,1.1)
    if y == "VISAMP" or y == "VIS2DATA":
        print("'{}' asked on y axis, setting limits to (-0.1, 1.1)".format(y))
        plt.ylim(-0.1,1.1)
    
    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)
    
    if single_plot == True:
        if cmap is not None:
            cb = plt.colorbar()
            cb.set_label("Wavelength (um)")
    
    plt.xlabel(x)
    plt.ylabel(y)
    
    if title is not None:
        plt.title(title)


def multiplot(oifits, x, y, vmin = None, vmax = None, cmap = "jet",
           xlim = None, ylim = None,
           figsize = (15,10),
           spafreq_unit = "Mlam", use_current_plot = False, ignore_cb = False,
           **kwargs):
    
    if use_current_plot == True:
        plt.gca()
    else:
        plt.figure(figsize = figsize)
    for oi in oifits:
        myplot(oi, x, y,
               vmin=vmin,
               vmax=vmax,
               cmap = cmap,
               xlim = xlim,
               ylim = ylim,
               single_plot = False,
               spafreq_unit = spafreq_unit, **kwargs)
    
    if ignore_cb == False:
        cb = plt.colorbar()
        cb.set_label("Wavelength (um)")



def getBandsLimits():
    bands = {"L": [3.0e-6, 4.0e-6],
             "M": [4.5e-6, 5.0e-6],
             "N": [8.0e-6, 12.0e-6]}
    return bands


def getOrientationDensity(file, arr = "OI_VIS", save = None, **kwargs):
    ois = []
    if type(file) == type([]):
        for f in file:
            ois.append(open_or_itself(f))
    else:
        ois.append(open_or_itself(file))
    
    thetas = np.array([])
    lengths = np.array([])
    for oi in ois:
        u = oi[arr].data["UCOORD"]
        v = oi[arr].data["VCOORD"]
        theta = np.arctan(np.divide(u, v)) * 180/np.pi
        thetas = np.append(thetas, theta)
        lengths = np.append(lengths, np.sqrt((np.array(u)**2 + np.array(v)**2)))
    
    plt.figure()
    plt.hist(thetas, bins = 30)
    plt.xlabel("U,V angle (deg)")
    plt.ylabel("Number of baselines")
    
    if "title" in kwargs:
        plt.title(kwargs["title"])
    plt.show()
    
    checks = np.linspace(-90,90,100)
    
    tmps = []
    for c in checks:
        # print(le)
        sm = []
        for th,le in zip(thetas, lengths):
            # print("--")
            # print(np.abs(np.cos(np.deg2rad(th-c)))*le)
            sm.append(np.abs(np.cos(np.deg2rad(th-c)))*le)
        # tmps.append(np.mean(sm))
        tmps.append(np.max(sm))
    
    # print(np.shape(lengths))
    # print(np.shape(checks))
    # print(np.shape(tmps))
    plt.figure()
    plt.plot(checks, tmps)
    plt.gca().set_ylim(bottom = 0)
    plt.xlabel("PA (deg)")
    plt.ylabel("Max projection for all baselines at this angle")
    
    if "title" in kwargs:
        plt.title(kwargs["title"])
    
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()
        
        
        
        
        