



import json
import numpy as np
import os
import astropy
import pandas as pd


def get_tree(directory = "./", get_file = "best_fit_model.json"):
    tree = {}
    origin = os.getcwd()
    os.chdir(directory)
    cwd = os.getcwd()
    
    if len(os.listdir()) == 0:
        return None
    
    for file in os.listdir():
        os.chdir(cwd)
        # print(file)
        if file == get_file:  
            # Opening JSON file
                tree["fits_results"] = open_json(file)
        elif os.path.isdir(file):
            tree[file] = get_tree(file)
    
    os.chdir(origin)
    return tree



class CustomEncoder(json.JSONEncoder):
    """ 
    Credits to 'hmallen' : https://github.com/hmallen/numpyencoder/blob/f8199a61ccde25f829444a9df4b21bcb2d1de8f2/numpyencoder/numpyencoder.py
    Custom encoder for numpy data types
    
    -> Added some astropy.units support"""
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):

            return int(obj)

        elif isinstance(obj, (np.float_, np.float16, np.float32, np.float64)):
            return float(obj)
        
        elif isinstance(obj, (np.complex_, np.complex64, np.complex128)):
            return {'real': obj.real, 'imag': obj.imag}
        
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
    
        elif isinstance(obj, (np.bool_)):
            return bool(obj)

        elif isinstance(obj, (np.void)): 
            return None
        
        elif isinstance(obj, astropy.units.quantity.Quantity):
            return obj.__str__()
        
        
        return json.JSONEncoder.default(self, obj)

class CustomEncoderErrorsToStr(json.JSONEncoder):
    """ 
    Credits to 'hmallen' : https://github.com/hmallen/numpyencoder/blob/f8199a61ccde25f829444a9df4b21bcb2d1de8f2/numpyencoder/numpyencoder.py
    Custom encoder for numpy data types
    
    -> Added some astropy.units support"""
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):

            return int(obj)

        elif isinstance(obj, (np.float_, np.float16, np.float32, np.float64)):
            return float(obj)
        
        elif isinstance(obj, (np.complex_, np.complex64, np.complex128)):
            return {'__complex__': True, 'real': obj.real, 'imag': obj.imag}
        
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
    
        elif isinstance(obj, (np.bool_)):
            return bool(obj)

        elif isinstance(obj, (np.void)): 
            return None
        
        elif isinstance(obj, astropy.units.quantity.Quantity):
            return obj.__str__()
        
        else:
            return obj.__str__()


def custom_json_hook(dct):
    if '__complex__' in dct:
        return complex(dct['real'], dct['imag'])
    
    for key in dct:
        if key == "unit":
            dct[key] = astropy.units.Unit(dct[key])
        if type(dct[key]) == type(0.1):
            if np.isinf(dct[key]):
                if dct[key] > 0:
                    dct[key] = np.inf
                else:
                    dct[key] = -np.inf
    
    return dct


def dict_to_json(dic, fname, sort_keys = False, indent = 4, encoder = CustomEncoder, errors_to_str = False):
    """
    'sort_keys' should pretty much always be False.
    Re-ordering the keys in the dictionnaries is very likely to lead to several issues in the rest of the code.
    """
    
    if encoder is None:
        encoder = json.JSONEncoder
    
    try:
        json.dump(dic, open(fname, 'w+'), sort_keys = sort_keys, indent = indent, cls=CustomEncoder)
    except:
        if errors_to_str == True:
            try:
                json.dump(dic, open(fname, 'w+'), sort_keys = sort_keys, indent = indent, cls=CustomEncoderErrorsToStr)
            except Exception as err:
                raise TypeError("""Couldn't serialize a value, even by turning it to .__str__() \n Terminated with error : \n{}""".format(err))
            
            
    
    
def open_json(fname):
    with open(fname) as json_file:
        data = json.load(json_file)
    return data

def isjsonable(val, encoder = CustomEncoder):
    if encoder is None:
        encoder = json.JSONEncoder
    try:
        json.dump(val, open("temp.temp.json", 'w+'))
        os.remove("temp.temp.json")
        return True
    except:
        print(val, type(val), type(val).__name__)
        return False




def concat(dfs, axis=0, *args, **kwargs):   
    """
    Wrapper for `pandas.concat'; concatenate pandas objects even if they have 
    unequal number of levels on concatenation axis.
    
    Levels containing empty strings are added from below (when concatenating along
    columns) or right (when concateniting along rows) to match the maximum number 
    found in the dataframes.
    
    Parameters
    ----------
    dfs : Iterable
        Dataframes that must be concatenated.
    axis : int, optional
        Axis along which concatenation must take place. The default is 0.

    Returns
    -------
    pd.DataFrame
        Concatenated Dataframe.
    
    Notes
    -----
    Any arguments and kwarguments are passed onto the `pandas.concat` function.
    
    See also
    --------
    pandas.concat
    """
    
    def index(df):
        return df.columns if axis==1 else df.index
    
    def add_levels(df):
        need = want - index(df).nlevels
        if need > 0:
            df = pd.concat([df], keys=[("\phantom{{}}",)*need], axis=axis) # prepend empty levels
            for i in range(want-need-1): # move empty levels to bottom
                # print("-----")
                # print(df)
                df = df.swaplevel(i, i+need, axis=axis) 
            # print(df)
        return df
    
    want = np.max([index(df).nlevels for df in dfs])    
    dfs = [add_levels(df) for df in dfs]
    return pd.concat(dfs, axis=axis, *args, **kwargs)



def pandas_to_latex(df, filename = "table.txt"):
    formatting = ""
    for i in range(len(df.columns)+1):
        formatting += "c|"
    with open(filename, 'w') as f:
        f.write(df.to_latex(multicolumn_format = "c|", column_format = formatting).replace('\\textbackslash ', '\\'))






def unlink_wrap(dat, lims=[-np.pi, np.pi], thresh = 0.95):
    """
    Thanks to farenorth : https://stackoverflow.com/questions/27138751/preventing-plot-joining-when-values-wrap-in-matplotlib-plots
    Iterate over contiguous regions of `dat` (i.e. where it does not
    jump from near one limit to the other).

    This function returns an iterator object that yields slice
    objects, which index the contiguous portions of `dat`.

    This function implicitly assumes that all points in `dat` fall
    within `lims`.

    """
    jump = np.nonzero(np.abs(np.diff(dat)) > ((lims[1] - lims[0]) * thresh))[0]
    lasti = 0
    for ind in jump:
        yield slice(lasti, ind + 1)
        lasti = ind + 1
    yield slice(lasti, len(dat))