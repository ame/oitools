# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 10:08:07 2021

@author: Ame
"""


from distutils.core import setup

setup(name='oitools',
      version='0.1',
      description='Optical Interferometry Tools Package',
      author='Anthony Meilland',
      author_email='ame@oca.eu',
      url='https://gitlab.oca.eu/ame/oitools',
      packages=['oitools'])
