#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 17:06:03 2021

@author: ddemars
"""

import os
from oitools import oimodel as oim
import numpy as np
import oitools.oifitstools as oiut
from astropy.io import fits
import pandas as pd
import oitools.utils as ut
import matplotlib.pyplot as plt

results_dir = "/home/ddemars/Documents/Stage_Nice/results"
data_dir =  "/home/ddemars/Documents/Stage_Nice/data/LM15/"


# data_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/LM15/"
# models_dir = "D:/User Documents/Documents/Projects/python modules/oitools/data_fitting/models/"
# results_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/results_ter_old/L/"


models = "all"
models = ["gauss", "egauss", "egauss_bckg", "egauss_punct", "egauss_punct_bckg"]
chrom = "achrom"
fitted_data = "VISAMP"
band = "M"

# models = ["egauss"]


lams = [oiut.getBandsLimits()[band][0], oiut.getBandsLimits()[band][1]]

results = {}


# models = ["egauss"]

os.chdir(results_dir + "/" + band)

stars = os.listdir()



def test(obj):
    return(str(obj))

# stars = ['CPD-529243']
# models = ["egauss_punct_bckg"]


stars = [star for star in stars if not ".table" in star]
print(stars)

for modname in models:
    dfs = []
    
    for star in stars:
        print("===============")
        print(star)
        os.chdir(results_dir)
        os.chdir(band)
        os.chdir(star)
        star_dir = os.getcwd()
        
        if models == "all":
            models = os.listdir( )
        
        results[star] = {}
    # for modname in models:
        print("--" + modname)
        os.chdir(star_dir)
        os.chdir(modname)
        mod_dir = os.getcwd()
        
        os.chdir(chrom)
        os.chdir(fitted_data)
        try:
            sim = oim.rebuild_simulator(data_dir + star + "/", band)
        except Exception as err:
            print("failed : ", err)
            continue
        # sim.plotOrientationDensity(title = star)
        # mode = "peak"
        # chi2max = "min+10"
        # discard = 10000
        # sim.fitter.getBestFit(discard = discard, mode = mode, chi2max = chi2max)
        
        # chi2_best = sim.fitter.computechi2(reduce = True)
        # print(chi2_best)
        
        # sim.fitter.getBestFit(discard = discard, mode = "peak", chi2max = chi2max)
        sim.fitter.getBestFit(discard = 14000, mode = "best", chi2max = "min+10")
        chi2_best = sim.fitter.computechi2(reduce = True)
        sim.fitter.model = oim.oim_model().load(filename = "model.json")
        sim.model = sim.fitter.model
        chi2_peak = sim.fitter.computechi2(reduce = True)
        
        # print(chi2_peak)
        # sim.plotDataModel(save = True, title = star)       
        # continue
        ch2df_peak = pd.DataFrame({"chi2r\\_peak": [chi2_peak]}, index = ["value"])
        ch2df_best = pd.DataFrame({"chi2r\\_best": [chi2_best]}, index = ["value"])
        ch2df = ut.concat([ch2df_peak,ch2df_best], axis = 1)
        
        thedf = sim.model.toPandas(to_get = ["value"], ignore = ["x", "y"], only_free = False)
        thedf = ut.concat([thedf,ch2df], axis = 1)
        
        thedf = thedf.round(decimals=2)
        dfs.append(thedf)
        
        
    final_df = pd.concat(dfs, keys = stars)
    print(final_df)
    
    
    fmt = ""
    for i in range(len(final_df.index.levels)):
        fmt += "l"
    fmt += "|"
    last = final_df.columns[0][-2]
    for col in final_df.columns:
        cur = col[-2]
        if cur != last:
            fmt += "|"
            last = cur
        fmt += "c"
    fmt += "|"
    

#%%





    os.chdir(results_dir)
    with open(modname + ".table", "w") as f:
        lat = final_df.to_latex(multicolumn_format = "c|", column_format = fmt, escape = False)
        lat = lat.replace("\\textbackslash phantom", "\\phantom{}")
        f.write(lat)





#%%

# import pandas as pd
# import numpy as np



# df = pd.Series(np.random.rand(3), index=["a","b","c"]).to_frame().T
# df.columns = pd.MultiIndex.from_product([["new_label"], df.columns])




