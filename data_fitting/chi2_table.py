#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 17:06:03 2021

@author: ddemars
"""

import os
from oitools import oimodel as oim
import numpy as np
import oitools.oifitstools as oiut
from astropy.io import fits
import pandas as pd
import oitools.utils as ut
import matplotlib.pyplot as plt


results_dir = "/home/ddemars/Documents/Stage_Nice/results/"
data_dir =  "/home/ddemars/Documents/Stage_Nice/data/"
# models_dir = "D:/User Documents/Documents/Projects/python modules/oitools/data_fitting/models/"

# data_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/"
# models_dir = "D:/User Documents/Documents/Projects/python modules/oitools/data_fitting/models/"
# results_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/results_ter/"


models = "all"
models = ["gauss", "egauss", "egauss_bckg", "egauss_punct", "egauss_punct_bckg"]
chrom = "chrom"
fitted_data = "VISAMP"
bands = ['L', 'M', 'N']


links = {"L": "/LM15/",
         "M": "/LM15/",
         "N": "/N21/"}

# lams = [oiut.getBandsLimits()[band][0], oiut.getBandsLimits()[band][1]]

results = {}




os.chdir(data_dir + "/LM15/")

stars = os.listdir()



# stars = ['CPD-529243']
# models = ["egauss_punct_bckg"]


stars = [star for star in stars if not ".table" in star]

bands = ["L", "M", "N"]
# stars = ["lPup"]
# models = ["egauss"]

models_dfs = []
for modname in models:
    stars_dfs = []
    # star_chi2s = []
    for star in stars:
        bands_chi2 = []
        for band in bands:
            # bands_chi2.append([1, 1])
            # continue
            try:
                print("===============")
                print(star)
                os.chdir(results_dir)
                os.chdir(band)
                os.chdir(star)
                star_dir = os.getcwd()
                
                
                
                print("--" + modname)
                os.chdir(star_dir)
                os.chdir(modname)
                mod_dir = os.getcwd()
                
                os.chdir(chrom)
                os.chdir(fitted_data)
            
                os.chdir(star_dir)
                os.chdir(modname)
                mod_dir = os.getcwd()
                
                os.chdir(chrom)
                os.chdir(fitted_data)
                sim = oim.rebuild_simulator(data_dir + links[band] + star + "/", band, verbose = False)
            except Exception as err:
                print(err)
                bands_chi2.append(["--", "--"])
                continue
            
            
            chi2_peak = np.round(sim.fitter.computechi2(reduce = True), 2)
            
            # sim.plotOrientationDensity(title = star)
            chi2max = "min+10"
            discard = 10000
            try:
                sim.fitter.getBestFit(discard = discard, mode = "best", chi2max = chi2max)
                chi2_best = np.round(sim.fitter.computechi2(reduce = True), 2)
            except Exception as err:
                print(err)
                chi2_best = "--"
            # print(chi2_best)
            
            # try:
            #     sim.fitter.getBestFit(discard = discard, mode = "peak", chi2max = chi2max)
            #     chi2_peak = np.round(sim.fitter.computechi2(reduce = True), 2)
            # except Exception as err:
            #     print(err)
            #     chi2_peak = "--"
            print(chi2_best, chi2_peak)
            # sim.plotDataModel(save = True, title = star)      
            bands_chi2.append([chi2_best, chi2_peak])
        # star_chi2.append(band_chi2)
    
        header = pd.MultiIndex.from_product([[modname],["best", "peak"]])
        index = pd.MultiIndex.from_product([[star],bands])
        thedf = pd.DataFrame(bands_chi2, index = index, columns = header)
    # thedf = thedf.round(decimals=2)
        stars_dfs.append(thedf)
    
    model_df = ut.concat(stars_dfs,axis = 0)
    models_dfs.append(model_df)



#%%

mode = "best"
discard = 10000
chi2max = "min+10"
# sim.fitter.cornerPlot(discard = discard, mode = mode, chi2max = chi2max, plot_hists = False)
sim.getBestFit(mode = mode, chi2max = chi2max, discard = discard)
sim.compute()
sim.plotDataModel()

mode = "peak"
# discard = 10000
# chi2max = "min*5"
sim.fitter.cornerPlot(discard = discard, mode = mode, zoom = 3, chi2max = chi2max, plot_hists = False)
sim.getBestFit(mode = mode, chi2max = chi2max, discard = discard)
sim.compute()
sim.plotDataModel()

#%%
 
 
final_df = ut.concat(models_dfs,axis = 1)
print(final_df)

#%%

fmt = "|"
for i in range(len(final_df.index.levels)):
# for i in range(1):
    fmt += "l|"
# fmt += "|"
last = final_df.columns[0][-2]
for col in final_df.columns:
    cur = col[-2]
    if cur != last:
        fmt += "|"
        last = cur
    fmt += "c"
fmt += "|"


os.chdir(results_dir)
with open(chrom+"_chi2s.table", "w") as f:
    lat = final_df.to_latex(multicolumn_format = "c|", column_format = fmt, multirow= True)
    lat = lat.replace("\\textbackslash phantom", "\\phantom{}")
    f.write(lat)





#%%

# import pandas as pd
# import numpy as np



# df = pd.Series(np.random.rand(3), index=["a","b","c"]).to_frame().T
# df.columns = pd.MultiIndex.from_product([["new_label"], df.columns])






