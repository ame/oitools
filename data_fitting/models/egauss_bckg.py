#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 13:58:26 2021

@author: ddemars
"""



"""
Template model, including :
        - Elongated Gaussian 
        - Background

"""




import oitools.oimodel as oim
import oitools.oifitstools as ut
import numpy as np
from copy import deepcopy



def getModel(chrom = True, band = "L", points = 3):
    if chrom == True:
        bands_limits = ut.getBandsLimits()[band]
        lams = np.linspace(bands_limits[0], bands_limits[-1], points)
        vals = [1 for l in lams]
        param = oim.oim_interp1d(lams, vals)
    else:
        param = 1
    
    bckg = oim.oim_bckg(f = deepcopy(param))
    eg=oim.oim_egauss(fwhm = deepcopy(param), f= 1, pa = 0, elong = 1)
    
    
    model = oim.oim_model([bckg, eg])
    
    model.setDefaults()
    
    
    bckg.params["x"].free = False
    bckg.params["y"].free = False
    eg.params["f"].free = False
    eg.params["x"].free = False
    eg.params["y"].free = False
    
    
    return model
