#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 13:59:42 2021

@author: ddemars
"""





"""
Template model, including :
        - Elongated Gaussian 
        - Background
        - Point Source
"""




import oitools.oimodel as oim
from oitools import oifitstools as ut
import numpy as np

from copy import deepcopy






def getModel(chrom = True, band = "L", points = 3):
    if chrom == True:
        bands_limits = ut.getBandsLimits()[band]
        lams = np.linspace(bands_limits[0], bands_limits[-1], points)
        vals = [1 for l in lams]
        param = oim.oim_interp1d(lams, vals)
    else:
        param = 1
        
    p = oim.oim_pt(f = 1)
    bckg = oim.oim_bckg(f = deepcopy(param))
    eg=oim.oim_egauss(fwhm = deepcopy(param), f= deepcopy(param), pa = 0, elong = 1)
    
    
    model = oim.oim_model([p, bckg, eg])

    
    model.setDefaults()
    
    p.params["f"].free = False
    p.params["x"].free = False
    p.params["y"].free = False
    # eg.params["f"].free = False
    eg.params["x"].free = False
    eg.params["y"].free = False
    bckg.params["x"].free = False
    bckg.params["y"].free = False
    
    
    return model

