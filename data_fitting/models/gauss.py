#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 13:23:27 2021

@author: ddemars
"""




"""
Template model, including :
        -Elongated Gaussian 


"""




import oitools.oimodel as oim
from oitools import oifitstools as ut
import numpy as np
from copy import deepcopy



def getModel(chrom = True, band = "L", points = 3):
    if chrom == True:
        bands_limits = ut.getBandsLimits()[band]
        lams = np.linspace(bands_limits[0], bands_limits[-1], points)
        vals = [1 for l in lams]
        param = oim.oim_interp1d(lams, vals)
    else:
        param = 1
    
    eg=oim.oim_gauss(fwhm = deepcopy(param), f= 1)
    model = oim.oim_model([eg])
    model.setDefaults()
    eg.params["x"].free = False
    eg.params["y"].free = False
    eg.params["f"].free = False
    
    
    return model




