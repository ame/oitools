#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 14:19:43 2021

@author: ddemars
"""









import os
import oitools.oifitstools as ut
import oitools.oimodel as oim
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors



data_dir = "/home/ddemars/Documents/Stage_Nice/data/"





bands_dir = {"L": "LM15/",
             "M": "LM15/",
             "N": "N21/"}


band = "N"

os.chdir(data_dir + "/" + bands_dir[band])


stars = "all"

if stars == "all":
    stars = os.listdir()

failed_stars = {}
for star_name in stars:
    os.chdir(data_dir + "/" + bands_dir[band])
    os.chdir(star_name)
    files = os.listdir()
    
    
    # Filtering for only choped files
    thefiles = []
    for file in files:
        if "noChop" in file or "simDat" in file:
            continue
        else:
            thefiles.append(file)
    files = thefiles
    
    # Cutting specified band
    ois = []
    for file in files:
        lams = [ut.getBandsLimits()[band][0], ut.getBandsLimits()[band][1]]
        if file.endswith(".fits"):
            oi = ut.cutWavelengthRange(fits.open(file), lams)
            ois.append(oi)
    
    if len(ois) == 0:
        failed_stars[star_name] = "no files"
        continue
    
    
    
    sim = oim.oim_simulator(oifits = ois)
    sim.plotData(title = star_name)
    sim.plotData(orientation = True, title = star_name)
    sim.plotData(color_per_file = True, title = star_name)