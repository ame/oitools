# -*- coding: utf-8 -*-
"""
Created on Thu May  6 18:41:31 2021

@author: Dorian
"""

import oitools.oimodel as oim
import oitools.fitter as fit
from oitools import oifitstools
import numpy as np
import os



"""
NOTES :
    - If you get the error "ValueError: Probability function returned NaN", then there is probably a 'NaN' value somewhere in the oifits file.
"""



os.chdir("D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/HD50138")
    
    
files= ["2020-02-15T022423_HD50138_A0B2D0C1_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2020-02-22T023050_HD50138_K0G2D0J3_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2020-02-29T011347_HD50138_A0G1J2J3_IR-LM_LOW_Chop_cal_oifits_0.fits"]#,
        # "2020-02-15T022423_HD50138_A0B2D0C1_IR-N_LOW_noChop_cal_oifits_0.fits",
        # "2020-02-22T023050_HD50138_K0G2D0J3_IR-N_LOW_noChop_cal_oifits_0.fits",
        # "2020-02-29T011347_HD50138_A0G1J2J3_IR-N_LOW_noChop_cal_oifits_0.fits"]

# files = ["2020-02-15T022423_HD50138_A0B2D0C1_IR-N_LOW_noChop_cal_oifits_0.fits",
#         "2020-02-22T023050_HD50138_K0G2D0J3_IR-N_LOW_noChop_cal_oifits_0.fits",
#         "2020-02-29T011347_HD50138_A0G1J2J3_IR-N_LOW_noChop_cal_oifits_0.fits"]





sim = oim.oim_simulator(oifits = files)

# oifitstools.cutWavelengthRange(sim.data[0], [3.1e-6,5e-6])
# oifitstools.cutWavelengthRange(sim.data[1], [3.1e-6,5e-6])
oifitstools.cutWavelengthRange(sim.data[2], [3.1e-6,5e-6])


simbis = oim.oim_simulator(oifits = files)




sim.setFitter()

# sim.fitter.computeSpaFreqs()
# sim.fitter.makeSimpleData()
# sim.fitter.makeFlattenData()
# sim.fitter.makeDataPerWavelength()

sim.fitted_data = ["VISAMP"]



# Big elongated gaussian
g=oim.oim_egauss(fwhm=oim.oim_interp1d([3e-6, 3.7e-6, 4.3e-6, 5e-6],[1,4,4, 4]),
                 f=oim.oim_interp1d([3e-6, 3.7e-6, 4.3e-6, 5e-6],[1,4,4,4]), 
                 elong = 1, pa = 0)

# Small gaussian for star + near environnement
g2 = oim.oim_gauss(fwhm = 1, f= 1)


m1=oim.oim_model([g, g2])





g.params["x"].free=False
g.params["y"].free=False
g2.params["x"].free=False
g2.params["y"].free=False
# g.params["f_interp2"].free=False

m1.getParameters()["f_1_interp1"].free = True
m1.getParameters()["f_1_interp2"].free = True
m1.getParameters()["f_1_interp1"].min = 0
m1.getParameters()["f_1_interp2"].min = 0
m1.getParameters()["f_1_interp3"].min = 0
m1.getParameters()["f_1_interp4"].min = 0
m1.getParameters()["f_1_interp1"].max = 50
m1.getParameters()["f_1_interp2"].max = 50
m1.getParameters()["f_1_interp3"].max = 50
m1.getParameters()["f_1_interp4"].max = 50



m1.getParameters()["f_2"].free = False


m1.getParameters()["fwhm_1_interp1"].min = 0
m1.getParameters()["fwhm_1_interp2"].min = 0
m1.getParameters()["fwhm_1_interp3"].min = 0
m1.getParameters()["fwhm_1_interp4"].min = 0
m1.getParameters()["fwhm_2"].min = 0
m1.getParameters()["fwhm_2"].max = 5

m1.getParameters()["fwhm_1_interp1"].max = 300
m1.getParameters()["fwhm_1_interp2"].max = 300
m1.getParameters()["fwhm_1_interp3"].max = 300
m1.getParameters()["fwhm_1_interp4"].max = 300


m1.getParameters()["elong_1"].min = 1
m1.getParameters()["elong_1"].max = 10
m1.getParameters()["pa_1"].min = 0
m1.getParameters()["pa_1"].max = 90


# g.params["f"][0].free = False


sim.setModel(m1)
sim.fitter.nsteps = 5000
sim.fitter.nwalkers = 32


sim.updateFitter(flagOut = True)

sim.fitter.makeInit(random = True)
#%%



sim.fitter.run()
sim.fitter.plotChain()

#%%
discard = 3000
sim.fitter.cornerPlot(discard = discard)


#%%


# tau = sim.fitter.sampler.get_autocorr_time()
# print(tau)



#%%



simbis.model = sim.model
simbis.fitter = sim.fitter

simbis.getBestFit()
simbis.compute()

simdat = simbis.simulatedData

        

simbis.plotDataModel()
#%%
simbis.save("./")


#%%


from astropy.io import fits
from oitools import oifitstools as ut


dir = "/home/ddemars/Documents/Stage_Nice/data/HD50138/"



os.chdir(dir)
files = os.listdir(dir)
files = [file for file in files if "0.fits" in file]


N = []
for t in files:
    if "N_LOW" in t:
        N.append(t)

LM = []
for t in files:
    if "LM_LOW" in t:
        LM.append(t)



"""
Plot LM
"""

oiss = ut.open_or_itself(N)
ois = []
for oi in oiss:
    ois.append(fits.open(oi))
# for oi in ois:
#     ut.flagVISAMPERR(oi, 0.2)
    
# oifitstools.cutWavelengthRange(ois[2], [3.1e-6,5e-6])



# ut.multiplot(ois, "SPA_FREQ", "VISAMP", xlim = (0,50), ylim = (-0.1,1.1), use_flag = [["OI_VIS", "FLAG_ERR"], ["OI_VIS", "FLAG"]], title = "Excluding FLAG and FLAG_ERR")


# ut.multiplot(simdat, "SPA_FREQ", "VISAMP", xlim = (0,50), ylim = (-0.1,1.1), title = "Excluding FLAG and FLAG_ERR", color = "black", vmin = None, vmax = None, cmap = None, use_current_plot = True, ignore_cb = True, alpha = 0.3)


# ut.multiplot(ois, "SPA_FREQ", "VISAMP", xlim = (0,50), ylim = (-0.1,1.1), title = "Full")

ut.multiplot(ois, "SPA_FREQ", "VISAMP", ylim = (-0.1,1.1), use_flag = ["OI_VIS", "FLAG"], title = "Excluding FLAG")

ut.multiplot(simdat, "SPA_FREQ", "VISAMP", ylim = (-0.1,1.1), title ="Excluding FLAG"  , color = "black", vmin = None, vmax = None, cmap = None, use_current_plot = True, ignore_cb = True, alpha = 0.3)



# ut.myplot(simdat, "SPA_FREQ", "VISAMP", xlim = (0,50), ylim = (-0.1,1.1), use_flag = [["OI_VIS", "FLAG_ERR"], ["OI_VIS", "FLAG"]], title = "Excluding FLAG and FLAG_ERR", cmap = None, vmin = None, vmax = None, color = "black")









#%%

# Plotting parametersper wavelength, to compare to old slice_files.py

fwhm0, fwhm1, elong1, pa1, f1, lams = np.loadtxt("/home/ddemars/Documents/Stage_Nice/data/HD50138/data_per_wl_N.txt", delimiter = ";" )


lams_fit = [8e-6, 10e-6, 11e-6, 13e-6]
f1_fit = mcmcs[0:4]
fwhm1_fit  = mcmcs[4:8]
elong1_fit = [mcmcs[8],mcmcs[8],mcmcs[8],mcmcs[8]]
pa1_fit = [mcmcs[9],mcmcs[9],mcmcs[9],mcmcs[9]]
fwhm0_fit = [mcmcs[10],mcmcs[10],mcmcs[10],mcmcs[10]]

import matplotlib.pyplot as plt

plt.figure()
plt.scatter(lams, fwhm0, marker = "+", label = "old")
plt.scatter(lams_fit, fwhm0_fit, marker = "+", label = "new")
plt.legend()
plt.xlabel("Lam (m)")
plt.ylabel("FWHM_0")

plt.figure()
plt.scatter(lams, fwhm1, marker = "+", label = "old")
plt.scatter(lams_fit, fwhm1_fit, marker = "+", label = "new")
plt.legend()
plt.xlabel("Lam (m)")
plt.ylabel("FWHM_1")

plt.figure()
plt.scatter(lams, elong1, marker = "+", label = "old")
plt.scatter(lams_fit, elong1_fit, marker = "+", label = "new")
plt.legend()
plt.xlabel("Lam (m)")
plt.ylabel("elong_1")

plt.figure()
plt.scatter(lams, pa1, marker = "+", label = "old")
plt.scatter(lams_fit, pa1_fit, marker = "+", label = "new")
plt.legend()
plt.xlabel("Lam (m)")
plt.ylabel("PA")

plt.figure()
plt.scatter(lams, f1, marker = "+", label = "old")
plt.scatter(lams_fit, f1_fit, marker = "+", label = "new")
plt.legend()
plt.xlabel("Lam (m)")
plt.ylabel("f1")