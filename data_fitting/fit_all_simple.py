#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 14:43:41 2021

@author: ddemars
"""

import os
import oitools.oifitstools as ut
import oitools.oimodel as oim
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import sys

import oitools

import matplotlib
# matplotlib.use('Agg')



def make_dirs(chrom, results_dir, fitted_data):
    # Save results
    if chrom == True:
        chromdir = "chrom/"
    else:
        chromdir = "achrom/"
    
    
    final_dir = results_dir + "/" + band + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    
    final_dir +=  star_name + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    final_dir += modname.split(".py")[0]
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    final_dir += "/" + chromdir
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    fdat_dir_name = ""
    for i in range(len(fitted_data)):
        fdat_dir_name += fitted_data[i]
        if i != len(fitted_data)-1:
            fdat_dir_name += " - "
    
    final_dir += "/" + fdat_dir_name + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    return final_dir


# WINDOWS

# data_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/"
# models_dir = "D:/User Documents/Documents/Projects/python modules/oitools/data_fitting/models/"
# results_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/results_ter/"


# PORTABLE

data_dir = "/home/ddemars/Documents/Stage_Nice/data/"
models_dir = "/home/ddemars/Documents/python_routines/oitools/data_fitting/models/"
results_dir = "/home/ddemars/Documents/Stage_Nice/results/"



# REMOTE

# data_dir = "/home/ddemars/data"
# models_dir = oitools.__git_dir__ + "/data_fitting/models/"
# results_dir = "/home/ddemars/fits/results/"


sys.path.append(models_dir)

bands_dir = {"L": "LM15/",
             "M": "LM15/",
             "N": "N21/"}





models = ["gauss.py",
          "egauss.py",
          "egauss_bckg.py",
          "egauss_punct.py",
          "egauss_punct_bckg.py"]

# models = ["egauss_punct_bckg.py"]



                                                                
band = "M"
chrom = False
points = 3
fitted_data = ["VISAMP"]

stars = "all"



stars = ["Hen3-298"]
models = ["egauss_bckg.py"]

failed_stars  = {}


os.chdir(data_dir + "/" + bands_dir[band])

if stars == "all":
    stars = os.listdir()
print(stars)    
for star_name in stars:
    os.chdir(data_dir + "/" + bands_dir[band])
    os.chdir(star_name)
    files = os.listdir()
    
    
    # Filtering for only choped files
    thefiles = []
    for file in files:
        if "noChop" in file or "simDat" in file:
            continue
        else:
            thefiles.append(file)
    files = thefiles
    
    # Cutting specified band
    ois = []
    for file in files:
        lams = [ut.getBandsLimits()[band][0], ut.getBandsLimits()[band][1]]
        if file.endswith(".fits"):
            oi = ut.cutWavelengthRange(fits.open(file), lams)
            ois.append(oi)
    
    if len(ois) == 0:
        failed_stars[star_name] = "no files"
        continue
    
    
    
    
    cwd = os.getcwd()
    for modname in models:
        os.chdir(cwd)
        print("")
        print("============================")
        print("---------------------------")
        print("Working in '{}'".format(star_name))
        print("band '{}' = ({}, {}) um".format(band, lams[0]*1e6, lams[1]*1e6))
        print("model = {}".format(modname))
        print("", flush = True)
        
        try:
            # Making model
            os.chdir(models_dir)
            if modname.endswith(".py"):
                # print(os.getcwd())
                exec("from {} import getModel".format(modname.split(".py")[0]))
                model = getModel(chrom = chrom, band = band, points = points) # error because 'getModel' is imported using 'exec("str_command")'
            else:
                print("continuing")
                continue
            
            os.chdir(cwd)
            
            if star_name in os.listdir():
                print("continuing")
                continue
            # print(1)
            
            final_dir = make_dirs(chrom, results_dir, fitted_data)
            # print(2)
            os.chdir(final_dir)
            # print(0)
            sim = oim.oim_simulator(oifits = ois)
            sim.setModel(model)
            # Checking for missing bands
            failed_files = []
            for i in range(len(sim.data)):
                if np.shape(sim.data[i]["OI_VIS"].data["VISAMP"]) == (0,):
                    failed_files.append(sim.data[i].filename())
            if len(failed_files) !=0:
                failed_stars[star_name] = ["Missing band in files for band {}, star {}".format(band, star_name), failed_files]
                print("Missing band in files for band {}, star {}".format(band, star_name))
                continue
            
            
            
            
            # sim.model.params["c2_eGauss_pa"].max = 0
            
            
            # Preparing fitter
            sim.setFitter(nsteps = 20000, nwalkers = 32, fitted_data = fitted_data, flagOut = True)
            
            sim.plotData(pdat = ["VISAMP"], title = star_name)
            
            
            
            
            # Running
            sim.runFit()
            
            
            sim.fitter.save(ignoreObject = True)
            
            # Plots
            sim.fitter.plotChain()
            
            discard = 14000
            chi2max = 'min+10'
            mode = "peak"
            sim.fitter.cornerPlot(discard = discard, mode = mode, chi2max = chi2max, fontsize = 25)
            
            # Plot results
            sim.getBestFit(mode = mode, chi2max = chi2max)
            sim.compute()
            sim.plotDataModel()
            sim.plotDataModel(save = True)
            
            dim = 256
            pix = 0.2
            plt.figure()
            im = sim.model.getImage(dim, pix, (lams[0] + lams[-1])/2)
            im = im/np.max(im)
            plt.imshow(im, norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.2, vmax=0.7), cmap = "jet", extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix])
            plt.xlabel("mas")
            plt.ylabel("mas")
            plt.savefig("image.png")
            
            # Plot orientation density
            ut.getOrientationDensity(sim.data, save = "oridens.png")
            
                    
            sim.save(final_dir, overwrite = True)
        except Exception as err:
            print(err)
            print_msg = "band = {}, model = {}, chrom = {}".format(band, modname, chrom)
            print("Failed " + star_name + " " + print_msg)
            if star_name in failed_stars:
                failed_stars[star_name].append(print_msg)
            else:
                failed_stars[star_name] = [print_msg]
        
        


import pprint

print("Failed stars :")
pprint.pprint(failed_stars)