
# -*- coding: utf-8 -*-
"""
Created on Thu May  6 18:41:31 2021

@author: Dorian
"""

import oitools.oimodel as oim
import oitools.fitter as fit
from oitools import oifitstools
import numpy as np
import os
from astropy.io import fits
import matplotlib.pyplot as plt
import matplotlib.colors as colors


#%%
"""
NOTES :
    - If you get the error "ValueError: Probability function returned NaN", then there is probably a 'NaN' value somewhere in the oifits file.
"""


# os.chdir("D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/LM15/FSCMa")
os.chdir("/home/ddemars/Documents/Stage_Nice/data/LM15/FSCMa")
    
    
files= ["2018-12-07T072811_HD45677_A0B2D0C1_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2018-12-09T060636_HD45677_K0B2D0J3_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2018-12-13T045840_HD45677_A0G1J2J3_IR-LM_LOW_Chop_cal_oifits_0.fits"]

files = os.listdir()




# lams = [2e-6,6e-6]
lams = [3e-6, 4e-6]

ois = []
for file in files:
    
    # oi["OI_T3"].data["T3PHIERR"] = np.dot(oi["OI_T3"].data["T3PHIERR"], 25)
    if file.endswith(".fits"):
        # oi = oifitstools.cutWavelengthRange(fits.open(file), lams)
        if not "simDat" in file and not "noChop" in file:
            oi = oifitstools.cutWavelengthRange(fits.open(file), lams)
            ois.append(oi)



sim = oim.oim_simulator(oifits = ois)

#%%




bckg = oim.oim_bckg(f=1)
pt = oim.oim_pt(f=1)
g=oim.oim_gauss(fwhm=1, f= 0.1)
g1=oim.oim_gauss(fwhm=1, f= 0.1)
g2=oim.oim_gauss(fwhm=2)
er=oim.oim_new_skw_ring(d=18, elong=1.5,pa=0, pa_skw = 0, skw=1)
cv=oim.oim_convolution(er, g2)

lor = oim.oim_lorentz(fwhm = 1)
cv_lor = oim.oim_convolution(er, lor)



bckg.params["x"].free = False
bckg.params["y"].free = False
er.params["x"].free = False
er.params["y"].free = False
g.params["x"].free = False
g.params["y"].free = False
g.params["f"].free = False
pt.params["f"].free = False
pt.params["x"].free = False
pt.params["y"].free = False
g1.params["x"].free = False
g1.params["y"].free = False


m1=oim.oim_model([bckg, g, cv])

m1.setDefaults()

g.params["fwhm"].max = 5
g2.params["fwhm"].max = 20

# g1.params["fwhm"].min = 5
# g1.params["f"].free = True
er.params["d"].max = 40
er.params["elong"].max = 5
er.params["pa"].min = 50


er=oim.oim_new_skw_ring(d=8, elong=1,pa=0, pa_skw = 0, skw=0)
g1=oim.oim_gauss2(fwhm=5, f= 0.1)
lor1 = oim.oim_lorentz(fwhm = 2, f = 0.1)
kernel = oim.kernel_lor_gauss([lor1,g1], rlim = er.params["d"])
cv = oim.oim_convolution(er, lor1)

m1=oim.oim_model([bckg, g1, cv])
# m1=oim.oim_model([kernel])
m1.setDefaults()

g1.params["x"].free = False
g1.params["y"].free = False
lor1.params["x"].free = False
lor1.params["y"].free = False
er.params["x"].free = False
er.params["y"].free = False


lor1.params["fwhm"].max = 15
er.params["d"].max = 40
er.params["elong"].max = 5
er.params["pa"].min = 50
g1.params["fwhm"].max = 10
g1.params["f"].free = False
g1.params["f"].value = 1


#%%

sim.setModel(m1)


nsteps = 20000
nwalkers = 32
fitted_data = ["VISAMP"]

sim.setFitter(nsteps = nsteps, nwalkers = nwalkers, fitted_data = fitted_data, flagOut = True)
# sim.updateFitter(flagOut = True)


# sim.fitter.debugSkipNanZeros()

sim.fitter.makeInit(random = True)

#%%



sim.runFit()
sim.fitter.plotChain()

#%%
# sim.fitter.resume(10000)
# sim.fitter.plotChain()

#%%
discard = 12000
chi2max = "min+10"
mode = 'peak'
sim.fitter.cornerPlot(discard = discard, mode = mode, chi2max = chi2max, fontsize = 25)


#%%



# sim.model = sim.model
# sim.fitter = sim.fitter
# mode = "peak"
sim.getBestFit(discard = discard, mode = mode, chi2max = chi2max)

#%%
sim.compute()

simdat = sim.simulatedData

        

sim.plotDataModel()


dim = 256
pix = 0.2

plt.figure()
im = sim.model.getImage(dim, pix, 3.5e-6)
im = im/np.max(im)
plt.imshow(im, norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.1, vmax=1), cmap = "jet", extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix])

plt.xlabel("mas")
plt.ylabel("mas")
plt.savefig("image.png", bbox_inches = "tight")
#%%



sim.save("./")

