# -*- coding: utf-8 -*-
"""
Created on Thu May  6 18:41:31 2021

@author: Dorian
"""

import oitools.oimodel as oim
import oitools.fitter as fit
from oitools import oifitstools
import numpy as np
import os
from astropy.io import fits
import matplotlib.pyplot as plt
import matplotlib.colors as colors

"""
NOTES :
    - If you get the error "ValueError: Probability function returned NaN", then there is probably a 'NaN' value somewhere in the oifits file.
"""



os.chdir("/home/ddemars/Documents/Stage_Nice/data/LM-15/CPD-572874")
    
    
files= ["2020-02-16T012108_CPD-572874_A0B2D0C1_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2020-02-21T001801_CPD-572874_K0G2D0J3_IR-LM_LOW_Chop_cal_oifits_0.fits",
        "2020-02-27T051029_CPD-572874_A0G1J2J3_IR-LM_LOW_Chop_cal_oifits_0.fits"]





lams = [3.1e-6,3.5e-6, 3.8e-6]

ois = [oifitstools.cutWavelengthRange(fits.open(file), (lams[0], lams[-1])) for file in files]


sim = oim.oim_simulator(oifits = ois)


sim.setFitter()

sim.fitted_data = ["VISAMP", "T3PHI"]



# Small gaussian for star + near environnement
g1 = oim.oim_egauss(fwhm = oim.oim_interp1d(lams,[1,2,3]),
                    f = oim.oim_interp1d(lams,[1,2,3]),
                    pa = 0, elong = 1)

g2 = oim.oim_gauss(fwhm = 2, f = 1)



m1 = oim.oim_model([g1, g2])





g1.params["x"].free=False
g1.params["y"].free=False

g1.params["pa"].min = 0
g1.params["pa"].max = 180
g1.params["elong"].min = 1
g1.params["elong"].max = 5




g2.params["f"].free = False
g2.params["x"].free=False
g2.params["y"].free=False
g2.params["fwhm"].min = 0
g2.params["fwhm"].max = 200


m1.getParameters()["fwhm_1_interp1"].free = True
m1.getParameters()["fwhm_1_interp2"].free = True
m1.getParameters()["fwhm_1_interp3"].free = True
# m1.getParameters()["fwhm_1_interp4"].free = True
m1.getParameters()["fwhm_1_interp1"].min = 0
m1.getParameters()["fwhm_1_interp2"].min = 0
m1.getParameters()["fwhm_1_interp3"].min = 0
# m1.getParameters()["fwhm_1_interp4"].min = 0
m1.getParameters()["fwhm_1_interp1"].max = 50
m1.getParameters()["fwhm_1_interp2"].max = 50
m1.getParameters()["fwhm_1_interp3"].max = 50
# m1.getParameters()["fwhm_1_interp4"].max = 50

m1.getParameters()["f_1_interp1"].free = True
m1.getParameters()["f_1_interp2"].free = True
m1.getParameters()["f_1_interp3"].free = True
# m1.getParameters()["f_1_interp4"].free = True
m1.getParameters()["f_1_interp1"].min = 0
m1.getParameters()["f_1_interp2"].min = 0
m1.getParameters()["f_1_interp3"].min = 0
# m1.getParameters()["f_1_interp4"].min = 0
m1.getParameters()["f_1_interp1"].max = 50
m1.getParameters()["f_1_interp2"].max = 50
m1.getParameters()["f_1_interp3"].max = 50
# m1.getParameters()["f_1_interp4"].max = 50





sim.setModel(m1)
sim.fitter.nsteps = 5000
sim.fitter.nwalkers = 32


sim.updateFitter(flagOut = True)

sim.fitter.makeInit(random = True)

#%%



sim.fitter.run()
sim.fitter.plotChain()

#%%
sim.fitter.resume(15000)
sim.fitter.plotChain()

#%%
discard = 1500
sim.fitter.cornerPlot(discard = discard)


#%%




sim.getBestFit(mode = 'best')
sim.compute()

# simdat = sim.simulatedData


sim.plotDataModel()


dim = 128
pix = 0.2

plt.figure()
im = sim.model.getImage(dim, pix, 3.3e-6)
im = im/np.max(im)
plt.imshow(im, norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.2, vmax=0.8), cmap = "plasma", extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix])

plt.xlabel("mas")
plt.ylabel("mas")

#%%



sim.save("./")

