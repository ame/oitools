#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 14:43:41 2021

@author: ddemars
"""

import os
import oitools.oifitstools as ut
import oitools.oimodel as oim
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import sys

import matplotlib
matplotlib.use('Agg')



def make_dirs(chrom, results_dir, fitted_data):
    # Save results
    if chrom == True:
        chromdir = "chrom/"
    else:
        chromdir = "achrom/"
    
    
    final_dir = results_dir + "/" + band + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    
    final_dir +=  star_name + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    final_dir += modname.split(".py")[0]
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    final_dir += "/" + chromdir
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    fdat_dir_name = ""
    for i in range(len(sim.fitted_data)):
        fdat_dir_name += sim.fitted_data[i]
        if i != len(sim.fitted_data)-1:
            fdat_dir_name += " - "
    
    final_dir += "/" + fdat_dir_name + "/"
    
    try:
        os.mkdir(final_dir)
    except:
        pass
    
    return final_dir

execdir = os.getcwd()

# data_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/data/"
# models_dir = "D:/User Documents/Documents/Projects/python modules/oitools/data_fitting/models/"
# results_dir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage_Nice/results/"


# data_dir = "/home/ddemars/Documents/Stage_Nice/data/"
# models_dir = "/home/ddemars/Documents/python_routines/oitools/data_fitting/models/"
# results_dir = "/home/ddemars/Documents/Stage_Nice/results/"

data_dir = "/home/ddemars/data/"
models_dir = "/home/ddemars/python_modules/oitools/data_fitting/models/"
results_dir = "/home/ddemars/fits/results/"

sys.path.append(models_dir)

bands_dir = {"L": "LM15/",
             "M": "LM15/",
             "N": "N21/"}





models = ["egauss.py",
          "egauss_bckg.py",
          "egauss_punct.py",
          "egauss_punct_bckg.py"]

# models = ["egauss_punct_bckg.py"]



                                                                
band = "L"
chrom = True
points = 3
fitted_data = ["VISAMP"]

# stars = ["Hen3-938"]
stars = "all"

failed_stars  = {}


os.chdir(data_dir + "/" + bands_dir[band])

if stars == "all":
    stars = os.listdir()
print(stars)    
for star_name in stars:
    os.chdir(data_dir + "/" + bands_dir[band])
    os.chdir(star_name)
    files = os.listdir()
    
    
    # Filtering for only choped files
    thefiles = []
    for file in files:
        if "noChop" in file or "simDat" in file:
            continue
        else:
            thefiles.append(file)
    files = thefiles
    
    # Cutting specified band
    ois = []
    for file in files:
        lams = [ut.getBandsLimits()[band][0], ut.getBandsLimits()[band][1]]
        if file.endswith(".fits"):
            oi = ut.cutWavelengthRange(fits.open(file), lams)
            ois.append(oi)
    
    if len(ois) == 0:
        failed_stars[star_name] = "no files"
        continue
    
    # Checking for missing bands
    failed_files = []
    idxs = []
    for i in range(len(ois)):
        if np.shape(ois[i]["OI_VIS"].data["VISAMP"]) == (0,):
            failed_files.append(ois[i].filename())
        else:
            idxs.append(i)
    if len(failed_files) == len(ois):
        failed_stars[star_name] = ["Missing band in files for band {}, star {}".format(band, star_name), failed_files]
        print("Missing band in files for band {}, star {}".format(band, star_name))
        continue
    else:
        print("Only some files contain the {} band. It will only use these files".format(band))
        ois = [ois[i] for i in idxs]

    
    cwd = os.getcwd()
    for modname in models:
        os.chdir(cwd)
        print("")
        print("============================")
        print("---------------------------")
        print("Working in '{}'".format(star_name))
        print("band '{}' = ({}, {}) um".format(band, lams[0]*1e6, lams[1]*1e6))
        print("model = {}".format(modname))
        print("", flush = True)
        
        try:
            # Making model
            os.chdir(models_dir)
            if modname.endswith(".py"):
                # print(os.getcwd())
                exec("from {} import getModel".format(modname.split(".py")[0]))
                model = getModel(chrom = chrom, band = band, points = points) # error because 'getModel' is imported using 'exec("str_command")'
            else:
                continue
            
            os.chdir(cwd)
            sim = oim.oim_simulator(oifits = ois)
            sim.setModel(model)
            
            
            
            
            
            # Preparing fitter
            sim.setFitter(nsteps = 5000, nwalkers = 32, fitted_data = fitted_data, flagOut = True)
            
            # sim.plotData(pdat = ["VISAMP"], title = star_name)
            
            
            final_dir = make_dirs(chrom, results_dir, sim.fitted_data)
            os.chdir(final_dir)
            
            # Running
            sim.runFit()
            
            # Plots
            sim.fitter.plotChain(save = "chain.png")
            
            discard = 2000
            chi2max = None
            sim.fitter.cornerPlot(discard = discard, chi2max = chi2max, fontsize = 25, save = "corner_plot.png")
            
            # Plot results
            sim.getBestFit(mode = 'best', chi2max = chi2max)
            sim.compute()
            sim.plotDataModel(save = True, title = star_name)
            
            dim = 256
            pix = 0.2
            # plt.figure()
            # im = sim.model.getImage(dim, pix, (lams[0] + lams[-1])/2)
            # im = im/np.max(im)
            # plt.imshow(im, norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.2, vmax=0.7), cmap = "jet", extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix])
            # plt.xlabel("mas")
            # plt.ylabel("mas")
            
            
            
            
                    
            sim.save(final_dir, overwrite = True)
        except Exception as err:
            os.chdir(execdir)
            print(err)
            print_msg = "band = {}, model = {}, chrom = {}".format(band, modname, chrom)
            print_msg = "Failed " + star_name + " " + print_msg
            print(print_msg)
            if star_name in failed_stars:
                failed_stars[star_name].append(print_msg)
            else:
                failed_stars[star_name] = [print_msg]
                
            with open("{}.log".format(band), 'w') as f:
                f.write("\n")
                f.write("=================")
                f.write(err)
                f.write("\n")
                f.write(print_msg)
                f.write("\n")

os.chdir(execdir)
with open("{}.log".format(band), 'w') as f:
    f.write("\n")
    f.write(failed_stars.__str__())
                
        


import pprint

print("Failed stars :")
pprint.pprint(failed_stars)