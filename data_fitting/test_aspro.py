# -*- coding: utf-8 -*-
"""
Created on Thu May  6 18:41:31 2021

@author: Dorian
"""

import oitools.oimodel as oim
import oitools.fitter as fit
from oitools import oifitstools
import numpy as np
import os
from astropy.io import fits
import matplotlib.pyplot as plt
import matplotlib.colors as colors

"""
NOTES :
    - If you get the error "ValueError: Probability function returned NaN", then there is probably a 'NaN' value somewhere in the oifits file.
"""



os.chdir("D:/User Documents/Documents/Projects/python modules/oitools/data_fitting")
    
    
files= ["aspro_test_no_errorbars.fits"]





lams = [3.0e-6,3.4e-6, 3.8e-6]

ois = [oifitstools.cutWavelengthRange(fits.open(file), (lams[0], lams[-1])) for file in files]

ois = [fits.open(file) for file in files]

sim = oim.oim_simulator(oifits = ois)




sim.setFitter()

sim.fitted_data = ["T3PHI"]



# Two disks
d1 = oim.oim_ud(d = 2, f = 1, x=12, y=0)
d2 = oim.oim_ud(d = 2, f = 1, x=0, y=0)




m1 = oim.oim_model([d1, d2])


# d1.params["x"].free = False
# d1.params["y"].free = False
d2.params["x"].free = False
d2.params["y"].free = False

d1.params["x"].min = -20
d1.params["x"].max = 20
d1.params["y"].min = -20
d1.params["y"].max = 20
d1.params["f"].min = 0
d1.params["f"].max = 3



d1.params["d"].min = 4
d1.params["d"].max = 10


d2.params["f"].free = False
# d2.params["x"].min = -20
# d2.params["x"].max = 20
# d2.params["y"].min = -20
# d2.params["y"].max = 20
d2.params["d"].min = 0
d2.params["d"].max = 4



sim.setModel(m1)
sim.fitter.nsteps = 5000
sim.fitter.nwalkers = 32


sim.updateFitter(flagOut = False)

sim.fitter.makeInit(random = True)

#%%


# sim.fitter.debugSkipNan()



sim.fitter.run()
sim.fitter.plotChain()

#%%
sim.fitter.resume(10000)
sim.fitter.plotChain()

#%%
discard = 3000
sim.fitter.cornerPlot(discard = discard, chi2max = "min*10")


#%%




sim.getBestFit(mode = 'best')

sim.compute()

# simdat = sim.simulatedData


sim.plotDataModel()


dim = 128
pix = 0.5

plt.figure()
im = np.flip(sim.model.getImage(dim, pix, 3.4e-6), axis = 1)
im = im/np.max(im)
plt.imshow(im, norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.2, vmax=0.8), cmap = "plasma", extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix], origin = "lower")

plt.xlabel("mas")
plt.ylabel("mas")

#%%



# sim.save("./")

