# oitools


Package for optical interferometry (in development).

## oimodel.py   
 
A modular fitting tool using simple geometric components

![boo](./images/oimodel_test_chromatic.png)

It contains:
- oim_interp1d    : class for chromatic value of model parameter (see example in test_oimodel_chromatic)
- oim_param       : class for all model parameters
- oim_component   : based class for components of model
- oim_pt          : component Point source
- oim_bckg        : component Background
- oim_ud          : component Uniform Disk
- oim_ellipse     : component Uniform Ellipse
- oim_gauss       : component Gaussian Disk
- oim_egauss      : component Gaussian Dllipse
- oim_ring        : component Ring 
- oim_ring2       : component Ring with width : din dout
- oim_ering       : component Elliptic ring 
- oim_ering2      : component Elliptic ring with width : din dout
- oim_eskewring   : Elliptic Skewed ring 
- oim_convolution : component created wiwth convolution of two components    
- oim_model       : Class for model (i.e. combination of components)    
- oim_simulator   : Class for simulator = data + model 

# cubetools.py  
Oifits Simulator  from image-cube models 
- oifitsSimulator : Class for simulator = data + model
- wavelength      : Class for wavelengths array (regular)
- showCube        : Class for interactive display of fits cube data
- computeWavelengthFromCube : function to get walvength array for an image cube

# oifitstools.py
Functions for manipulating oifits files 
- copy
- flagWavelengthRange
- cutWavelengthRange
- normalizeV2
- normalizeFlux
- computeDifferentialError
- shiftWavelength
- uvplot
- create_oi_array
- create_oi_target_from_simbad
- create_oi_wavelength
- create_oi_vis2
- smooth
- binWavelength
- baselinename
- spectralSmoothing
- oiPlot
- getBaselineLengthAndPA
- getSpaFreq


## testfiles 
- test_oimodel.py             : Simple oimodels examples
- test_oimodel_chromatic.py   : oimodels examples with some chromatic parameters
- test_oimodel_oifits.py      : Example of simulated oifits data using oimodel
- test_oimodel_speed.py       : Speed test of oimodel 

