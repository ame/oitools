# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:23:36 2021

@author: Ame
"""
from oitools.oimodel import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
from datetime import datetime
mas2rad=np.pi/180/3600./1000.
from tqdm import tqdm
from astropy.io import fits
import os

dataset="MATISSE"
#dataset="NPOI"

if dataset=="NPOI":
    #Some example data from NPOI
    dirdata="D:\\Travail\\Etoiles\\Beta CMi\\DATA\\NPOI\\"
    fname=[os.path.join(dirdata,"beta-CMI-best-24.fits")]
    wlRef=[0.5e-6,1e-6]


if dataset=="MATISSE":
    dirdata="D:\\Travail\\Etoiles\\BpeMATISSE\\data\\LM-15\\HD50138\\new\\"
    fname=[os.path.join(dirdata,fi) for fi in os.listdir(dirdata) if ("_Chop" in fi and ".fits" in fi)]
    wlRef=[3.0e-6,5.0e-6]



#Some components for the models
ud=oim_ud(d=0.7)
ud2=oim_ud(x=5,y=15,d=1.5,f=0.1)
g0=oim_gauss(fwhm=4)
g1=oim_gauss(fwhm=oim_interp1d(wlRef,[1,2]))
g2=oim_gauss(fwhm=oim_interp1d(wlRef,[1,2]),f=oim_interp1d(wlRef,[1,4]))
er=oim_eskewring(d=oim_interp1d(wlRef,[10,12]),elong=2,pa=0,skw=0.9)
eg=oim_egauss(fwhm=1,elong=2,pa=0,f=oim_interp1d(wlRef,[1,0.01]))
g3=oim_gauss(fwhm=oim_interp1d(wlRef,[1,2]))
cv=oim_convolution(er, g3)

#6 models to test with increasing complexity
m0=oim_model([g0])
m1=oim_model([ud])
m2=oim_model([g1])
m3=oim_model([ud,ud2])
m4=oim_model([ud,g2])
m5=oim_model([cv,eg])
models=[m0,m1,m2,m3,m4,m5]

names=[ "1 Component  : GD (0 chromatic parameters)",
       "1 Component  : UD (0 chromatic parameters)",
       "1 Component  : GD (1 chromatic parameters)",       
       "2 Components : Binary UDs (0 chromatic parameters)",
       "2 Components : GD + UD (2 chromatic parameters)",       
       "3 Components : SkRing*GD + EG (3 chromatic parameters)"]
colors= plt.rcParams['axes.prop_cycle'].by_key()['color']


nsim=10

# the main loop on the models
for imodel,(name,m) in  enumerate(zip(names,models)):
    print("******************************************************************")
    print(name)
    print("******************************************************************")
    sim=oim_simulator(fname,m)
    
    if dataset=="MATISSE":
        for fit in sim.fit:
            fit.VISPHI=False 
            fit.T3PHI=False             
    sim.compute()

    start=datetime.now()    
    for i in range(nsim):
        sim.compute()
    dt1 = (datetime.now()-start).total_seconds()*1000/nsim
    
    start=datetime.now() 
    for i in range(nsim):
        sim.computeObservables()
    dt2 = (datetime.now()-start).total_seconds()*1000/nsim
   
    start=datetime.now() 
    for i in range(nsim):
        sim.computeChi2()
    dt3 = (datetime.now()-start).total_seconds()*1000/nsim    

    start=datetime.now()
    for i in range(nsim):
        sim.compute()
        sim.computeChi2()
    dt4 = (datetime.now()-start).total_seconds()*1000/nsim
    
    print("computeComplexVis : {:.1f}ms".format(dt1-dt2))    
    print("computeObservables : {:.1f}ms".format(dt2))    
    print("computeChi2 : {:.1f}ms".format(dt3))  
    print("------------------------------------------------------------------")
    print("Total : {:.1f}ms".format(dt4))      

    
    
    
    
    
    
    
    
    
    
    

    
