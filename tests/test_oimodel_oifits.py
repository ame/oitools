# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:38:15 2021

@author: Ame
"""

from oitools.oimodel import *
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import os
from oifitstools import getSpaFreq,cutWavelengthRange
import matplotlib.colors as mcolors

#better looking error bars
def errorFill(axe,X,Y,dY,color="k",alpha=1,zorder=0):
    X=np.concatenate([X,np.flip(X)])
    Y=np.concatenate([Y-dY,np.flip(Y+dY)])
    axe.fill(X,Y,zorder=zorder,alpha=alpha,color=color)




dir0="D:\\Travail\\Etoiles\\BpeMATISSE\\data\\LM-15\\HD50138\\new\\"
#dir0="D:\\Travail\\Etoiles\\BpeMATISSE\\data\\LM-15\\HD50138\\ringpunct\\"
wlRange=[3e-6,5e-6]
#wlRange=[3.4e-6,3.5e-6] # small band 
wlRange=[3.0e-6,3.8e-6]  # L only
files=[os.path.join(dir0,fi) for fi in os.listdir(dir0)]# if "-LM_LOW_Chop" in fi]
data=[cutWavelengthRange(fits.open(fi),wlRange) for fi in files]
"""
for datai in data:
    print(np.median(datai["OI_T3"].data["T3PHIERR"]))
    datai["OI_T3"].data["T3PHIERR"]*=100
    print(np.median(datai["OI_T3"].data["T3PHIERR"]))
"""
    
PA=71
elong=2
fsmall=0.11


#The skewed ring with a gaussian profile)
g=oim_gauss(fwhm=1)
er=oim_eskewring(d=20,elong=elong,pa=PA,skw=0.5,f=0.8)
r=oim_ering(d=20,elong=elong,pa=PA)
cv=oim_convolution(er, g)
#cv.params["f"].value=1
pt=oim_pt(f=0.2)
#A small ellongated gaussian representing the gas disk (+ star contribution)
eg=oim_egauss(fwhm=1.0,elong=elong,pa=PA,f=0)

#the model
m=oim_model([er,pt])

#The simulator with the dataset and the model
sim=oim_simulator(data,m)

#computing the simulated data that will then be accessible here => sim.simulatedData
sim.compute()
#Computing the chi2 on VIS2 & T3PHI 
sim.computeChi2()
print("Chi2r={0}".format(sim.chi2r))



alpha=0.3
cols=plt.rcParams['axes.prop_cycle'].by_key()['color']
ncols=len(cols)

fig,ax=plt.subplots(1,3,figsize=(15,5))
fig.subplots_adjust(wspace=0.3)   
#PLot the visibility
icol=0
for idata in range(len(data)):   
    
    spfreq=getSpaFreq(data[idata],unit="cycles/mas")
    for iB in range(6):
        v=data[idata]["OI_VIS"].data["VISAMP"][iB,:]
        verr=data[idata]["OI_VIS"].data["VISAMPERR"][iB,:]
        errorFill(ax[0],spfreq[iB,:],v,verr,alpha=alpha,color=cols[icol%ncols])
        ax[0].plot(spfreq[iB,:],v,color="k",ls=":")    
        vsim=sim.simulatedData[idata]["OI_VIS"].data["VISAMP"][iB,:]
        ax[0].plot(spfreq[iB,:],vsim,color=cols[icol%ncols])
        icol+=1        
ax[0].set_ylim(0,1)
ax[0].set_xlabel("B/$\\lambda$ (cycles/mas)")
ax[0].set_ylabel("Visibility")

#PLot the closure phase
icol=0
for idata in range(len(data)):      
    # For the closure phase this return the larger spatial freq of the three baselines 
    spfreq=getSpaFreq(data[idata],arr="OI_T3",unit="cycles/mas")
    print()
    for iB in range(4):
        cp=data[idata]["OI_T3"].data["T3PHI"][iB,:]
        cperr=data[idata]["OI_T3"].data["T3PHIERR"][iB,:]
        errorFill(ax[1],spfreq[iB,:],cp,cperr,alpha=alpha,color=cols[icol%ncols])
        ax[1].plot(spfreq[iB,:],cp,color="k",ls=":")        
        cpsim=sim.simulatedData[idata]["OI_T3"].data["T3PHI"][iB,:]
        ax[1].plot(spfreq[iB,:],cpsim,color=cols[icol%ncols])
        icol+=1        
ax[1].set_ylim(-180,180)
ax[1].set_xlabel("B/$\\lambda$ (cycles/mas)")
ax[1].set_ylabel("Cl:osure phase ($^o$)")
# Create an plot an image of the model
dim=256   #Number of pixel for the image
pix=0.1  #size of the pixel in the image in mas
im=m.getImage(dim,pix)
mp=ax[2].imshow(im,extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix],norm=mcolors.PowerNorm(0.2, vmin=0, vmax=np.max(im)))
ax[2].set_xlabel("$\\alpha$(mas)")
ax[2].set_ylabel("$\\delta$(mas)")
plt.colorbar(mp,label="Flux (arbitrary unit)")



