# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:23:36 2021

@author: Ame
"""
from oitools.oimodel import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

mas2rad=np.pi/180/3600./1000.


#Some components with or without chromaticity
ud=oim_ud(d=0.7,f=1)
ud2=oim_ud(x=0,y=5,d=1.5,f=oim_interp1d([3e-6,4e-6],[1,0.1]))
ud3=oim_ud(x=-5,y=2,d=0.5,f=oim_interp1d([3e-6,4e-6],[0.1,1]))
g=oim_gauss(fwhm=oim_interp1d([3e-6,3.5e-6,4e-6],[1,4,4]),
            f=oim_interp1d([3e-6,4e-6],[1,4]))
er=oim_eskewring(d=oim_interp1d([3e-6,4e-6],[10,10]),elong=2,pa=0,skw=0.9)
eg=oim_egauss(fwhm=0.1,elong=2,pa=0,f=oim_interp1d([3e-6,4e-6],[1,1]))
g3=oim_gauss(fwhm=oim_interp1d([3e-6,4e-6],[3,3]))
cv=oim_convolution(er, g3)

#Building models
m1=oim_model([g])
m2=oim_model([ud,g])
m3=oim_model([ud,ud2,ud3])
m4=oim_model([cv,eg])

#linking some parameters, actually replacing them
cv.params["elong"]=eg.params["elong"]
cv.params["pa"]=eg.params["pa"]

#fixing others
eg.params["x"].free=False
eg.params["y"].free=False
cv.params["x"].free=False
cv.params["y"].free=False
eg.params["pa"].value=0

nB=50 #number of baselines 
nwl=100 #number of walvengths

#Create some spatial frequencies

wl=np.linspace(3e-6,4e-6,num=nwl)
B=np.linspace(1,400,num=nB)
Bs=np.tile(B,(nwl,1)).flatten()
wls=np.transpose(np.tile(wl,(nB,1))).flatten()
spf0=Bs/wls

spf1=[spf0,spf0*0]  # a set of baseline from 0.01m to 400m aligned East-West
spf2=[spf0*0,spf0]  # a set of baseline from 0.01m to 400m aligned North-South

#Plots of the simulated visiblities and images for all models
models=[m1,m2,m3,m4]
model_names=["GD (chrom. fwhm)",
             "UD + GD (chrom. fwhm and flx)",
             "Triple star UDs (chrom.  flx)",             
             " SkRing*GD + GE"]
fig,ax=plt.subplots(4,4,figsize=(8,8))

for i,m in enumerate(models):
    #Compute visibilities for the models and spatial frequencies
    v1=np.abs(m.getComplexVisibility(spf1,wls)).reshape(len(wl),len(B))
    v2=np.abs(m.getComplexVisibility(spf2,wls)).reshape(len(wl),len(B))
    for iwl,wli in enumerate(wl):
        ax[2,i].plot(B/wli*mas2rad,v1[iwl,:],color=plt.cm.plasma(iwl/nwl),alpha=0.5)
        ax[3,i].plot(B/wli*mas2rad,v2[iwl,:],color=plt.cm.plasma(iwl/nwl),alpha=0.5)        
    ax[2,i].scatter(spf0*mas2rad,v1,c=wls*1e6,s=0.1,cmap="plasma")
    sc=ax[3,i].scatter(spf0*mas2rad,v2,c=wls*1e6,s=0.1,cmap="plasma")
    ax[3,i].set_xlabel("B/$\\lambda$ (cycles/mas)")
    ax[2,i].margins(0,0)
    ax[2,i].set_ylim(0,1)
    ax[3,i].margins(0,0)
    ax[3,i].set_ylim(0,1)
    #ax[2,i].set_title(model_names[i],fontsize=8)
    if i!=0:
        ax[2,i].get_yaxis().set_visible(False)
        ax[3,i].get_yaxis().set_visible(False) 
ax[2,0].set_ylabel("Vis. (East-West)")         
ax[3,0].set_ylabel("Vis. (North-South)") 

# Create direct images of the model

dim=129   #Number of pixels for the image
pix=0.15  #size of the pixel in the image in mas
wls=[3e-6,4e-6] #wavelengths array for the images

for i,m in enumerate(models):    
    im=m.getImage(dim,pix,wls)
    for iwl,wli in enumerate(wls):     
        imi=im[iwl,:,:]
        imi/=np.max(imi)
        imshow=ax[iwl,i].imshow(imi,extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix],
                                norm=colors.TwoSlopeNorm(vmin=0., vcenter=1e-1, vmax=1)
                                ,cmap="plasma", interpolation=None)
        ax[iwl,i].text(0,dim*pix/2,"\n{:.1f}$\\mu$m".format(wli*1e6),
                       color="w",va="top",ha="center")
        if i!=0:
            ax[iwl,i].get_yaxis().set_visible(False)
        #if iwl!=1:
        #    ax[iwl,i].get_xaxis().set_visible(False)
        #if iwl==1:
        ax[iwl,i].set_xlabel("$\\alpha$(mas)")
           
        if i==0:
            ax[iwl,i].set_ylabel("$\\delta$(mas)")
            
fig.tight_layout()            
fig.colorbar(imshow, ax=ax[:2,:].ravel().tolist(),label="Normalized Intensity")
fig.colorbar(sc, ax=ax[2:,:].ravel().tolist(),label="$\\lambda$ ($\\mu$m)")

