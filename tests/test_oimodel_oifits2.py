# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:38:15 2021

@author: Ame
"""

from oitools.oimodel import *
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import os
from oifitstools import getSpaFreq,cutWavelengthRange
import matplotlib.colors as colors

#better looking error bars
def errorFill(axe,X,Y,dY,color="k",alpha=1,zorder=0):
    X=np.concatenate([X,np.flip(X)])
    Y=np.concatenate([Y-dY,np.flip(Y+dY)])
    axe.fill(X,Y,zorder=zorder,alpha=alpha,color=color)




dir0="D:\\Travail\\Etoiles\\BpeMATISSE\\data\\LM-15\\HD50138\\new\\"
#dir0="D:\\Travail\\Etoiles\\BpeMATISSE\\data\\LM-15\\HD50138\\ringpunct\\"
wlRange=[3e-6,5e-6]
#wlRange=[3.4e-6,3.5e-6] # small band 
wlRange=[3e-6,4e-6]  # L only
files=[os.path.join(dir0,fi) for fi in os.listdir(dir0) if "-LM_LOW_Chop" in fi]
data=[cutWavelengthRange(fits.open(fi),wlRange) for fi in files]

for datai in data:
    print(np.median(datai["OI_T3"].data["T3PHIERR"]))
    datai["OI_T3"].data["T3PHIERR"]*=100
    print(np.median(datai["OI_T3"].data["T3PHIERR"]))

    
PA=71
elong=2
fsmall=0.11


#The skewed ring with a gaussian profile)
eg_small=oim_egauss(fwhm=4,elong=2,pa=70,f=1)
eg=oim_egauss(fwhm=oim_interp1d([3e-6,4e-6],[20,30]),elong=2,pa=70,f=1.5)
er=oim_eskewring(d=oim_interp1d([3e-6,4e-6],[20,30]),elong=2,pa=70,skw=1,pa2=70)
gconv=oim_gauss(fwhm=oim_interp1d([3e-6,4e-6],[2,4]))
cv=oim_convolution(er, gconv)

#two gaussians
eg=oim_egauss(fwhm=oim_interp1d([3e-6,5e-6],[22,33.5]),f=oim_interp1d([3e-6,5e-6],[0.83,3.4]),pa=71,elong=2.4)
g=oim_gauss(fwhm=4,f=1)

#the model
m=oim_model([eg_small,cv])
m=oim_model([eg,g])


#The simulator with the dataset and the model
sim=oim_simulator(data,m)
for fit in sim.fit:
    fit.VISPHI=0
    fit.T3PHI=0
#computing the simulated data that will then be accessible here => sim.simulatedData
sim.compute()
#Computing the chi2 on VIS2 & T3PHI 
sim.computeChi2()
print("Chi2r={0}".format(sim.chi2r))

# Create images
dim=128   #Number of pixel for the image
pix=0.5  #size of the pixel in the image in mas
wlimages=[3e-6,4e-6]
im=m.getImage(dim,pix,wlimages)


alpha=0.3
cols=plt.rcParams['axes.prop_cycle'].by_key()['color']
ncols=len(cols)

#%%
fig,ax=plt.subplots(2,2,figsize=(12,12))
fig.subplots_adjust(wspace=0.3)   
#PLot the visibility
icol=0
for idata in range(len(data)):   
    
    spfreq=getSpaFreq(data[idata],unit="cycles/mas")
    for iB in range(6):
        v=data[idata]["OI_VIS"].data["VISAMP"][iB,:]
        verr=data[idata]["OI_VIS"].data["VISAMPERR"][iB,:]
        errorFill(ax[0,0],spfreq[iB,:],v,verr,alpha=alpha,color=cols[icol%ncols])
        ax[0,0].plot(spfreq[iB,:],v,color="k",ls=":")    
        vsim=sim.simulatedData[idata]["OI_VIS"].data["VISAMP"][iB,:]
        ax[0,0].plot(spfreq[iB,:],vsim,color=cols[icol%ncols])
        icol+=1        
ax[0,0].set_ylim(0,1)
ax[0,0].set_xlabel("B/$\\lambda$ (cycles/mas)")
ax[0,0].set_ylabel("Visibility")

#PLot the closure phase
icol=0
for idata in range(len(data)):      
    # For the closure phase this return the larger spatial freq of the three baselines 
    spfreq=getSpaFreq(data[idata],arr="OI_T3",unit="cycles/mas")
    print()
    for iB in range(4):
        cp=data[idata]["OI_T3"].data["T3PHI"][iB,:]
        cperr=data[idata]["OI_T3"].data["T3PHIERR"][iB,:]
        errorFill(ax[1,0],spfreq[iB,:],cp,cperr,alpha=alpha,color=cols[icol%ncols])
        ax[1,0].plot(spfreq[iB,:],cp,color="k",ls=":")        
        cpsim=sim.simulatedData[idata]["OI_T3"].data["T3PHI"][iB,:]
        ax[1,0].plot(spfreq[iB,:],cpsim,color=cols[icol%ncols])
        icol+=1        
ax[1,0].set_ylim(-180,180)
ax[1,0].set_xlabel("B/$\\lambda$ (cycles/mas)")
ax[1,0].set_ylabel("Cl:osure phase ($^o$)")


# plot  images of the model
for i in range(2):
    imi=im[i,:,:]/np.max(im[i,:,:])
    mp=ax[i,1].imshow(imi,extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix],norm=colors.TwoSlopeNorm(vmin=0., vcenter=1e-1, vmax=1))
    ax[i,1].set_xlabel("$\\alpha$(mas)")
    ax[i,1].set_ylabel("$\\delta$(mas)")
    ax[i,1].text(0,dim/2*pix,"\n{:.1f}$\\mu$m".format(wlimages[i]*1e6),color="w",va="top",fontsize=16,ha="center")
plt.colorbar(mp,ax=ax[:,1].ravel().tolist(),label="Flux (arbitrary unit)")



