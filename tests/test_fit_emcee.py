#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 30 16:20:37 2021

@author: ddemars
"""




from oitools import oimodel as oim
from oitools import __git_dir__
from oitools import oifitstools as oiut
from astropy.io import fits
import os

os.chdir(__git_dir__ + "/tests/data")


# Used files contain L and M band.
# We're gonna work on the L band only

band = "L"


#List files
files = os.listdir()

# Open the files and cut the L band
ois = []
for file in files:
    lams = [oiut.getBandsLimits()[band][0], oiut.getBandsLimits()[band][1]] # Get L band limits
    if file.endswith(".fits") and not 'simDat' in files:
        oi = oiut.cutWavelengthRange(fits.open(file), lams) # Cut the L band
        ois.append(oi)


# Working in L band

# Make the simulator
sim = oim.oim_simulator(ois)


# Make an analytical model, containing:
#   - A point source
#   - A background
#   - An elongated gaussian disk
p = oim.oim_pt(f = 1)
bckg = oim.oim_bckg(f = 1)
eg = oim.oim_egauss(fwhm = 1, f = 1, pa = 0, elong = 1)


# The composite model itself, made from the 3 components
model = oim.oim_model([p, bckg, eg])

# Setting the min and max values for each parameter to their default value
model.setDefaults()

# We're now telling the model that some parameters should not be free
p.params["f"].free = False
p.params["x"].free = False
p.params["y"].free = False
eg.params["x"].free = False
eg.params["y"].free = False
bckg.params["x"].free = False
bckg.params["y"].free = False


# Because the fitter finds a local minimum at -85°
# eg.params["pa"].min = -70


# Let's tell the simulator to use this model
sim.setModel(model)

# Now we want to fit the data
# We'll use the default EMCEE fitter
# Since the model is axi-symetric, we can only use 'VISAMP' for the fit
# We're setting EMCEE with 20000 steps, and 32 walkers
sim.setFitter("default", fitted_data = "VISAMP", nsteps = 20000, nwalkers = 32)


# We can now run EMCEE

sim.runFit()

#%%

# Let's plot the chain

sim.fitter.plotChain()


mode = 'peak'

# Let's plot the beautiful cornerplot, and ignore the first 5000 steps as burn-in
sim.fitter.cornerPlot(discard = 5000, mode =  mode)


# Let's get te best fit and plot the data on top of it
sim.getBestFit(mode =  mode)
sim.compute()           # This computes simulated data from the fitted model
sim.plotDataModel()