# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:23:36 2021

@author: Ame
"""

from oitools.oimodel import *
import matplotlib.pyplot as plt
import numpy as np

mas2rad=np.pi/180/3600./1000.



#Some components
ud=oim_ud(d=3.3,f=0.2)
g=oim_gauss(fwhm=2,f=0.5)
el=oim_ellipse(d=5,f=0.8,elong=2,pa=0)
er=oim_eskewring(d=10,elong=2,pa=0,skw=0.5)
g2=oim_egauss(fwhm=1.0,elong=2,pa=0,f=0.1)
cv=oim_convolution(er, g)
r=oim_ring(d=2.3)

#Building models
m1=oim_model([ud])
m2=oim_model([g])
m3=oim_model([r])
m4=oim_model([cv,g2])




#Create some spatial frequencies
lam=2.1e-6
B=np.linspace(0.01,400,num=1000)

spfx=B/lam
spfy=spfx*0
spf1=[spfx,spfy]  # spf1 are a set of baseline from 0.01m to 400m aligned East-West
spf2=[spfy,spfx]  # spf2 are a set of baseline from 0.01m to 400m aligned North-South

#Plots of the simulated visiblities
models=[m1,m2,m3,m4]
plt.figure()
cols= plt.rcParams['axes.prop_cycle'].by_key()['color']
for i,m in enumerate(models):
    #Compute visibilities for the models and spatial frequencies
    v1=np.abs(m.getComplexVisibility(spf1))
    v2=np.abs(m.getComplexVisibility(spf2))
    plt.plot(B/lam*mas2rad,v1,color=cols[i])
    plt.plot(B/lam*mas2rad,v2,color=cols[i],ls=":")
plt.xlabel("B$\\lambda$ (cycle/mas)")
plt.ylabel("Visibility")  
plt.margins(0,0)

#%%
# Create direct images of the model
dim=128   #Number of pixel for the image
pix=0.1  #size of the pixel in the image in mas
fig,ax=plt.subplots(1,len(models),figsize=(15,5))
for i,m in enumerate(models):    
    im=m.getImage(dim,pix)
    ax[i].imshow(im,extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix])
    ax[i].set_xlabel("$\\alpha$(mas)")
    ax[i].set_ylabel("$\\delta$(mas)")