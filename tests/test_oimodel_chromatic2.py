# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:23:36 2021

@author: Ame
"""
from oitools.oimodel import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

mas2rad=np.pi/180/3600./1000.


#Some components with or without chromaticity
ud=oim_ud(d=0.7,f=1)
ud2=oim_ud(x=0,y=2,d=1.5,f=oim_interp1d([3e-6,4e-6],[1,0.3]))
ud3=oim_ud(x=-2,y=1,d=0.5,f=oim_interp1d([3e-6,4e-6],[1,0.1]))
g=oim_gauss(fwhm=oim_interp1d([3e-6,3.5e-6,4e-6],[1,4,4]),f=oim_interp1d([3e-6,4e-6],[1,4]))
er=oim_eskewring(d=oim_interp1d([3e-6,4e-6],[10,20]),elong=2,pa=30+90,skw=1,f=oim_interp1d([3e-6,4e-6],[5,30]),pa2=30+90)
eg=oim_egauss(fwhm=3,elong=3,pa=30,f=oim_interp1d([3e-6,4e-6],[1,0.1]))
g3=oim_gauss(fwhm=oim_interp1d([3e-6,4e-6],[1,4]))
cv=oim_convolution(er, g3)

#Building models
m1=oim_model([g])
m2=oim_model([ud,g])
m3=oim_model([ud,ud2,ud3])
m4=oim_model([cv,eg])
m5=oim_model([cv,eg,ud,ud2,ud3])

#linking some parameters, actually replacing them
#cv.params["elong"]=eg.params["elong"]
#cv.params["pa"]=eg.params["pa"]

#fixing others
eg.params["x"].free=False
eg.params["y"].free=False
cv.params["x"].free=False
cv.params["y"].free=False
eg.params["pa"].value=30

nB=50 #number of baselines 
nwl=40 #number of walvengths

#Create some spatial frequencies

wl=np.linspace(3e-6,4e-6,num=nwl)
B=np.linspace(1,400,num=nB)
Bs=np.tile(B,(nwl,1)).flatten()
wls=np.transpose(np.tile(wl,(nB,1))).flatten()
spf0=Bs/wls

spf1=[spf0,spf0*0]  # a set of baseline from 0.01m to 400m aligned East-West
spf2=[spf0*0,spf0]  # a set of baseline from 0.01m to 400m aligned North-South

#Plots of the simulated visiblities and images for all models
m=m5
n=5
fig,ax=plt.subplots(1,n+2,figsize=(15,4))
    #Compute visibilities for the models and spatial frequencies
v1=np.abs(m.getComplexVisibility(spf1,wls)).reshape(len(wl),len(B))
v2=np.abs(m.getComplexVisibility(spf2,wls)).reshape(len(wl),len(B))
for iwl,wli in enumerate(wl):
    ax[0].plot(B/wli*mas2rad,v1[iwl,:],color=plt.cm.plasma(iwl/nwl),alpha=0.5)
    ax[1].plot(B/wli*mas2rad,v2[iwl,:],color=plt.cm.plasma(iwl/nwl),alpha=0.5)        
ax[0].scatter(spf0*mas2rad,v1,c=wls*1e6,s=0.1,cmap="plasma")
sc=ax[1].scatter(spf0*mas2rad,v2,c=wls*1e6,s=0.1,cmap="plasma")
ax[1].set_xlabel("B/$\\lambda$ (cycles/mas)")
ax[0].margins(0,0)
ax[0].set_ylim(0,1)
ax[1].margins(0,0)
ax[1].set_ylim(0,1)
#ax[2,i].set_title(model_names[i],fontsize=8)

ax[0].set_ylabel("Vis. (East-West)")         
ax[1].set_ylabel("Vis. (North-South)") 

# Create direct images of the model

dim=128   #Number of pixels for the image
pix=0.15  #size of the pixel in the image in mas
wls=np.linspace(3,4,n)*1e-6 #wavelengths array for the images

  
im=m.getImage(dim,pix,wls)
for iwl,wli in enumerate(wls):     
    imi=im[iwl,:,:]
    imi/=np.max(imi)
    imshow=ax[iwl+2].imshow(imi,extent=[dim/2*pix,-dim/2*pix,-dim/2*pix,dim/2*pix],
                            norm=colors.TwoSlopeNorm(vmin=0., vcenter=0.2, vmax=1)
                            ,cmap="plasma", interpolation=None)
    ax[iwl+2].text(0,dim*pix/2,"\n{:.1f}$\\mu$m".format(wli*1e6),
                   color="w",va="top",ha="center")

    #if iwl!=1:
    #    ax[iwl,i].get_xaxis().set_visible(False)
    #if iwl==1:
    ax[iwl+2].set_xlabel("$\\alpha$(mas)")
    #ax[iwl+2].set_ylabel("$\\delta$(mas)")
        
fig.tight_layout()            
fig.colorbar(imshow, ax=ax[2:].ravel().tolist(),label="Normalized Intensity")
fig.colorbar(sc, ax=ax[:2].ravel().tolist(),label="$\\lambda$ ($\\mu$m)")

