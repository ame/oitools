1# -*- coding: utf-8 -*-
"""
Created on Wed May 26 10:56:39 2021

@author: Ame
"""

from oitools.oimodel import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
import time

mas2rad=np.pi/180/3600./1000.


dim=256
Bmax=1000 #meter
lam=1e-6 #meter

B=np.linspace(-Bmax,Bmax,dim)

spfy=np.outer(B,B*0+1)/lam
spfx=np.transpose(spfy)

spfy=spfy.flatten()
spfx=spfx.flatten()


g_flat = oim_gauss(fwhm=1)

ud=oim_ud(d=1)
er=oim_new_skw_ring(d=8,elong=2,pa=70,skw=1,pa_skw=0)
g=oim_gauss2(fwhm=1)
cv=oim_convolution(er,g)

eg=oim_egauss(fwhm=30,elong=1,pa=0)


#############################
er=oim_new_skw_ring(d=10, elong=1,pa=0, pa_skw = 0, skw=0.7)
g1=oim_gauss2(fwhm=4, f= 0.1)
lor1 = oim_lorentz(fwhm = 2, f = 0.1)
kernel = kernel_lor_gauss([lor1, g1], rlim = er.params["d"], rlim_mode = "diameter")
cv = oim_convolution(er, kernel)

prod = oim_product(er,kernel)
disk = oim_ud(d=3)

er1 = oim_new_skw_ring(d=10, elong=1,pa=0, pa_skw = 0, skw=0.7)
cv1 = oim_convolution(er1,disk)

er2 = oim_new_skw_ring(d=14, elong=1,pa=0, pa_skw = 0, skw=0.7)
cv2 = oim_convolution(er2,disk)

##############################



m=oim_model([er])

t1 = time.time()
ft=m.getComplexVisibility([spfx,spfy]).reshape(dim,dim)
t2 = time.time()
print(t2-t1)

im=np.abs(np.fft.fftshift(np.fft.ifft2(ft)))

freq=np.fft.fftshift(np.fft.fftfreq(dim,spfx[1]-spfx[dim])/mas2rad)
pix=freq[1]-freq[0]

im0=m.getImage(dim,pix)[:,:]

im=im/np.max(im)
im0=im0/np.max(im0)

fig,ax=plt.subplots(1,3,figsize=(10,5))
a = ax[0].imshow(im,extent=[freq[0],-freq[0],freq[0],-freq[0]], cmap = "jet", vmin = 0, vmax = 1)
b = ax[1].imshow(im0,extent=[freq[0],-freq[0],freq[0],-freq[0]], cmap = "jet")
c = ax[2].imshow(im0-im,extent=[freq[0],-freq[0],freq[0],-freq[0]], cmap = "jet")
ax[1].set_xlabel("$\\alpha$(mas)")
ax[1].get_yaxis().set_visible(False)
ax[0].set_xlabel("$\\alpha$(mas)")
ax[0].set_ylabel("$\\delta$(mas)")
ax[0].text(0,-freq[0],"\n Image from inverse FFT",ha="center", va="top",color="white")
ax[1].text(0,-freq[0],"\n Direct Image",ha="center", va="top",color="white")
ax[2].text(0,-freq[0],"\n  Direct image - FFt Image",ha="center", va="top",color="white")


cb = plt.colorbar(c)
cb.set_label("Residuals")


plt.figure()
plt.plot(np.linspace(freq[0],-freq[0],len(im[int(dim/2)])), im[int(dim/2)], label = "from FFT")
plt.plot(np.linspace(freq[0],-freq[0],len(im[int(dim/2)])), im0[int(dim/2)], label = "direct")
plt.xlabel("$\\alpha$(mas)")
plt.grid()
# plt.xlim(-30,-10)
plt.legend()


# tf = np.fft.fft2(im0)
# print(np.shape(tf))
# plt.figure()
# plt.plot(np.abs(tf[int(dim/2)]))