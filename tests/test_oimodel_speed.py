# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:23:36 2021

@author: Ame
"""
from oitools.oimodel import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
from datetime import datetime
mas2rad=np.pi/180/3600./1000.
from tqdm import tqdm

#Some components for the models
ud=oim_ud(d=0.7)
ud2=oim_ud(x=5,y=15,d=1.5,f=0.1)
g0=oim_gauss(fwhm=4)
g1=oim_gauss(fwhm=oim_interp1d([3e-6,3.5e-6,4e-6],[1,4,4]))
g2=oim_gauss(fwhm=oim_interp1d([3e-6,3.5e-6,4e-6],[1,4,4]),
            f=oim_interp1d([3e-6,4e-6],[1,4]))

er=oim_eskewring(d=oim_interp1d([3e-6,4e-6],[10,12]),elong=2,pa=0,skw=0.9)
eg=oim_egauss(fwhm=1,elong=2,pa=0,f=oim_interp1d([3e-6,4e-6],[1,0.01]))
g3=oim_gauss(fwhm=oim_interp1d([3e-6,4e-6],[1,2]))
cv=oim_convolution(er, g3)



#6 models to test with increasing complexity
m0=oim_model([g0])
m1=oim_model([ud])
m2=oim_model([g1])
m3=oim_model([ud,ud2])
m4=oim_model([ud,g2])
m5=oim_model([cv,eg])
models=[m0,m1,m2,m3,m4,m5]

names=[ "1 Component  : GD (0 chromatic parameters)",
       "1 Component  : UD (0 chromatic parameters)",
       "1 Component  : GD (1 chromatic parameters)",       
       "2 Components : Binary UDs (0 chromatic parameters)",
       "2 Components : GD + UD (2 chromatic parameters)",       
       "3 Components : SkRing*GD + EG (3 chromatic parameters)"]
colors= plt.rcParams['axes.prop_cycle'].by_key()['color']

# Arrays with number of baselines and wavelengths to be computed
nBs=np.linspace(2,200,num=40)
nwls=np.linspace(2,200,num=40)

# Arrays to contain final results for all models and numbers of 
#baselines and wavelengths
xx=[]
yy=[]

#number of simulated pts for each set of model,Baseline,wavelength
#Increasing the number increase the precision on the computation time 
# measurements but it affect the total time of the programme linearly
Ksim=1e5



# the main loop on the models
for imodel,(name,m) in  enumerate(zip(names,models)):
    npts_tot=0
    start0=datetime.now()
    t=np.ndarray([len(nBs),len(nwls)])
    ntot=np.ndarray([len(nBs),len(nwls)])
    
    #loops on the number of pts for the model : baselines and wavelength
    for iB,nB in enumerate(tqdm(nBs)):
        for iwl,nwl in  enumerate(nwls):
            
            #Create spatial frequencies
            #We only compute baselines aligned East-West for the test
            nBi=int(nB)
            nwli=int(nwl)
            wl=np.linspace(3e-6,4e-6,num=nwli)
            B=np.linspace(1,400,num=nBi)
            Bs=np.tile(B,(nwli,1)).flatten()
            wls=np.transpose(np.tile(wl,(nBi,1))).flatten()
            spf0=Bs/wls     
            spf=[spf0,spf0*0]  
            
            
            #As the models are fast to compute we loop on them nsim times
            #to increase accuracy on the computation time
            #nsim depends on the number of pts : nsim*nB*nwl ~ Ksim
            nsim=int(Ksim/nBi/nwli)+1
            start=datetime.now()
            for i in range(nsim):
                v=np.abs(m.getComplexVisibility(spf,wls))
                m.getFreeParameters()['fwhm_1_interp1']=np.random.rand()*5+1
            t[iB,iwl] = (datetime.now()-start).total_seconds()*1000/nsim
            ntot[iB,iwl] = nBi*nwli#*nsim
            npts_tot+= int(ntot[iB,iwl]*nsim)
            



    x=ntot.flatten()
    y=t.flatten()
    

    
    #appending the x,y to the final arrays for plotting later
    xx.append(x)
    yy.append(y)

    
    ttot = (datetime.now()-start0).total_seconds()
    print("Total time for {} pts in model {} : {:.1f}s => {:.2f}us/pt"
          .format(npts_tot,imodel,ttot,ttot/npts_tot*1e6), flush=True)
    
#%%    
#plotting the results    
fig,ax=plt.subplots(1,1,figsize=(8,8))
for imodel,(name,m) in  enumerate(zip(names,models)):  

    #Fitting the data with a linear function x=p*y 
    #p=[np.sum(yy[imodel])/np.sum(xx[imodel]),0]  
    
    #Or fitting the data with a linear function x=p[0]*y +p[1]
    p=np.polyfit(xx[imodel], yy[imodel],1)
    
    yfit=np.poly1d(p)(x)
      
    ax.scatter(xx[imodel],yy[imodel],marker=".",color=colors[imodel],alpha=0.3)
    ax.set_xlabel("Number of simulated data points = nB x n$\\lambda$ ")
    ax.set_ylabel("computing time (ms)")
    ax.plot(xx[imodel],yfit,color="k",lw=4)
    ax.plot(xx[imodel],yfit,color=colors[imodel],label=name)
    ax.text(np.max(xx[imodel]),np.max(yfit),"{:.2}$\\mu$s/pt".format(p[0]*1000),
            color=colors[imodel],va="top")


#One MATISSE LR measurement is 6 baselines with 110 walvenegth so 660 pts
#Plotting vertical lines for 10 and 50 measurements.
matLR=np.array([660,660])
for n in [10,50]:
    ax.plot(matLR*n,[0,np.max([yfit,yy[-1]])],color="k",ls=":")
    ax.text(matLR[0]*n,np.max([yfit,yy[-1]])*0.6,"{} MATISSE LR obs. ".format(n),
            color="k",ha="center",backgroundcolor="w")

ax.margins(0,0)
ax.legend()